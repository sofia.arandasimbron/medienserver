/**
 * @file DMXManager.cpp
 * @author Paul Hermann
 * @brief MediaServer DMX framework automatic tests
 * @version 0.1
 * @date 2022-10-01
 *
 * @copyright Copyright (c) 2022
 */

#include "dmx_framework.hpp"
#include <iostream>
#include <stdio.h>

/**
 * @brief       Test function for the DMX Manager Library
 */
int main()
{
    std::cout << "Mini DMX Example" << std::endl;
    DMXManager dmx_manager;
    dmx_manager.set_universe(1);
    dmx_manager.set_start_address(1);
    dmx_manager.set_footprint_size(10);

    auto f = [](DMXPackage dmx) {
        for (int i = 0; i < 10; i++)
        {
            printf("%3d%s", static_cast<int>(dmx.channels[i]), (dmx.is_new[i] ? "* " : "  "));
        }
        std::cout << std::endl;
    };

    dmx_manager.set_dmx_callback(f);
    dmx_manager.start();

    std::cin.get();
    dmx_manager.stop();
    std::cout << "Deinit" << std::endl;
    exit(EXIT_SUCCESS);
}