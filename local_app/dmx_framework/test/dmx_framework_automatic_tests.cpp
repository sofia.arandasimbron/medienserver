/**
 * @file DMXManager.cpp
 * @author Paul Hermann
 * @brief MediaServer DMX framework automatic tests
 * @version 0.1
 * @date 2022-10-01
 *
 * @copyright Copyright (c) 2022
 */

#include <iostream>
#include <stdio.h>
#include <gtest/gtest.h>
#include "dmx_framework.hpp"

void testWrongConfig(uint16_t universe_id, uint16_t start_address, uint16_t footprint_size) {
    EXPECT_THROW({
        try
        {
            DMXManager dmx_manager;
            dmx_manager.set_universe(universe_id);
            dmx_manager.set_start_address(start_address);
            dmx_manager.set_footprint_size(footprint_size);
            dmx_manager.start();
        }
        catch( const std::logic_error& err )
        {
            EXPECT_STREQ( "Invalid Receiver Config", err.what() );
            throw;
        }
    }, std::logic_error );
}

void testRightConfig(uint16_t universe_id, uint16_t start_address, uint16_t footprint_size) {
    try
    {
        DMXManager dmx_manager;
        dmx_manager.set_universe(universe_id);
        dmx_manager.set_start_address(start_address);
        dmx_manager.set_footprint_size(footprint_size);
        dmx_manager.start();
    }
    catch(...)
    {
        FAIL();
        return;
    }
    SUCCEED();
}

TEST(DMXFrameworkTests, WrongChannelTest1) {
    testWrongConfig(1,0,1);
}

TEST(DMXFrameworkTests, WrongChannelTest2) {
    testWrongConfig(1,513,1);
}

TEST(DMXFrameworkTests, WrongUniverseTest1) {
    testWrongConfig(0,1,1);
}

TEST(DMXFrameworkTests, WrongUniverseTest2) {
    testWrongConfig(64001,1,1);
}

TEST(DMXFrameworkTests, WrongFootprintTest1) {
    testWrongConfig(1,1,0);
}

TEST(DMXFrameworkTests, WrongFootprintTest2) {
    testWrongConfig(1,1,513);
}

TEST(DMXFrameworkTests, RightChannelTest1) {
    testWrongConfig(1,0,1);
}

TEST(DMXFrameworkTests, RightChannelTest) {
    testRightConfig(1,512,1);
}

TEST(DMXFrameworkTests, RightUniverseTest) {
    testRightConfig(63999,1,1);
}

TEST(DMXFrameworkTests, RightFootprintTest) {
    testRightConfig(1,1,512);
}