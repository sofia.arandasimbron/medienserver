/**
 * @file dmx_framework.cpp
 * @author Paul Hermann
 * @brief MediaServer DMX framework
 * @version 0.1
 * @date 2022-01-01
 *
 * @copyright Copyright (c) 2022
 */

#include "dmx_framework.hpp"

#include <chrono>
#include <iostream>

/**
 * @brief Construct a new DMXManager::DMXManager object
 * 
 */
DMXManager::DMXManager() {
    sacn::Init();
}

/**
 * @brief Sets start_address
 * @details Works while receiver is running
 *
 * @param start_address in range of 1..512
 */
void DMXManager::set_start_address(uint16_t start_address)
{
    if (start_address < 1 || start_address > 512)
    {
        throw std::logic_error("Invalid Receiver Config");
    }
    else
    {
        this->start_address = start_address - 1;
    }
}

/**
 * @brief Sets footprint-size
 * @details Only works, when receiver is not running
 *
 * @param footprint_size in range of 1..512
 */
void DMXManager::set_footprint_size(uint16_t footprint_size)
{
    if (footprint_size < 1 || footprint_size > 512)
    {
        throw std::logic_error("Invalid Receiver Config");
    }
    else
    {
        this->footprint_size = footprint_size;
    }
}

/**
 * @brief Sets universe
 * @details Works while receiver is running
 * @param universe in range of 1..63999
 */
void DMXManager::set_universe(uint16_t universe)
{
    if (universe < 1 || universe > 63999)
    {
        throw std::logic_error("Invalid Receiver Config");
    }
    else
    {
        this->universe = universe;
        this->recv.ChangeUniverse(universe);
    }
}

/**
 * @brief       starts the receiver
 */
void DMXManager::start()
{
    this->config.universe_id = this->universe;
        
    if (!this->config.IsValid())
    {
        throw std::logic_error("Invalid Receiver Config");
    }
    this->recv.Startup(this->config, *this);
}

/**
 * @brief       Stops the receiver
 */
void DMXManager::stop()
{
    this->recv.Shutdown();
    sacn::Deinit();
}

/**
 * @brief       Function will be called on incoming sacn-packets.
 * @details     The function checks, if the values in the defined range are different to the previous and calls the
 * callback function.
 *
 * @param[in]   receiver_handle This function will be called on incoming packets
 * @param[in]   source_addr
 * @param[in]   receiver_handle
 * @param[in]   receiver_handle
 */
void DMXManager::HandleUniverseData(Handle receiver_handle, const etcpal::SockAddr &source_addr,
                                    const SacnRemoteSource &source_info, const SacnRecvUniverseData &universe_data)
{
    // auto start_time = std::chrono::high_resolution_clock::now();
    // std::chrono::duration<double> diff = start_time.duration;

    int sent_start           = universe_data.slot_range.start_address;
    int sent_count           = universe_data.slot_range.address_count;
    bool new_value           = false;
    bool new_value_in_packet = false;

    DMXPackage pack;

    uint16_t size = (sent_count < (this->footprint_size + this->start_address)) ? sent_count : this->footprint_size;

    for (int i = 0; i < size; i++)
    {
        new_value      = previous_dmx_values[i] != universe_data.values[i + this->start_address];
        pack.is_new[i] = new_value;
        if (new_value)
        {
            new_value_in_packet = true;
        };
    }
    for (int i = size; i < this->footprint_size; i++)
    {
        pack.is_new[i] = false;
    }

    std::copy_n(universe_data.values + this->start_address, size, std::begin(pack.channels));
    this->previous_dmx_values = pack.channels;

    // auto stop = std::chrono::high_resolution_clock::now();
    // auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
    // std::cout << duration.count() << std::endl;

    if (new_value_in_packet)
    {
        this->callback(pack);
    }
}

/**
 * @brief       Will be called, when sacn-source is lost
 *
 * @note        Currently not used
 */
void DMXManager::HandleSourcesLost(Handle handle, uint16_t universe, const std::vector<SacnLostSource> &lost_sources) {}

/**
 * @brief       Sets the callback-function for incoming sacn-packets
 *
 * @param[in]   f This function will be called on incoming packets
 */
void DMXManager::set_dmx_callback(std::function<void(DMXPackage)> f)
{
    this->callback = f;
}