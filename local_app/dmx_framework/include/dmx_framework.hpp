/**
 * @file dmx_framework.hpp
 * @author Paul Hermann
 * @brief MediaServer DMX framework
 * @version 0.1
 * @date 2022-01-01
 *
 * @copyright Copyright (c) 2022
 */

#pragma once
#include "sacn/cpp/receiver.h"
#include <array>
#include <functional>
#include <stdint.h>
#include <vector>
using Handle = sacn::Receiver::Handle;

/**
 * @struct DMXPackage
 * @brief A struct to hold processed dmx-information
 *
 * @var DMXPackage::channels
 * Array of channel-values
 *
 * @var DMXPackage::is_new
 * Includes the information for each channel, if the value changed in comparison to the last dmx-frame
 *
 * @var DMXPackage::length
 * Used length of channel-array, depends on configured footprint-size
 *
 */
struct DMXPackage
{
    std::array<uint8_t, 512> channels;
    std::array<bool, 512> is_new;
    std::size_t length{};
};

/**
 * @brief The DMXManager class receives sacn-frames, processes them and offers a simple API to retreive the data
 */
class DMXManager : public sacn::Receiver::NotifyHandler
{
  private:
    uint16_t footprint_size;
    uint16_t start_address;
    uint16_t universe;
    sacn::Receiver recv{};
    sacn::Receiver::Settings config{};
    std::function<void(DMXPackage)> callback{};
    std::array<uint8_t, 512> previous_dmx_values;
    void HandleUniverseData(Handle receiver_handle, const etcpal::SockAddr &source_addr,
                            const SacnRemoteSource &source_info, const SacnRecvUniverseData &universe_data) override;
    void HandleSourcesLost(Handle handle, uint16_t universe, const std::vector<SacnLostSource> &lost_sources) override;

  public:
    DMXManager();
    void set_start_address(uint16_t start_address);
    void set_footprint_size(uint16_t footprint_size);
    void set_universe(uint16_t universe);
    void start();
    void stop();
    void set_dmx_callback(std::function<void(DMXPackage)> f);
};