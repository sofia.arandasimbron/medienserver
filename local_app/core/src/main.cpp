/**
 * @file main.cpp
 * @author Paul Hermann
 * @brief MediaServer Core Executable File
 * @version 0.1
 * @date 2021-12-31
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "dmx_framework.hpp"
#include "file_watcher.hpp"
#include "json_reader.hpp"
#include "config_reader.hpp"
#include "media_framework.hpp"
#include <iostream>
#include <string>
#include <thread>

class Mediaserver
{
  protected:
    // Relative path of media.json file, containing Lists and Elements
    std::string mediafile;
    std::string configfile;
    DMXManager dmx_manager;
    MediaBlackbox media;
    MediaReader media_reader;
    ConfigReader config_reader;
    FileWatcher media_watcher;
    FileWatcher config_watcher;

    /**
     * @brief Set the dmx config in realtime from configfile
     */
    void set_dmx_config() {
        uint16_t universe = config_reader.get_universe();
        uint16_t start_address = config_reader.get_start_address();
        std::cout << "[CORE] Updating dmx-config to universe: " << universe << ", start_address: " << start_address << std::endl;
        dmx_manager.set_start_address(start_address);
        dmx_manager.set_universe(universe);
    }

    /**
     * @brief Reloads mediafile
     * @details Blocks if file does not exist
     *
     */
    void reload_media_file()
    {
        // Endless loop, if mediafile is not existing
        while (!std::filesystem::exists(mediafile))
        {
            std::cout << "[CORE] Couldn't find mediafile" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        std::cout << "[CORE] Reloading mediafile" << std::endl;
        try {
            media_reader.set_file(mediafile);
        } catch (...){
            // ToDo: Handle better
            std::cout << "[CORE] Reloading mediafile failed" << std::endl;
        }
    }

    /**
     * @brief Reloads configfile
     * @details Blocks if file does not exist, sets dmx config, if existing
     *
     */
    void reload_config_file()
    {
        // Endless loop, if configfile is not existing
        while (!std::filesystem::exists(configfile))
        {
            std::cout << "[CORE] Couldn't find configfile" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        std::cout << "[CORE] Reloading configfile" << std::endl;
        try {
            config_reader.set_file(configfile);
        } catch (...){
            // ToDo: Handle better
            std::cout << "[CORE] Reloading configfile failed" << std::endl;
        }
        set_dmx_config();
    }

public:
    
    Mediaserver(std::string mediafile, std::string configfile) : mediafile{mediafile}, configfile{configfile}
    {
        std::cout << "[CORE] Media Server Core starting..." << std::endl;
    };

    void start()
    {

        // Callback function to handle incoming dmx-packets
        auto dmx_received = [=](DMXPackage dmx) {
            // Only look into packet, if some value has changed
            if (dmx.is_new[0] || dmx.is_new[1])
            {
                std::cout << "[CORE] Received new DMX Values [" << unsigned(dmx.channels[0]) << ";"
                          << unsigned(dmx.channels[1]) << "]" << std::endl;
                // Check, if entry in list and list-element exists for new dmx-values
                if (!media_reader.is_empty(dmx.channels[0], dmx.channels[1]))
                {
                    // Get Playpath of mediafile and set it in mediaframework
                    std::string playpath = media_reader.get_path(dmx.channels[0], dmx.channels[1]);
                    std::cout << "[CORE] set_media_src(" << playpath << ")" << std::endl;
                    media.set_media_src(playpath);
                }
                else
                {
                    // Stop mediafile, if no corresponding element in mediafile exists
                    std::cout << "[CORE] No corresponding element in media.json: stop_media()" << std::endl;
                    media.stop_media();
                }
            }
        };

        // Initializing mediaframework
        media.init();
        auto result = std::thread([=]() { media.bus_loop(); });
        reload_media_file();

        // Initializing dmx framework
        dmx_manager.set_dmx_callback(dmx_received);
        reload_config_file();
        dmx_manager.set_footprint_size(config_reader.get_footprint_size());

        // Initialize filewatcher to look for updates in mediafile
        media_watcher.start(mediafile, std::chrono::milliseconds(5000), [=]() -> void {
            std::cout << "[CORE] Detected update in mediafile" << std::endl;
            reload_media_file();
        });

        // Initialize filewatcher to look for updates in configfile
        config_watcher.start(configfile, std::chrono::milliseconds(5000), [=]() -> void {
            std::cout << "[CORE] Detected update in configfile" << std::endl;
            reload_config_file();
        });

        // Starting to listen for dmx
        std::cout << "[CORE] Starting to listen for dmx..." << std::endl;
        dmx_manager.start();

        std::cin.get();
    }

    void stop()
    {
        std::cout << "[CORE] Stopping media-playback" << std::endl;
        media.stop_media();
        std::cout << "[CORE] Stopping dmx-listener" << std::endl;
        dmx_manager.stop();
        std::cout << "[CORE] Stopping media-watcher" << std::endl;
        media_watcher.stop();
        std::cout << "[CORE] Stopping config-watcher" << std::endl;
        config_watcher.stop();
    }
};

/**
 * @brief Main function for core application
 *
 * @return int
 */
int main(int arg, char *argv[])
{
    Mediaserver mediaserver("/usr/bin/mediaserver/medien.json", "/usr/bin/mediaserver/config.json");
    mediaserver.start();
    std::cin.get();
    mediaserver.stop();

    exit(EXIT_SUCCESS);
}
