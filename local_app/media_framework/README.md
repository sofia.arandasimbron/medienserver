# GStreamer Notes

## gst_bin_add(_many)

Every element needs to be unowned before adding it to the pipeline, because else it will be unreferenced more than one time