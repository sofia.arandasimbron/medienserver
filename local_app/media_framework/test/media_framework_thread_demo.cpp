#include "media_framework.hpp"
#include <chrono>
#include <iostream>
#include <math.h>
#include <thread>
#include <vector>
int main(int arg, char *argv[])
{
    using namespace std::chrono_literals;
    MediaBlackbox mbb;
    mbb.init();
    mbb.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");
    auto result = std::thread([&mbb]() { mbb.bus_loop(); });
    std::cin.get();

    auto sine = std::thread([&mbb]() {
        for (int i = 0; i < 10000; i++)
        {
            double x = (sin(i / 10.0) + 1) / 2.0;
            mbb.set_saturation(x);
            std::cout << x << std::endl;
            std::this_thread::sleep_for(10ms);
        }
    });

    sine.join();
    mbb.set_saturation(0.0);
    std::cin.get();
    mbb.set_saturation(1.0);
    std::cin.get();
    // gst::deinit();
}