#include "media_framework.hpp"
#include <iostream>
#include <vector>

#include <thread>
int main(int arg, char *argv[])
{
    MediaBlackbox mbb;
    mbb.init();
    std::cin.get();
    printf("Calling start_media()\n");
    mbb.start_media();

    std::cin.get();
    printf("Calling stop_media()\n");
    mbb.stop_media();

    std::cin.get();
    printf("Calling pause_media()\n");
    mbb.pause_media();

    std::cin.get();
    printf("Calling set_media_src() with valid path\n");
    mbb.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");

    std::cin.get();
    printf("Calling set_media_src() with valid path\n");
    mbb.set_media_src("/home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4");

    std::cin.get();
    printf("Calling start_media()\n");
    mbb.start_media();

    std::cin.get();
    printf("Calling stop_:media()\n");
    mbb.stop_media();

    std::cin.get();
    printf("Calling set_media_src() with invalid path\n");
    mbb.set_media_src("invalid path");

    std::cin.get();
    printf("Calling set_media_src() with invalid path\n");
    mbb.set_media_src("invalid path");

    std::cin.get();
    printf("Calling pause_media()\n");
    mbb.pause_media();

    std::cin.get();
    printf("Calling start_media()\n");
    mbb.start_media();

    std::cin.get();
    printf("Calling set_media_src() with valid path\n");
    mbb.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");

    std::cin.get();
    printf("Calling pause_media()\n");
    mbb.pause_media();

    std::cin.get();
    printf("Calling start_media()\n");
    mbb.start_media();

    std::cin.get();
    printf("Calling set_media_src() with invalid path\n");
    mbb.set_media_src("invalid path");

    std::cin.get();
    printf("Calling stop_:media()\n");
    mbb.stop_media();
}