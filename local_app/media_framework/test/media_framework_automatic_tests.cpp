/**
 * @file media_framework_automatic_tests.cpp
 * @author Sofia Aranda
 * @brief MediaServer Media framework automatic tests
 * @version 0.2
 *
 * @copyright Copyright (c) 2022
 */

#include "core/Element.hpp"
#include "media_framework.hpp"
#include <iostream>
#include "Prelude.hpp"
#include <stdio.h>
#include <stdexcept>
#include <exception>
#include <fstream>
#include <gtest/gtest.h>
#include <chrono>
#include <thread>

class MediaBlackboxTest : public ::testing::Test{
    //Testing convention: all test files should have a test class in there. This avoids the multiple object creation.
    protected:
    void SetUp() override{
        underTest.init();
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }

    void TearDown() override
    {
        underTest.deinit();
    }

  public:
  MediaBlackbox underTest;
    
};

TEST_F(MediaBlackboxTest, test_start_media_stateIsNull_ifNoMediaSet){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    //init()
    //then
    EXPECT_TRUE(underTest.get_decodebin_state() == gst::Null);
}

TEST_F(MediaBlackboxTest, test_start_media_stateIsPlaying_ifValidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 1);
    EXPECT_TRUE(underTest.get_decodebin_state() == gst::Playing);
}

TEST_F(MediaBlackboxTest, test_start_media_stateIsNotPlaying_ifInvalidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pii/media/bbb_sunflower_720p_30fps_h264.mp4");
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 0);
    EXPECT_TRUE(underTest.get_decodebin_state() != gst::Playing);
}

TEST_F(MediaBlackboxTest, test_stop_media_stateIsNotPlaying_ifNoMediaSet){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.stop_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 0);
    EXPECT_TRUE(underTest.get_decodebin_state() != gst::Playing);
}

TEST_F(MediaBlackboxTest, test_stop_media_stateIsNotPlaying_ifValidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");
    underTest.start_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    underTest.stop_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 0);
    EXPECT_TRUE(underTest.get_decodebin_state() != gst::Playing);
}

TEST_F(MediaBlackboxTest, test_stop_media_stateIsNotPlaying_ifInvalidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pii/media/bbb_sunflower_720p_30fps_h264.mp4");
    underTest.start_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    underTest.stop_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 0);
    EXPECT_TRUE(underTest.get_decodebin_state() != gst::Playing);
}

TEST_F(MediaBlackboxTest, test_pause_media_stateIsNull_ifNoMediaSet){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.pause_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_decodebin_state() == gst::Null);
}

TEST_F(MediaBlackboxTest, test_pause_media_stateIsPaused_ifValidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4");
    underTest.start_media();
    underTest.pause_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 1);
    EXPECT_TRUE(underTest.get_decodebin_state() == gst::Paused);
}

TEST_F(MediaBlackboxTest, test_pause_media_stateIsNotPlaying_ifInvalidUri){
    //given
    //underTest object declared in the MediaBlackboxTest class
    //when
    underTest.set_media_src("/home/pii/media/bbb_sunflower_720p_30fps_h264.mp4");
    underTest.start_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    underTest.pause_media();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    //then
    EXPECT_TRUE(underTest.get_alpha_value() == 0);
    EXPECT_TRUE(underTest.get_decodebin_state() != gst::Playing);
}