/**
 * @file media_framework.cpp
 * @author Leena Jamil
 * @brief
 * @version 0.1
 * @date 2022-01-15
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "media_framework.hpp"
#include "Prelude.hpp"
#include "core/Pad.hpp"
#include "core/Structure.hpp"
#include "gst/gstbus.h"
#include "gst/gstclock.h"
#include "gst/gstinfo.h"
#include "gst/gstmessage.h"
#include "gst/gstobject.h"
#include <filesystem>
#include <iostream>
#include <stdexcept>
#include <string>

/**
 * @brief Initialises Mediaframework
 * @details Must be called, before any other Media Framework method can be called
 *
 */
void MediaBlackbox::init()
{
    gst_println("[Mediaframework] Initializing");
    gst::init(NULL, NULL);

    gst_println("[Mediaframework] Creating pipeline elements");

    /**
     * @brief Element Creation
     */

    // Uridecodebin Source Branch Elements
    source.reset(new gst::Element(gst::ElementFactory::make("uridecodebin")));
    video_balance.reset(new gst::Element(gst::ElementFactory::make("videobalance")));
    video_convert.reset(new gst::Element(gst::ElementFactory::make("videoconvert")));
    video_scale.reset(new gst::Element(gst::ElementFactory::make("videoscale")));
    caps_filter_source.reset(new gst::Element(gst::ElementFactory::make("capsfilter")));

    // Black Background Source Branch Elements
    black.reset(new gst::Element(gst::ElementFactory::make("videotestsrc")));
    caps_filter_black.reset(new gst::Element(gst::ElementFactory::make("capsfilter")));

    // Video Sink Branch Elements
    compositor.reset(new gst::Element(gst::ElementFactory::make("compositor")));
    caps_filter_sink.reset(new gst::Element(gst::ElementFactory::make("capsfilter")));
    videosink.reset(new gst::Element(gst::ElementFactory::make("kmssink")));

    // Audio Sink Branch Elements
    audio_convert.reset(new gst::Element(gst::ElementFactory::make("audioconvert")));
    audio_resample.reset(new gst::Element(gst::ElementFactory::make("audioresample")));
    audiosink.reset(new gst::Element(gst::ElementFactory::make("autoaudiosink")));

    // More element creation
    pipeline.reset(new gst::Pipeline(gst::Pipeline::make("MediaBlackbox")));

    /**
     * @brief Creation of Pipeline WITHOUT Uridecodebin
     * Adds an links elements (without Uridecodebin)
     */

    gst_println("[Mediaframework] Adding Elements to pipeline");
    pipeline->add(*black).add(*caps_filter_black);                      // Black Background Source Branch
    pipeline->add(*compositor).add(*caps_filter_sink).add(*videosink);  // Video Sink Branch
    pipeline->add(*audio_convert).add(*audio_resample).add(*audiosink); // Audio Sink Branch

    gst_println("[Mediaframework] Linking static pads");
    black->link(*caps_filter_black).link(*compositor);     // Black Background Source Branch
    compositor->link(*caps_filter_sink).link(*videosink);  // Video Sink Branch
    audio_convert->link(*audio_resample).link(*audiosink); // Audio Sink Branch

    /**
     * @brief Configures Pipeline-Elements
     * Configures Object-settings, adds callbacks
     */
    gst_println("[Mediaframework] Getting Compositor sinkpad of background");
    sinkpad_comp_background.reset(new gst::Pad(compositor->static_pad("sink_0")));

    gst_println("[Mediaframework] Adding pad-added callback for uridecodebin");
    g_signal_connect(source->ref(), "pad-added", G_CALLBACK(this->decode_pad_added_handler), this);

    gst_println("[Mediaframework] Configuring Elements");
    g_object_set(compositor->ref(), "background", 3, NULL);          // Set Background of Compositor to transparent
    g_object_set(black->ref(), "pattern", 2, NULL);                  // Set Source Pattern of Background Source to Black
    g_object_set(video_scale->ref(), "n-threads", 4, NULL);          // Use 4 Threads for Videoscaling
    g_object_set(sinkpad_comp_background->ref(), "zorder", 1, NULL); // Set z-layer for background

    // TODO support variable display-sizes (not only full hd)
    // Configure Capsfilter for background (1FPS only)
    g_object_set(G_OBJECT(caps_filter_black->ref()), "caps",
                 gst_caps_from_string("video/x-raw, width=1920, height=1080, framerate=1/1, format=I420"), NULL);
    // Configure Capsfilter for sink
    g_object_set(G_OBJECT(caps_filter_sink->ref()), "caps",
                 gst_caps_from_string("video/x-raw, width=1920, height=1080, format=I420"), NULL);
    // Configure Capsfilter of source (SCALING TO FULL HD IS PERFORMANCE BOTTLENECK)
    g_object_set(G_OBJECT(caps_filter_source->ref()), "caps",
                 gst_caps_from_string("video/x-raw, width=1920, height=1080, format=I420"), NULL);

    gst_println("[Mediaframework] Adding Buffer Probe to videosink");
    gst::Pad sink_pad = videosink->static_pad("sink");
    // Adds Probe (Callback) to videosink-sinkpad, to add uridecodebin, when the first valid frame reaches videosink
    gst_pad_add_probe(sink_pad.ref_pad(), GST_PAD_PROBE_TYPE_BUFFER, compositor_running_callback, this, NULL);

    gst_println("[Mediaframework] Starting Playback");
    this->pipeline->set_state(gst::State::Playing);

    gst_println("[Mediaframework] init() done");
}

/**
 * @brief Will be called, when compositor gets buffers the first time
 * @details Adds Uridecodebin to Pipeline
 *
 */
GstPadProbeReturn MediaBlackbox::compositor_running_callback(GstPad *pad, GstPadProbeInfo *info, gpointer user_data)
{
    gst_println("[Mediaframework] First buffer reached videosink");
    static_cast<MediaBlackbox *>(user_data)->add_source(); // Call add_source()
    return GST_PAD_PROBE_REMOVE;                           // Deactivate Callback
}

/**
 * @brief Adds Source-Pipeline to bin, links it and configures it
 *
 */
void MediaBlackbox::add_source()
{
    gst_println("[Mediaframework] Adding Source-Pipeline");
    // Adds Uridecodebin-Branch Pipeline to Bin
    pipeline->add(*source).add(*video_balance).add(*video_convert).add(*video_scale).add(*caps_filter_source);
    // Links static pads of Uridecodebin-Pipeline-Branch
    video_balance->link(*video_convert).link(*video_scale).link(*caps_filter_source).link(*compositor);
    // Saves sinkpad of compositor, which is linked to uridecodebin-branch
    sinkpad_comp_foreground.reset(new gst::Pad(compositor->static_pad("sink_1")));
    // Brings uridecodebin to the front layer
    g_object_set(sinkpad_comp_foreground->ref(), "zorder", 2, NULL);
    gst_println("[Mediaframework] Source-Pipeline added succesfully");
}

/**
 * @brief Deinitalisation of GStreamer, stops playback
 *
 */
void MediaBlackbox::deinit()
{
    pipeline->set_state(gst::State::Null);
    gst::deinit();
}

/**
 * @brief Sets saturation value
 *
 * @param saturation 0 equals black/white, 1 equals normal color
 */
void MediaBlackbox::set_saturation(double saturation)
{
    g_object_set(video_balance->ref(), "saturation", saturation, NULL);
}

/**
 * @brief Sets alpha
 *
 * @param alpha 0 equals invisible, 1 equals visible, anything between 0 and 1 kills performance on pi (but should be
 * possible)
 */
void MediaBlackbox::set_alpha(double alpha)
{
    g_object_set(sinkpad_comp_foreground->ref(), "alpha", alpha, NULL);
}

/**
 * @brief Start playing the currently set media source
 *
 */
void MediaBlackbox::start_media()
{
    if (!source->get_property("uri").empty())
    {
        this->pipeline->set_state(gst::State::Playing);
        this->set_alpha(1); // Sets source visible
    }
}

/**
 * @brief pause playing the currently selected media source (pausing)
 *
 */
void MediaBlackbox::pause_media()
{
    if (!source->get_property("uri").empty())
    {
        this->pipeline->set_state(gst::State::Paused);
    }
}

/**
 * @brief stop the pipeline and set its state to Null
 * Needs to be called at the end of main function
 *
 */
void MediaBlackbox::stop_media()
{
    this->set_alpha(0); // Sets source invisible
    this->source->set_state(gst::State::Paused);
}

/**
 * @brief Setting the currently selected
 *
 * @param file_path file path to a media file on the disk (relative to executable allowed)
 */
void MediaBlackbox::set_media_src(std::string file_path)
{
    std::string new_path     = "file://";
    std::string current_path = source->get_property("uri");
    std::string uri          = new_path.append(std::filesystem::absolute(file_path));
    if (!file_path.empty() && std::filesystem::exists(file_path))
    {
        gst_println("[Mediaframework] Starting playback of file: %s", uri.c_str());
        pipeline->set_state(gst::State::Ready);
        source->set_property_from_string("uri", uri);
        pipeline->set_state(gst::State::Playing);
        this->set_alpha(1); // Sets source visible
    }
    else
    {
        this->stop_media();
        source->set_property_from_string("uri", "");
    }
}

/**
 * @brief Callback handler for the added pad on uridecodebin
 *
 * @param src the uridecodebin as GstElement*
 * @param new_pad the new pad added on the uridecodebin
 * @param data the mediablackbox for simpler pipeline access
 */
void MediaBlackbox::decode_pad_added_handler(GstElement *src, GstPad *_new_pad, MediaBlackbox *data)
{
    gst::Pad audio_sink_pad = data->audio_convert->static_pad("sink");
    gst::Pad video_sink_pad = data->video_balance->static_pad("sink");
    gst::Pad new_pad        = gst::Pad(_new_pad);

    if (audio_sink_pad.is_linked() && video_sink_pad.is_linked())
    {
        gst_println("[Mediaframework] New pad added, video and audio are already linked");
        return;
    }

    gst::LinkReturn ret;
    gst::Caps new_pad_caps        = new_pad.current_caps();
    gst::Structure new_pad_struct = new_pad_caps.structure(0);
    std::string new_pad_type      = new_pad_struct.name();

    if ((g_str_has_prefix(new_pad_type.c_str(), "audio/x-raw") == 0) &&
        (g_str_has_prefix(new_pad_type.c_str(), "video/x-raw") == 0))
    {
        gst_println("[Mediaframework] Type of new pad is neither audio/x-raw nor video/x-raw");
        return;
    }

    if (g_str_has_prefix(new_pad_type.c_str(), "audio/x-raw") != 0)
    {
        gst_println("[Mediaframework] Trying to link audio");
        ret = new_pad.link(audio_sink_pad);
    }
    else
    {
        gst_println("[Mediaframework] Trying to link video");
        ret = new_pad.link(video_sink_pad);
    }

    if (ret < gst::LinkReturn::OK)
    {
        gst_println("[Mediaframework] Type is %s but link failed .", new_pad_type.c_str());
    }
    else
    {
        gst_println("[Mediaframework] Link succeeded (type %s).", new_pad_type.c_str());
    }
}

/**
 * @brief listens to bus
 *
 */
void MediaBlackbox::bus_loop()
{
    gst::Message msg = nullptr;
    gst::Bus bus     = pipeline->bus();
    while (true)
    {
        using MsgType = gst::msg::Type;
        msg           = bus.pop_timed_filtered(-1, static_cast<MsgType>(MsgType::Error | MsgType::EOS));
        switch (msg.type())
        {
            GError *err;
            gchar *debug_info;
        case MsgType::Error:
            gst_message_parse_error(msg.ref(), &err, &debug_info);
            g_printerr("Error received from elment %s:%s", GST_OBJECT_NAME(msg.ref()->src), err->message);
            g_printerr("Debugging Information : %s", debug_info ? debug_info : "None");
            g_clear_error(&err);
            g_free(debug_info);
        case MsgType::EOS:
            this->pipeline->set_state(gst::State::Paused);
            return;
        default:
            break;
        }
    }
}

/**
 * @brief Get the Pipeline State (according to enum values)
 *
 * @return enum State
 */
gst::State MediaBlackbox::get_pipeline_state()
{
    return this->pipeline->state();
}

/**
 * @brief Get the Decodebin State (according to enum values)
 *
 * @return enum State
 */
gst::State MediaBlackbox::get_decodebin_state()
{
    return this->source->state();
}

/**
 * @brief Returns alpha value of front layer in Compositor
 *
 * @return double: alpha value
 */
double MediaBlackbox::get_alpha_value()
{
    double return_value;
    g_object_get(sinkpad_comp_foreground->ref(), "alpha", &return_value);
    return return_value;
}