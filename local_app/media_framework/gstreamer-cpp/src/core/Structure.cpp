#include "core/Structure.hpp"
#include "gst/gststructure.h"

namespace gst
{
    Structure::Structure(GstStructure *ref) : ref_structure(ref) {}

    std::string Structure::name()
    {
        return gst_structure_get_name(this->ref_structure);
    }

    GstStructure *Structure::ref()
    {
        return this->ref_structure;
    }
} // namespace gst