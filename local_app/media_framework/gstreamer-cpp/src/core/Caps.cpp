#include "core/Caps.hpp"
#include "gst/gstcaps.h"
#include "gst/gstobject.h"
namespace gst
{
    // Caps(GstCaps *ref);
    // std::string to_string();
    Caps::Caps(GstCaps *ref) : caps_ref(ref) {}
    Caps::~Caps()
    {
        gst_object_unref(this->caps_ref);
    }

    GstCaps *Caps::ref()
    {
        return this->caps_ref;
    }

    std::string Caps::to_string()
    {
        return std::string(gst_caps_to_string(this->caps_ref));
    }

    Structure Caps::structure(uint32_t idx)
    {
        return Structure(gst_caps_get_structure(this->caps_ref, idx));
    }

    bool Caps::empty()
    {
        return this->caps_ref != nullptr;
    }
} // namespace gst