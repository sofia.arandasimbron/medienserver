#pragma once

#include "core/Structure.hpp"
#include "gst/gstcaps.h"
#include <string>
namespace gst
{
    class Caps
    {
      private:
        GstCaps *caps_ref;
        // TODO: Copy, Move Constructor

      public:
        Caps(GstCaps *ref);
        ~Caps();
        GstCaps *ref();
        std::string to_string();
        Structure structure(uint32_t idx);
        bool empty();
    };

} // namespace gst