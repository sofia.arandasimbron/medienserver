#pragma once

#include "gst/gststructure.h"
#include <string>
namespace gst
{
    class Structure
    {
      private:
        GstStructure *ref_structure;

      public:
        Structure(GstStructure *ref);
        GstStructure *ref();
        std::string name();
    };
} // namespace gst