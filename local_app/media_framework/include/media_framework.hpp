/**
 * @file media_framework.hpp
 * @author Leena Jamil
 * @brief
 * @version 0.1
 * @date 2022-01-15
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once
#include "Prelude.hpp"
#include <memory>

/**
 * @brief Media API class
 * Creates a pipeline internally and provides methods to start, stop and set media sources
 *
 */
class MediaBlackbox
{
  protected:
    // Uridecodebin Source Branch
    std::unique_ptr<gst::Element> source;
    std::unique_ptr<gst::Element> video_balance;
    std::unique_ptr<gst::Element> video_convert;
    std::unique_ptr<gst::Element> video_scale;
    std::unique_ptr<gst::Element> caps_filter_source;

    // Black Background Source Branch
    std::unique_ptr<gst::Element> black;
    std::unique_ptr<gst::Element> caps_filter_black;

    // Video Sink Branch
    std::unique_ptr<gst::Element> compositor;
    std::unique_ptr<gst::Element> caps_filter_sink;
    std::unique_ptr<gst::Element> videosink;

    // Audio Sink Branch
    std::unique_ptr<gst::Element> audio_convert;
    std::unique_ptr<gst::Element> audio_resample;
    std::unique_ptr<gst::Element> audiosink;

    // More elements
    std::unique_ptr<gst::Pipeline> pipeline;
    std::unique_ptr<gst::Pad> sinkpad_comp_background;
    std::unique_ptr<gst::Pad> sinkpad_comp_foreground;

    static void decode_pad_added_handler(GstElement *src, GstPad *new_pad, MediaBlackbox *data);
    static GstPadProbeReturn compositor_running_callback(GstPad *pad, GstPadProbeInfo *info, gpointer user_data);
    void add_source();

  public:
    MediaBlackbox() = default;
    void init();
    void deinit();
    void set_saturation(double saturation);
    void set_alpha(double alpha);
    void start_media();
    void pause_media();
    void stop_media();
    void set_media_src(std::string file_path);
    void bus_loop();

    //TESTING METHODS
    gst::State get_pipeline_state();
    gst::State get_decodebin_state();
    double get_alpha_value();
};