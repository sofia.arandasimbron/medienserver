macro(format_file TARGET_THING)
  # Find all source files
  file(GLOB_RECURSE ALL_CXX_SOURCE_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/src/**.[ch]pp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/**.[ch]
    ${CMAKE_CURRENT_SOURCE_DIR}/include/**.[h]pp
    ${CMAKE_CURRENT_SOURCE_DIR}/include/**.[h]
  )

  # Generate Include directories
  set(INCLUDE_DIR_LIST "")
  get_target_property(dirs ${TARGET_THING} INCLUDE_DIRECTORIES)
  foreach(dir ${dirs})
      set(INCLUDE_DIR_LIST ${INCLUDE_DIR_LIST} -I ${dir})
      message("CLANG: Adding includepath ${dir} to ${TARGET_THING}")
  endforeach()

  # Run clang-tidy
  find_program(CLANG_TIDY "clang-tidy")
  if(CLANG_TIDY)
    message("CLANG-TIDY: Formatting ${TARGET_THING}")
    add_custom_target(tidy-${TARGET_THING} ALL
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      COMMAND clang-tidy
      ${ALL_CXX_SOURCE_FILES}
      --format-style=file
      --export-fixes="fixes.yaml"
      --use-color
      --
      --std=c++17
      ${INCLUDE_DIR_LIST}
    )
  endif()

  # Run clang-format
  find_program(CLANG_FORMAT "clang-format")
  if(CLANG_FORMAT)
    message("CLANG-FORMAT: Formatting ${TARGET_THING}")
    add_custom_target(format-${TARGET_THING} ALL
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      COMMAND clang-format
      ${ALL_CXX_SOURCE_FILES}
      --style=file
      --verbose
      -i
    )
  endif()
endmacro()