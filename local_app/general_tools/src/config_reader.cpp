/**
 * @file config_reader.hpp
 * @author Paul Hermann
 * @brief Reads Config.json File
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <cstdint>
#include <filesystem>
#include <fstream>
#include <config_reader.hpp>

/**
 * @brief sets given file path to read from
 *
 * @throw runtime error if parsing fails or path does not exist
 * @param file_path string
 *
 */
void ConfigReader::set_file(std::string file_path)
{
    if (std::filesystem::exists(file_path))
    {
        std::ifstream file(file_path);
        try
        {
            ConfigReader::config_file = nlohmann::json::parse(file);
        }
        catch (nlohmann::json::exception &e)
        {
            throw std::runtime_error("Json: parsing failed (" + file_path + ")");
        }
    }
    else
    {
        throw std::runtime_error("Json: path does not exist (" + file_path + ")");
    }
}

/**
 * @brief returns the value of a given key at a given position
 * @param key string
 * @throw runtime error if element doesn't exist
 * @return value of key : uint16_t
 */
uint16_t ConfigReader::read_element(std::string key)
{
    try
    {
        return ConfigReader::config_file[key];
    }
    catch (nlohmann::json::exception &e)
    {
        throw std::runtime_error("[JSON Framework] " + key + " does not exist");
    }
}

uint16_t ConfigReader::get_universe() {
    return read_element("universe");
}

uint16_t ConfigReader::get_start_address() {
    return read_element("start_address");
}

uint16_t ConfigReader::get_footprint_size() {
    return read_element("footprint_size");
}