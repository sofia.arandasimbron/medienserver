/**
 * @file file_watcher.cpp
 * @author Paul Hermann
 * @brief Calls callback, when watched file is changed
 * @version 0.1
 * @date 2022-02-07
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "file_watcher.hpp"
#include <iostream>

/**
 * @brief Monitor "file_to_watch" for changes and in case of a change execute the user supplied "action" function
 *
 * @param action Callback-function to be called
 */
void FileWatcher::start(std::string file_to_watch, std::chrono::duration<int, std::milli> delay,
                        const std::function<void()> &action)
{
    running_ = true;
    this->delay         = delay;
    this->file_to_watch = file_to_watch;
    // save initial filetime
    this->file_time     = std::filesystem::last_write_time(file_to_watch);

    // Create Thread to look for file changed
    thread = std::thread([=]() {
        // needed for stopping
        while (running_)
        {
            // Sleep Thread between file lookups
            std::this_thread::sleep_for(delay);

            // Check existence of file  
            if (std::filesystem::exists(file_to_watch))
            {
                // Get new file-write-time and compare it to the saved time
                std::filesystem::file_time_type new_file_time = std::filesystem::last_write_time(file_to_watch);
                this->file_deleted = false;
                if (file_time != new_file_time)
                {
                    // When file time has changed: Save new filetime and call callback
                    file_time = new_file_time;
                    action();
                }
            }
            else
            // If file does not exist
            {
                // Call Callback only one time per deletion
                if (!this->file_deleted)
                {
                    std::cout << "[FILEWATCHER]: File " << file_to_watch << " does not exist. Calling callback...\n";
                    file_deleted = true;
                    action();
                }
            }
        }
    });
}

/**
 * @brief Stops Listener
 *
 */
void FileWatcher::stop()
{
    running_ = false;
    thread.join();
}
