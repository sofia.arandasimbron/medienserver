/**
 * @file reader.cpp
 * @author Leena Jamil
 * @brief to read all media paths we need from a given file
 * example json file in main.cpp
 * @version 0.2
 * @date 2022-01-03
 *
 * @copyright Copyright (c) 2022
 *
 */
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <json_reader.hpp>

/**
 * @brief sets given file path to read from
 *
 * @throw runtime error if parsing fails or path does not exist
 * @param file_path string
 *
 */
void MediaReader::set_file(std::string file_path)
{
    if (std::filesystem::exists(file_path))
    {
        std::ifstream file(file_path);
        try
        {
            MediaReader::media_file = nlohmann::json::parse(file);
        }
        catch (nlohmann::json::exception &e)
        {
            throw std::runtime_error("Json: parsing failed (" + file_path + ")");
        }
    }
    else
    {
        throw std::runtime_error("Json: path does not exist (" + file_path + ")");
    }
}

/**
 * @brief checks whether file contains given value or not
 *
 * @param value string
 * @return true if file contains given value, false otherwise
 */
bool MediaReader::contains_value(std::string value)
{
    std::string file_as_string = media_file.dump();
    return file_as_string.find(value) != std::string::npos;
}

/**
 * @brief returns the value of a given key at a given position
 *
 * @param x uint8_t : index of outer list
 * @param y uint8_t : index of inner list
 * @param key string
 * @throw runtime error if element doesn't exist
 * @return value of key : string
 */
std::string MediaReader::read_element(uint8_t x, uint8_t y, std::string key)
{
    try
    {
        return MediaReader::media_file["lists"][x]["elements"][y][key];
    }
    catch (nlohmann::json::exception &e)
    {
        throw std::runtime_error("[JSON Framework] " + key + "(list: " + std::to_string(x) + ", element: " + std::to_string(y) +
                                 ") does not exist");
    }
}

/**
 * @brief checks whether an element at given position is empty or not
 *
 * @param x uint8_t : index of outer list
 * @param y uint8_t : index of inner list
 * @return true if path or identifier at given position ist empty, false otherwise
 */
bool MediaReader::is_empty(uint8_t x, uint8_t y)
{
    return (read_element(x, y, "identifier").empty() || read_element(x, y, "path").empty());
}

/**
 * @brief returns path at given position
 *
 * @param x uint8_t : index of outer list
 * @param y uint8_t : index of inner list
 * @return path : string
 */
std::string MediaReader::get_path(uint8_t x, uint8_t y)
{
    return read_element(x, y, "path");
}

/**
 * @brief returns identifier at given position
 *
 * @param x uint8_t : index of outer list
 * @param y uint8_t : index of inner list
 * @return identifier : string
 */
std::string MediaReader::get_identifier(uint8_t x, uint8_t y)
{
    return read_element(x, y, "identifier");
}