/**
 * @file json_reader.hpp
 * @author Leena Jamil
 * @brief to read all media paths we need from a given media file
 * example json file in main.cpp
 * @version 0.2
 * @date 2022-01-03
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once
#include <json.hpp>

/**
 * @brief class containing methods to set the file, return and check for its
 * elements
 *
 */
class MediaReader
{
  private:
    /**
     * @brief Internal media json file which holds all information
     *
     */
    nlohmann::json media_file;
    /**
     * @brief method to return the value of a key
     *
     */
    std::string read_element(uint8_t x, uint8_t y, std::string key);

    /**
     * @brief public methods to do the following:
     * to set the file
     * to check whether file contains a key
     * to check whether an element is empty or not
     * to return a path or identifier
     *
     */
  public:
    void set_file(std::string file_path);
    bool contains_value(std::string value);
    bool is_empty(uint8_t x, uint8_t y);
    std::string get_path(uint8_t x, uint8_t y);
    std::string get_identifier(uint8_t x, uint8_t y);
};