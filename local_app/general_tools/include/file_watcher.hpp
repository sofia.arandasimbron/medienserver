/**
 * @file file_watcher.cpp
 * @author Paul Hermann
 * @brief Calls callback, when watched file is changed
 * @version 0.1
 * @date 2022-02-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <chrono>
#include <string>
#include <functional>
#include <filesystem>
#include <thread>

class FileWatcher {
public:
    std::chrono::duration<int, std::milli> delay;
    FileWatcher() = default;
    void start(std::string file_to_watch, std::chrono::duration<int, std::milli> delay, const std::function<void ()> &action);
    void stop();
private:
    std::filesystem::file_time_type file_time;
    std::string file_to_watch;
    std::thread thread;
    bool running_ = true;
    bool file_deleted = false;
};