/**
 * @file config_reader.hpp
 * @author Paul Hermann
 * @brief Reads Config.json File
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <json.hpp>

/**
 * @brief class containing methods to set the file and return its elements
 */
class ConfigReader
{
  private:
    /**
     * @brief Internal media json file which holds all information
     */
    nlohmann::json config_file;

    uint16_t read_element(std::string key);

    /**
     * @brief public methods to do the following:
     * to set the file
     * get values for universe, start_address and footprint_size
     */
  public:
    void set_file(std::string file_path);
    uint16_t get_universe();
    uint16_t get_start_address();
    uint16_t get_footprint_size();
};