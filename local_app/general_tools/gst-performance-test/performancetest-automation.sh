#!/bin/bash

declare -a testfiles=(
                    "bbb_sunflower_720p_30fps_h264.mp4"
                    "bbb_sunflower_720p_60fps_h264.mp4"
                    "bbb_sunflower_1080p_30fps_h264.mp4"
                    "bbb_sunflower_1080p_60fps_h264.mp4"
                    )
FILEPATH="/media/usb0/BP-Testmedia/"
STARTING_COMMAND="./performancetest.sh gst-launch-1.0"
PIPELINE_BEFORE_FILE="uridecodebin uri=file://"
PIPELINE_AFTER_FILE=" ! videobalance ! videoconvert ! videoscale n-threads=4 ! video/x-raw, width=1920, height=1080, format=I420 ! queue ! compositor name=comp background=3 sink_0::zorder=2 sink_1::zorder=1 ! video/x-raw, width=1920, height=1080, format=I420 ! kmssink videotestsrc pattern=2 ! video/x-raw, width=1920, height=1080, framerate=1/1, format=I420 ! comp."

for file in "${testfiles[@]}"
do
    COMMAND="$STARTING_COMMAND $PIPELINE_BEFORE_FILE$FILEPATH$file$PIPELINE_AFTER_FILE"
    echo ""
    echo "STARTING TEST:"
    echo "$COMMAND"
    echo ""
    $COMMAND
done