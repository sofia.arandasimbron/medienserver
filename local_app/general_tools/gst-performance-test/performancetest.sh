#!/bin/bash

# Display help message
if [ $1 == "--help" ]
then
    echo "Performancetests GST"
    echo
    echo "usage : $0 EXECUTABLE"
    echo
    echo "e.g. $0 gst-launch-1.0 videotestsrc ! videoconvert ! fbdevsink"
    echo "only Applications with GST Main Loop will work"
    echo
    echo "The Performance-Graph-Files will be outputted in output/graphs"
    echo "The Pipeline-Graph-Files will be outputted in output/pipeline"
    echo
    exit
fi

#Save time and date for later file-naming
echo "Initialising measurement"
NOW=`date +'%d%m%y-%H%M%S'`

#Activate Outputfiles and set output directory
echo "Initialising folders"
unset GST_SHARK_CTF_DISABLE
mkdir output/raw -p -v
mkdir output/graphs -v
mkdir output/dotfiles -v
mkdir output/pipeline -v

#Clear Folders with tmp data
echo "Clearing temporary folders"
rm -rf output/raw/*
rm -rf output/dotfiles/*

#Setting system variables for GST Measurements and Pipeline Data
echo "Setting system variables"
export GST_DEBUG_DUMP_DOT_DIR=output/dotfiles
export GST_SHARK_LOCATION=output/raw

#Start measurement of Application
echo "Starting measurement"
GST_DEBUG="GST_TRACER:7" GST_TRACERS="framerate;cpuusage;interlatency" $@

#Generate Plot and copy to output directory
echo "Generating plots"
cd external/gst-shark/scripts/graphics
./gstshark-plot ../../../../output -s pdf -l extern
#./gstshark-plot ../../../../output -s pdf -l extern -f "source|qtdemux0_video_0|h264parse0_src|v4l2h264dec0|vdconv_src|fbdevsink0_sink"
cd ../../../../
cp external/gst-shark/scripts/graphics/tracer.pdf output/graphs/graphs-$NOW.pdf -f

#Convert Pipeline dotfile to PDF
echo "Generating pipeline-graph"
DOT_FILES_DIR="output/dotfiles"
PDF_FILES_DIR="output/pipeline"

DOT_FILES=`ls $DOT_FILES_DIR | grep PAUSED_PLAYING.dot`

for dot_file in $DOT_FILES
do
  pdf_file="pipeline-$NOW.pdf"
  dot -Tpdf $DOT_FILES_DIR/$dot_file > $PDF_FILES_DIR/$pdf_file
done