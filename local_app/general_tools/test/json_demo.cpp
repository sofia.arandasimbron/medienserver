/**
 * @file json_demo.cpp
 * @author Leena Jamil
 * @brief example of using @see MediaReader
 * @version 0.1
 * @date 2022-01-03
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "json_reader.hpp"
#include <fstream>
#include <iostream>
#include <string>

/**
 * @brief an example of how the json file must look like
 *
 * @return nlohmann::json returns an example json document
 */
nlohmann::json generate_demo_json()
{
    return nlohmann::json::parse(R"(
{
  "lists": [
    {
      "id": 1,
      "identifier": "list_1",
      "elements": [
        {
          "id": 1,
          "identifier": "first.jpg",
          "path": "../images/image_1.jpg"
        },
        {
          "id": 2,
          "identifier": "second.jpg",
          "path": "../images/image_2.jpg"
        }
      ]
    },
    {
      "id": 2,
      "identifier": "list_2",
      "elements": [
        {
          "id": 1,
          "identifier": "first.mp4",
          "path": "../videos/video_1.mp4"
        },
        {
          "id": 2,
          "identifier": "second.mp4",
          "path": "../videos/video_2.mp4"
        }
      ]
    }
  ]
}
)");
}

/**
 * @brief
 * this method creates and deletes a json file to be able to use all public
 * methods and prints the results
 * @return int
 */
int main()
{
    MediaReader media_reader;
    nlohmann::json demo_json = generate_demo_json();

    std::ofstream media_file("test_media.json");
    media_file << std::setw(1) << demo_json << std::endl;
    media_reader.set_file("test_media.json");
    std::cout << "media file set to \"test_media.json\" \n"
              << "read identifier at position 0,1: " << media_reader.get_identifier(0, 1) << '\n'
              << "read path at position 0,1: " << media_reader.get_path(0, 1) << '\n'
              << std::boolalpha << "does file contain \"video.mp4\" ? : " << media_reader.contains_value("video.mp4")
              << '\n'
              << "is position 0,1 empty? : " << media_reader.is_empty(0, 1) << '\n'
              << std::endl;
    media_file.close();
    remove("test_media.json");
    return 0;
}
