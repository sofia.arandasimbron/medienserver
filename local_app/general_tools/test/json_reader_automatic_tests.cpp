/**
 * @file json_reader_automatic_tests.cpp
 * @author Leena Jamil
 * @brief
 * @version 0.1
 * @date 2022-01-12
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "json.hpp"
#include "json_reader.hpp"
#include <exception>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>

TEST(general_tools, ReadBasicJson)
{
    const std::string json_string = R"(
    {
    "test1": "works",
    "test2": ["hello","json"]
    }
    )";

    using json = nlohmann::json;

    auto doc = json::parse(json_string);
    // Expect two strings not to be equal.
    EXPECT_STREQ(doc["test1"].get<std::string>().c_str(), "works");
    EXPECT_STREQ(doc["test2"][0].get<std::string>().c_str(), "hello");
    EXPECT_STREQ(doc["test2"][1].get<std::string>().c_str(), "json");
}

class MediaReaderTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        json_string = R"(
    {
    "lists": [
        {
        "identifier": "list_1",
        "elements": [
            {
            "identifier": "",
            "path": "../images/image_1.jpg"
            },
            {
            "identifier": "second.jpg",
            "path": "../images/image_2.jpg"
            }
        ]
        },
        {
        "identifier": "list_2",
        "elements": [
            {
            "identifier": "first.mp4",
            "path": ""
            },
            {
            "identifier": "second.mp4",
            "path": "../videos/video_2.mp4"
            }
        ]
        }
    ]
    }
    )";

        std::ofstream output_media_file("media.json");
        output_media_file << std::setw(4) << json_string << std::endl;
        output_media_file.close();
    }

    void TearDown() override
    {
        remove("media.json");
    }

  public:
    std::string json_string;
    MediaReader mr;
};

TEST_F(MediaReaderTest, set_file)
{
    // File exists
    EXPECT_NO_FATAL_FAILURE(mr.set_file("media.json"));
    // File doesnt exist
    EXPECT_THROW(mr.set_file("doesntexist"), std::runtime_error);
}

TEST_F(MediaReaderTest, contains_value)
{
    mr.set_file("media.json");
    // value doesn't exist in json
    EXPECT_FALSE(mr.contains_value("doesntexist.mp4"));
    // value exists in json
    EXPECT_TRUE(mr.contains_value("second.mp4"));
}

TEST_F(MediaReaderTest, is_empty)
{
    mr.set_file("media.json");
    // Empty Identifier
    EXPECT_TRUE(mr.is_empty(0, 0));
    // Empty Path
    EXPECT_TRUE(mr.is_empty(1, 0));
    // Both not empty
    EXPECT_FALSE(mr.is_empty(1, 1));
}

TEST_F(MediaReaderTest, get_path)
{
    mr.set_file("media.json");
    // Path exists
    EXPECT_STREQ(mr.get_path(0, 1).c_str(), "../images/image_2.jpg");
    // Element doesn't exist
    EXPECT_THROW(mr.get_path(10, 1), std::runtime_error);
}

TEST_F(MediaReaderTest, get_identifier)
{
    mr.set_file("media.json");
    // Identifier exists
    EXPECT_STREQ(mr.get_identifier(0, 1).c_str(), "second.jpg");
    // Element doesn't exist
    EXPECT_THROW(mr.get_identifier(10, 1), std::runtime_error);
}