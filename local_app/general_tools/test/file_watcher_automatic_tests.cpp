/**
 * @file file_watcher_automatic_tests.cpp
 * @author Sofia Aranda
 * @brief
 * @version 0.1
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "json.hpp"
#include "file_watcher.hpp"
#include <cstdio>
#include <exception>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>
#include <stdint.h>
#include <string>

class FileWatcherTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        std::chrono::duration<int, std::milli> delay = std::chrono::milliseconds(2);
        FileWatcher underTest;
        updated = false;
    }

    void TearDown() override
    {
        try
        {
            underTest.stop();
            remove("test_media.json");
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
        }
    }

    void create_file(std::string filename) {
        std::ofstream file(filename);
        file << std::setw(1) << "Testinhalt dieser Datei" << std::endl;
        file.close();
    }

  public:
    std::chrono::duration<int, std::milli> delay;
    FileWatcher underTest;
    bool updated;
    
};


TEST_F(FileWatcherTest, test_start_resultFailed_ifInvalidFile)
{
    //given
    //underTest object declared in the FileWatcherTest class
    //when
    //invalid path
    //then
    try
    {
        underTest.start("home/piiii/invalid.json" , delay, [=]() -> void {
            updated = true;
        });
        FAIL();
    }
    catch(const std::exception& e)
    {
        EXPECT_EQ(updated, false);
        return;
    }
}

TEST_F(FileWatcherTest, test_start_updatedTrue_ifFiledeleted)
{
    //given
    //underTest object declared in the FileWatcherTest class
    create_file("test_media.json");
    //when
    underTest.start("test_media.json", delay, [=]() -> void {
        updated = true;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    remove("test_media.json");
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //then
    EXPECT_EQ(updated, true);
}

TEST_F(FileWatcherTest, test_start_updatedTrue_ifValidFileAndModified)
{
    //given
    //underTest object declared in the FileWatcherTest class
    create_file("test_media.json");
    //when
    underTest.start("test_media.json", delay, [=]() -> void {
        updated = true;
    });

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    create_file("test_media.json");

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //then
    EXPECT_EQ(updated, true);
}

TEST_F(FileWatcherTest, test_start_updateFalse_ifValidFileAndNotModified)
{
    //given
    //underTest object declared in the FileWatcherTest class
    create_file("test_media.json");
    //when
    underTest.start("test_media.json", delay, [=]() -> void {
        updated = true;
    });

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //then
    EXPECT_EQ(updated, false);
}

TEST_F(FileWatcherTest, test_start_updateTrue_ifStartedAgainOnValidFile)
{
    //given
    //underTest object declared in the FileWatcherTest class
    create_file("test_media.json");
    //when
    underTest.start("test_media.json", delay, [=]() -> void {
        updated = true;
    });
    
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //then
    EXPECT_EQ(updated, false);
    
    underTest.stop();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    underTest.start("test_media.json", delay, [=]() -> void {
        updated = true;
    });
    
    create_file("test_media.json");

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    EXPECT_EQ(updated, true);   
}