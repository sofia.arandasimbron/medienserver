/**
 * @file file_watcher_demo.cpp
 * @author Sofia Aranda
 * @brief example of using FileWatcher
 * @version 0.1
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "file_watcher.hpp"
#include <fstream>
#include <iostream>
#include <ratio>
#include <string>
#include <thread>

/**
 * @brief
 * this method creates and deletes a json file to be able to use all public
 * methods and prints the results
 * @return int
 */
int main()
{
    FileWatcher file_watcher;
    std::chrono::duration<int, std::milli> delay = std::chrono::milliseconds(2);

    // Create test_media.json file
    std::ofstream file_to_watch("test_media.json");
    file_to_watch << std::setw(1) << "Testinhalt dieser Datei" << std::endl;
    file_to_watch.close();

    // Start filewatcher
    file_watcher.start("test_media.json", delay, [=]() -> void {
        std::cout << "[CORE] Detected update in mediafile" << std::endl;
    });
    std::cout << "file to watch set to \"test_media.json\" \n" << std::endl;

    // Press key to end application and delete file
    std::cin.get();
    file_watcher.stop();
    remove("test_media.json");
    return 0;
}