/**
 * @file config_reader_automatic_tests.cpp
 * @author Sofia Aranda
 * @brief
 * @version 0.1
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "json.hpp"
#include "config_reader.hpp"
#include <exception>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>
#include <stdint.h>

class ConfigReaderTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        json_valid_string = R"(
      {
        "universe": 42,
        "start_address": 43,
        "footprint_size": 44
      }
      )";

        std::ofstream output_media_file("media.json");
        output_media_file << std::setw(4) << json_valid_string << std::endl;
        output_media_file.close();

        json_invalid_universe_string = R"(
      {
        "start_address": 43, 
        "footprint_size": 44
      }
      )";

        std::ofstream output_invalid_universe_jason("invalid_universe.json");
        output_invalid_universe_jason << std::setw(4) << json_invalid_universe_string << std::endl;
        output_invalid_universe_jason.close();
        
        json_invalid_start_address_string = R"(
      {
        "universe": 42,
        "footprint_size": 44
      }
      )";

        std::ofstream output_invalid_start_address_jason("invalid_start_address.json");
        output_invalid_start_address_jason << std::setw(4) << json_invalid_start_address_string << std::endl;
        output_invalid_start_address_jason.close();

        json_invalid_footprint_size_string = R"(
      {
        "universe": 42,
        "start_address": 43
      }
      )";

        std::ofstream output_invalid_footprint_size_jason("invalid_footprint_size.json");
        output_invalid_footprint_size_jason << std::setw(4) << json_invalid_footprint_size_string << std::endl;
        output_invalid_footprint_size_jason.close();
    }

    void TearDown() override
    {
        remove("media.json");
        remove("invalid_universe.json");
        remove("invalid_start_address.json");
        remove("invalid_footprint_size.json");
    }

  public:
    std::string json_valid_string;
    std::string json_invalid_universe_string;
    std::string json_invalid_start_address_string;
    std::string json_invalid_footprint_size_string;
    ConfigReader underTest;
};

TEST_F(ConfigReaderTest, test_set_file_resultOk_ifJsonValid)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    //setting a valid Json
    //then
    EXPECT_NO_FATAL_FAILURE(underTest.set_file("media.json"));
}

TEST_F(ConfigReaderTest, test_set_file_resultFailed_ifJsonInvalid)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    //setting an invalid Json
    //then
    EXPECT_THROW(underTest.set_file("doesntexist"), std::runtime_error);
}

TEST_F(ConfigReaderTest, test_read_universe_resultOk_ifJsonValidAndElementExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("media.json");
    //then
    EXPECT_EQ(underTest.get_universe(), 42);
}

TEST_F(ConfigReaderTest, test_read_universe_resultFailed_ifJsonValidAndElementDoesNotExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("invalid_universe.json");
    //then
    EXPECT_THROW(underTest.get_universe(), std::runtime_error);
}

TEST_F(ConfigReaderTest, test_read_start_address_resultOk_ifJsonValidAndElementExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("media.json");
    //then
    EXPECT_EQ(underTest.get_start_address(), 43);
}

TEST_F(ConfigReaderTest, test_read_start_address_resultFailed_ifJsonValidAndElementDoesNotExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("invalid_start_address.json");
    //then
    EXPECT_THROW(underTest.get_start_address(), std::runtime_error);
}

TEST_F(ConfigReaderTest, test_read_footprint_size_resultOk_ifJsonValidAndElementExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("media.json");
    //then
    EXPECT_EQ(underTest.get_footprint_size(), 44);
}

TEST_F(ConfigReaderTest, test_read_footprint_size_resultFailed_ifJsonValidAndElementDoesNotExists)
{
    //given
    //underTest object declared in the ConfigReaderTest class
    //when
    underTest.set_file("invalid_footprint_size.json");
    //then
    EXPECT_THROW(underTest.get_footprint_size(), std::runtime_error);
}