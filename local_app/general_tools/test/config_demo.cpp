/**
 * @file config_demo.cpp
 * @author Paul Hermann
 * @brief Demo of config reader usage
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "config_reader.hpp"
#include <fstream>
#include <iostream>
#include <string>

/**
 * @brief an example of how the json file must look like
 *
 * @return nlohmann::json returns an example json document
 */
nlohmann::json generate_demo_json()
{
    return nlohmann::json::parse(R"(
      {
        "universe": 42,
        "start_address": 43,
        "footprint_size": 44
      }
      )");
}

/**
 * @brief
 * this method creates and deletes a json file to be able to use all public
 * methods and prints the results
 * @return int
 */
int main()
{
    ConfigReader config_reader;
    nlohmann::json demo_json = generate_demo_json();

    std::ofstream config_file("test_config.json");
    config_file << std::setw(1) << demo_json << std::endl;
    config_reader.set_file("test_config.json");
    std::cout << "config file set to \"test_config.json\" \n"
              << "read universe: " << config_reader.get_universe() << '\n'
              << "read start_address: " << config_reader.get_start_address() << '\n'
              << "read footprint_size: " << config_reader.get_footprint_size() << '\n'
              << std::endl;
    config_file.close();
    remove("test_config.json");
    return 0;
}
