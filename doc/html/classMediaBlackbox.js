var classMediaBlackbox =
[
    [ "MediaBlackbox", "classMediaBlackbox.html#a9fc612bd5058aaccc9134e2965a5b125", null ],
    [ "bus_loop", "classMediaBlackbox.html#ada1c5730f5e98336a63ff22abcbd1ea6", null ],
    [ "get_pipeline_state", "classMediaBlackbox.html#aa267457fa1d640a4a92da0588b875454", null ],
    [ "pause_media", "classMediaBlackbox.html#a2aab2cedb799e30419eb1892e02dfb11", null ],
    [ "set_media_src", "classMediaBlackbox.html#a62c6e8f750e5ed03862f6b09c3ec9811", null ],
    [ "set_saturation", "classMediaBlackbox.html#a364cc46ae31617c55f31ace0567fae33", null ],
    [ "start_media", "classMediaBlackbox.html#a91b00710fe30dc390c6a10d697ed2693", null ],
    [ "stop_media", "classMediaBlackbox.html#af26eb9f26f51ac6ec06b7c9314bf3ab4", null ],
    [ "audio_convert", "classMediaBlackbox.html#ac54982cf05440624c7097dce89d4b5da", null ],
    [ "audio_resample", "classMediaBlackbox.html#aa0b255736b1c9b45ca2c593fa064797f", null ],
    [ "audiosink", "classMediaBlackbox.html#a5c1f40630b8268f8f9842e57faf75b75", null ],
    [ "current_path", "classMediaBlackbox.html#a4757e0d43f14f770a907880f858b2789", null ],
    [ "pipeline", "classMediaBlackbox.html#a6775aed081e1453f302aa07a3f95b54d", null ],
    [ "source", "classMediaBlackbox.html#a816690f94dbf719181057d74e12c8b9d", null ],
    [ "video_balance", "classMediaBlackbox.html#a01ae378a2f32ba1cefb4d6837a919eec", null ],
    [ "video_convert", "classMediaBlackbox.html#a8769511f5c30ed3fe8131088685be834", null ],
    [ "videosink", "classMediaBlackbox.html#a9f252da9e0adfd6d048644d95bd63f82", null ]
];