/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Media Server", "index.html", [
    [ "Core", "md__home_pi_medienserver_local_app_media_framework_gstreamer_cpp_include_core_README.html", null ],
    [ "gstreamer-cpp", "md__home_pi_medienserver_local_app_media_framework_gstreamer_cpp_README.html", null ],
    [ "GStreamer Notes", "md__home_pi_medienserver_local_app_media_framework_README.html", [
      [ "gst_bin_add(_many)", "md__home_pi_medienserver_local_app_media_framework_README.html#autotoc_md3", null ]
    ] ],
    [ "todo", "md__home_pi_medienserver_local_app_media_framework_todo.html", null ],
    [ "Media Server Local", "md__home_pi_medienserver_local_app_README.html", [
      [ "Architecture", "md__home_pi_medienserver_local_app_README.html#autotoc_md5", null ]
    ] ],
    [ "How to become a contributor and submit your own code", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html", [
      [ "Contributor License Agreements", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md7", null ],
      [ "Are you a Googler?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md8", null ],
      [ "Contributing A Patch", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md9", null ],
      [ "The Google Test and Google Mock Communities", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md10", [
        [ "Please Be Friendly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md11", null ]
      ] ],
      [ "Style", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md12", null ],
      [ "Requirements for Contributors", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md13", null ],
      [ "Developing Google Test and Google Mock", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md14", [
        [ "Testing Google Test and Google Mock Themselves", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_CONTRIBUTING.html#autotoc_md15", null ]
      ] ]
    ] ],
    [ "Advanced googletest Topics", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html", [
      [ "Introduction", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md17", null ],
      [ "More Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md18", [
        [ "Explicit Success and Failure", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md19", null ],
        [ "Exception Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md20", null ],
        [ "Predicate Assertions for Better Error Messages", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md21", [
          [ "Using an Existing Boolean Function", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md22", null ],
          [ "Using a Function That Returns an AssertionResult", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md23", null ],
          [ "Using a Predicate-Formatter", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md24", null ]
        ] ],
        [ "Floating-Point Comparison", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md25", [
          [ "Floating-Point Macros", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md26", null ],
          [ "Floating-Point Predicate-Format Functions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md27", null ]
        ] ],
        [ "Asserting Using gMock Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md28", null ],
        [ "More String Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md29", null ],
        [ "Windows HRESULT assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md30", null ],
        [ "Type Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md31", null ],
        [ "Assertion Placement", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md32", null ]
      ] ],
      [ "Teaching googletest How to Print Your Values", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md33", null ],
      [ "Death Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md34", [
        [ "How to Write a Death Test", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md35", null ],
        [ "Death Test Naming", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md36", null ],
        [ "Regular Expression Syntax", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md37", null ],
        [ "How It Works", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md38", null ],
        [ "Death Tests And Threads", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md39", null ],
        [ "Death Test Styles", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md40", null ],
        [ "Caveats", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md41", null ]
      ] ],
      [ "Using Assertions in Sub-routines", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md42", [
        [ "Adding Traces to Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md43", null ],
        [ "Propagating Fatal Failures", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md44", [
          [ "Asserting on Subroutines with an exception", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md45", null ],
          [ "Asserting on Subroutines", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md46", null ],
          [ "Checking for Failures in the Current Test", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md47", null ]
        ] ]
      ] ],
      [ "Logging Additional Information", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md48", null ],
      [ "Sharing Resources Between Tests in the Same Test Suite", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md49", null ],
      [ "Global Set-Up and Tear-Down", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md50", null ],
      [ "Value-Parameterized Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md51", [
        [ "How to Write Value-Parameterized Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md52", null ],
        [ "Creating Value-Parameterized Abstract Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md53", null ],
        [ "Specifying Names for Value-Parameterized Test Parameters", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md54", null ]
      ] ],
      [ "Typed Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md55", null ],
      [ "Type-Parameterized Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md56", null ],
      [ "Testing Private Code", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md57", null ],
      [ "\"Catching\" Failures", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md58", null ],
      [ "Registering tests programmatically", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md59", null ],
      [ "Getting the Current Test's Name", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md60", null ],
      [ "Extending googletest by Handling Test Events", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md61", [
        [ "Defining Event Listeners", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md62", null ],
        [ "Using Event Listeners", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md63", null ],
        [ "Generating Failures in Listeners", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md64", null ]
      ] ],
      [ "Running Test Programs: Advanced Options", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md65", [
        [ "Selecting Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md66", [
          [ "Listing Test Names", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md67", null ],
          [ "Running a Subset of the Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md68", null ],
          [ "Stop test execution upon first failure", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md69", null ],
          [ "Temporarily Disabling Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md70", null ],
          [ "Temporarily Enabling Disabled Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md71", null ]
        ] ],
        [ "Repeating the Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md72", null ],
        [ "Shuffling the Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md73", null ],
        [ "Controlling Test Output", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md74", [
          [ "Colored Terminal Output", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md75", null ],
          [ "Suppressing test passes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md76", null ],
          [ "Suppressing the Elapsed Time", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md77", null ],
          [ "Suppressing UTF-8 Text Output", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md78", null ],
          [ "Generating an XML Report", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md79", null ],
          [ "Generating a JSON Report", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md80", null ]
        ] ],
        [ "Controlling How Failures Are Reported", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md81", [
          [ "Detecting Test Premature Exit", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md82", null ],
          [ "Turning Assertion Failures into Break-Points", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md83", null ],
          [ "Disabling Catching Test-Thrown Exceptions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md84", null ]
        ] ],
        [ "Sanitizer Integration", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_advanced.html#autotoc_md85", null ]
      ] ]
    ] ],
    [ "Community-Created Documentation", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_community_created_documentation.html", null ],
    [ "Googletest FAQ", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html", [
      [ "Why should test suite names and test names not contain underscore?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md88", null ],
      [ "Why does googletest support <tt>EXPECT_EQ(NULL, ptr)</tt> and <tt>ASSERT_EQ(NULL, ptr)</tt> but not <tt>EXPECT_NE(NULL, ptr)</tt> and <tt>ASSERT_NE(NULL, ptr)</tt>?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md89", null ],
      [ "I need to test that different implementations of an interface satisfy some common requirements. Should I use typed tests or value-parameterized tests?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md90", null ],
      [ "I got some run-time errors about invalid proto descriptors when using <tt>ProtocolMessageEquals</tt>. Help!", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md91", null ],
      [ "My death test modifies some state, but the change seems lost after the death test finishes. Why?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md92", null ],
      [ "EXPECT_EQ(htonl(blah), blah_blah) generates weird compiler errors in opt mode. Is this a googletest bug?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md93", null ],
      [ "The compiler complains about \"undefined references\" to some static const member variables, but I did define them in the class body. What's wrong?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md94", null ],
      [ "Can I derive a test fixture from another?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md95", null ],
      [ "My compiler complains \"void value not ignored as it ought to be.\" What does this mean?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md96", null ],
      [ "My death test hangs (or seg-faults). How do I fix it?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md97", null ],
      [ "Should I use the constructor/destructor of the test fixture or SetUp()/TearDown()?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#CtorVsSetUp", null ],
      [ "The compiler complains \"no matching function to call\" when I use ASSERT_PRED*. How do I fix it?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md98", null ],
      [ "My compiler complains about \"ignoring return value\" when I call RUN_ALL_TESTS(). Why?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md99", null ],
      [ "My compiler complains that a constructor (or destructor) cannot return a value. What's going on?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md100", null ],
      [ "My SetUp() function is not called. Why?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md101", null ],
      [ "I have several test suites which share the same test fixture logic, do I have to define a new test fixture class for each of them? This seems pretty tedious.", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md102", null ],
      [ "googletest output is buried in a whole bunch of LOG messages. What do I do?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md103", null ],
      [ "Why should I prefer test fixtures over global variables?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md104", null ],
      [ "What can the statement argument in ASSERT_DEATH() be?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md105", null ],
      [ "I have a fixture class <tt>FooTest</tt>, but <tt>TEST_F(FooTest, Bar)</tt> gives me error <tt>\"no matching function for call to `FooTest::FooTest()'\"</tt>. Why?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md106", null ],
      [ "Why does ASSERT_DEATH complain about previous threads that were already joined?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md107", null ],
      [ "Why does googletest require the entire test suite, instead of individual tests, to be named *DeathTest when it uses ASSERT_DEATH?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md108", null ],
      [ "But I don't like calling my entire test suite *DeathTest when it contains both death tests and non-death tests. What do I do?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md109", null ],
      [ "googletest prints the LOG messages in a death test's child process only when the test fails. How can I see the LOG messages when the death test succeeds?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md110", null ],
      [ "The compiler complains about \"no match for 'operator<<'\" when I use an assertion. What gives?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md111", null ],
      [ "How do I suppress the memory leak messages on Windows?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md112", null ],
      [ "How can my code detect if it is running in a test?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md113", null ],
      [ "How do I temporarily disable a test?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md114", null ],
      [ "Is it OK if I have two separate <tt>TEST(Foo, Bar)</tt> test methods defined in different namespaces?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_faq.html#autotoc_md115", null ]
    ] ],
    [ "gMock Cheat Sheet", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html", [
      [ "Defining a Mock Class", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md117", [
        [ "Mocking a Normal Class", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#MockClass", null ],
        [ "Mocking a Class Template", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#MockTemplate", null ],
        [ "Specifying Calling Conventions for Mock Functions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md118", null ]
      ] ],
      [ "Using Mocks in Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#UsingMocks", null ],
      [ "Setting Default Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#OnCall", null ],
      [ "Setting Expectations", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#ExpectCall", null ],
      [ "Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#MatcherList", [
        [ "Wildcard", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md119", null ],
        [ "Generic Comparison", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md120", null ],
        [ "Floating-Point Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#FpMatchers", null ],
        [ "String Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md121", null ],
        [ "Container Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md122", null ],
        [ "Member Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md123", null ],
        [ "Matching the Result of a Function, Functor, or Callback", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md124", null ],
        [ "Pointer Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md125", null ],
        [ "Multi-argument Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#MultiArgMatchers", null ],
        [ "Composite Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md126", null ],
        [ "Adapters for Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md127", null ],
        [ "Using Matchers as Predicates", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#MatchersAsPredicatesCheat", null ],
        [ "Defining Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md128", null ]
      ] ],
      [ "Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#ActionList", [
        [ "Returning a Value", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md129", null ],
        [ "Side Effects", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md130", null ],
        [ "Using a Function, Functor, or Lambda as an Action", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md131", null ],
        [ "Default Action", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md132", null ],
        [ "Composite Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md133", null ],
        [ "Defining Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md134", null ]
      ] ],
      [ "Cardinalities", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#CardinalityList", null ],
      [ "Expectation Order", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md135", [
        [ "The After Clause", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#AfterClause", null ],
        [ "Sequences", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#UsingSequences", null ]
      ] ],
      [ "Verifying and Resetting a Mock", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md136", null ],
      [ "Mock Classes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md137", null ],
      [ "Flags", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cheat_sheet.html#autotoc_md138", null ]
    ] ],
    [ "gMock Cookbook", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html", [
      [ "Creating Mock Classes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md140", [
        [ "Dealing with unprotected commas", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md141", null ],
        [ "Mocking Private or Protected Methods", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md142", null ],
        [ "Mocking Overloaded Methods", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md143", null ],
        [ "Mocking Class Templates", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md144", null ],
        [ "Mocking Non-virtual Methods", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#MockingNonVirtualMethods", null ],
        [ "Mocking Free Functions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md145", null ],
        [ "Old-Style <tt>MOCK_METHODn</tt> Macros", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md146", null ],
        [ "The Nice, the Strict, and the Naggy", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#NiceStrictNaggy", null ],
        [ "Simplifying the Interface without Breaking Existing Code", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#SimplerInterfaces", null ],
        [ "Alternative to Mocking Concrete Classes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md147", null ],
        [ "Delegating Calls to a Fake", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#DelegatingToFake", null ],
        [ "Delegating Calls to a Real Object", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md148", null ],
        [ "Delegating Calls to a Parent Class", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md149", null ]
      ] ],
      [ "Using Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md150", [
        [ "Matching Argument Values Exactly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md151", null ],
        [ "Using Simple Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md152", null ],
        [ "Combining Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#CombiningMatchers", null ],
        [ "Casting Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#SafeMatcherCast", null ],
        [ "Selecting Between Overloaded Functions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#SelectOverload", null ],
        [ "Performing Different Actions Based on the Arguments", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md153", null ],
        [ "Matching Multiple Arguments as a Whole", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md154", null ],
        [ "Using Matchers as Predicates", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md155", null ],
        [ "Using Matchers in googletest Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md156", null ],
        [ "Using Predicates as Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md157", null ],
        [ "Matching Arguments that Are Not Copyable", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md158", null ],
        [ "Validating a Member of an Object", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md159", null ],
        [ "Validating the Value Pointed to by a Pointer Argument", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md160", null ],
        [ "Testing a Certain Property of an Object", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md161", null ],
        [ "Matching Containers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md162", null ],
        [ "Sharing Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md163", null ],
        [ "Matchers must have no side-effects", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#PureMatchers", null ]
      ] ],
      [ "Setting Expectations", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md164", [
        [ "Knowing When to Expect", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#UseOnCall", null ],
        [ "Ignoring Uninteresting Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md165", null ],
        [ "Disallowing Unexpected Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md166", null ],
        [ "Understanding Uninteresting vs Unexpected Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#uninteresting-vs-unexpected", null ],
        [ "Ignoring Uninteresting Arguments", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#ParameterlessExpectations", null ],
        [ "Expecting Ordered Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#OrderedCalls", null ],
        [ "Expecting Partially Ordered Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#PartialOrder", null ],
        [ "Controlling When an Expectation Retires", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md167", null ]
      ] ],
      [ "Using Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md168", [
        [ "Returning References from Mock Methods", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md169", null ],
        [ "Returning Live Values from Mock Methods", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md170", null ],
        [ "Combining Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md171", null ],
        [ "Verifying Complex Arguments", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#SaveArgVerify", null ],
        [ "Mocking Side Effects", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#MockingSideEffects", null ],
        [ "Changing a Mock Object's Behavior Based on the State", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md172", null ],
        [ "Setting the Default Value for a Return Type", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#DefaultValue", null ],
        [ "Setting the Default Actions for a Mock Method", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md173", null ],
        [ "Using Functions/Methods/Functors/Lambdas as Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#FunctionsAsActions", null ],
        [ "Using Functions with Extra Info as Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md174", null ],
        [ "Invoking a Function/Method/Functor/Lambda/Callback Without Arguments", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md175", null ],
        [ "Invoking an Argument of the Mock Function", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md176", null ],
        [ "Ignoring an Action's Result", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md177", null ],
        [ "Selecting an Action's Arguments", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#SelectingArgs", null ],
        [ "Ignoring Arguments in Action Functions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md178", null ],
        [ "Sharing Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md179", null ],
        [ "Testing Asynchronous Behavior", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md180", null ]
      ] ],
      [ "Misc Recipes on Using gMock", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md181", [
        [ "Mocking Methods That Use Move-Only Types", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md182", [
          [ "Legacy workarounds for move-only types", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#LegacyMoveOnly", null ]
        ] ],
        [ "Making the Compilation Faster", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md183", null ],
        [ "Forcing a Verification", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md184", null ],
        [ "Using Check Points", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#UsingCheckPoints", null ],
        [ "Mocking Destructors", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md185", null ],
        [ "Using gMock and Threads", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#UsingThreads", null ],
        [ "Controlling How Much Information gMock Prints", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md186", null ],
        [ "Gaining Super Vision into Mock Calls", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md187", null ],
        [ "Running Tests in Emacs", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md188", null ]
      ] ],
      [ "Extending gMock", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md189", [
        [ "Writing New Matchers Quickly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#NewMatchers", null ],
        [ "Writing New Parameterized Matchers Quickly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md190", null ],
        [ "Writing New Monomorphic Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md191", null ],
        [ "Writing New Polymorphic Matchers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md192", null ],
        [ "Legacy Matcher Implementation", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md193", null ],
        [ "Writing New Cardinalities", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md194", null ],
        [ "Writing New Actions Quickly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#QuickNewActions", [
          [ "Legacy macro-based Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md195", null ],
          [ "Legacy macro-based parameterized Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md196", null ]
        ] ],
        [ "Restricting the Type of an Argument or Parameter in an ACTION", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md197", null ],
        [ "Writing New Action Templates Quickly", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md198", null ],
        [ "Using the ACTION Object's Type", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md199", null ],
        [ "Writing New Monomorphic Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#NewMonoActions", null ],
        [ "Writing New Polymorphic Actions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#NewPolyActions", null ],
        [ "Teaching gMock How to Print Your Values", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md200", null ]
      ] ],
      [ "Useful Mocks Created Using gMock", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md201", [
        [ "Mock std::function", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#MockFunction", null ]
      ] ]
    ] ],
    [ "Legacy gMock FAQ", "GMockFaq.html", null ],
    [ "gMock for Dummies", "GMockForDummies.html", [
      [ "What Is gMock?", "GMockForDummies.html#autotoc_md220", null ],
      [ "Why gMock?", "GMockForDummies.html#autotoc_md221", null ],
      [ "Getting Started", "GMockForDummies.html#autotoc_md222", null ],
      [ "A Case for Mock Turtles", "GMockForDummies.html#autotoc_md223", null ],
      [ "Writing the Mock Class", "GMockForDummies.html#autotoc_md224", [
        [ "When I call a method on my mock object, the method for the real object is invoked instead. What's the problem?", "GMockFaq.html#autotoc_md202", null ],
        [ "Can I mock a variadic function?", "GMockFaq.html#autotoc_md203", null ],
        [ "MSVC gives me warning C4301 or C4373 when I define a mock method with a const parameter. Why?", "GMockFaq.html#autotoc_md204", null ],
        [ "I can't figure out why gMock thinks my expectations are not satisfied. What should I do?", "GMockFaq.html#autotoc_md205", null ],
        [ "My program crashed and <tt>ScopedMockLog</tt> spit out tons of messages. Is it a gMock bug?", "GMockFaq.html#autotoc_md206", null ],
        [ "How can I assert that a function is NEVER called?", "GMockFaq.html#autotoc_md207", null ],
        [ "I have a failed test where gMock tells me TWICE that a particular expectation is not satisfied. Isn't this redundant?", "GMockFaq.html#autotoc_md208", null ],
        [ "I get a heapcheck failure when using a mock object, but using a real object is fine. What can be wrong?", "GMockFaq.html#autotoc_md209", null ],
        [ "The \"newer expectations override older ones\" rule makes writing expectations awkward. Why does gMock do that?", "GMockFaq.html#autotoc_md210", null ],
        [ "gMock prints a warning when a function without EXPECT_CALL is called, even if I have set its behavior using ON_CALL. Would it be reasonable not to show the warning in this case?", "GMockFaq.html#autotoc_md211", null ],
        [ "How can I delete the mock function's argument in an action?", "GMockFaq.html#autotoc_md212", null ],
        [ "How can I perform an arbitrary action on a mock function's argument?", "GMockFaq.html#autotoc_md213", null ],
        [ "My code calls a static/global function. Can I mock it?", "GMockFaq.html#autotoc_md214", null ],
        [ "My mock object needs to do complex stuff. It's a lot of pain to specify the actions. gMock sucks!", "GMockFaq.html#autotoc_md215", null ],
        [ "I got a warning \"Uninteresting function call encountered - default action taken..\" Should I panic?", "GMockFaq.html#autotoc_md216", null ],
        [ "I want to define a custom action. Should I use Invoke() or implement the ActionInterface interface?", "GMockFaq.html#autotoc_md217", null ],
        [ "I use SetArgPointee() in WillOnce(), but gcc complains about \"conflicting return type specified\". What does it mean?", "GMockFaq.html#autotoc_md218", null ],
        [ "I have a huge mock class, and Microsoft Visual C++ runs out of memory when compiling it. What can I do?", "GMockFaq.html#autotoc_md219", null ],
        [ "How to Define It", "GMockForDummies.html#autotoc_md225", null ],
        [ "Where to Put It", "GMockForDummies.html#autotoc_md226", null ]
      ] ],
      [ "Using Mocks in Tests", "GMockForDummies.html#autotoc_md227", null ],
      [ "Setting Expectations", "GMockForDummies.html#autotoc_md228", [
        [ "General Syntax", "GMockForDummies.html#autotoc_md229", null ],
        [ "Matchers: What Arguments Do We Expect?", "GMockForDummies.html#autotoc_md230", null ],
        [ "Cardinalities: How Many Times Will It Be Called?", "GMockForDummies.html#autotoc_md231", null ],
        [ "Actions: What Should It Do?", "GMockForDummies.html#autotoc_md232", null ],
        [ "Using Multiple Expectations", "GMockForDummies.html#MultiExpectations", null ],
        [ "All Expectations Are Sticky (Unless Said Otherwise)", "GMockForDummies.html#StickyExpectations", null ],
        [ "Uninteresting Calls", "GMockForDummies.html#autotoc_md233", null ]
      ] ]
    ] ],
    [ "Using GoogleTest from various build systems", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_pkgconfig.html", null ],
    [ "Googletest Primer", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html", [
      [ "Introduction: Why googletest?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md239", null ],
      [ "Beware of the nomenclature", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md240", null ],
      [ "Basic Concepts", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md241", null ],
      [ "Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md242", [
        [ "CMake", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_pkgconfig.html#autotoc_md235", null ],
        [ "Help! pkg-config can't find GoogleTest!", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_pkgconfig.html#autotoc_md236", null ],
        [ "Using pkg-config in a cross-compilation setting", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_pkgconfig.html#autotoc_md237", null ],
        [ "Basic Assertions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md243", null ],
        [ "Binary Comparison", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md244", null ],
        [ "String Comparison", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md245", null ]
      ] ],
      [ "Simple Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md246", null ],
      [ "Test Fixtures: Using the Same Data Configuration for Multiple Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#same-data-multiple-tests", null ],
      [ "Invoking the Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md247", null ],
      [ "Writing the main() Function", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md248", null ],
      [ "Known Limitations", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_primer.html#autotoc_md249", null ]
    ] ],
    [ "Googletest Samples", "samples.html", null ],
    [ "Content Moved", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_docs_README.html", null ],
    [ "Customization Points", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_include_gmock_internal_custom_README.html", [
      [ "Header <tt>gmock-port.h</tt>", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_include_gmock_internal_custom_README.html#autotoc_md252", [
        [ "Flag related macros:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_include_gmock_internal_custom_README.html#autotoc_md253", null ]
      ] ]
    ] ],
    [ "Googletest Mocking (gMock) Framework", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_README.html", null ],
    [ "Please Note:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_scripts_README.html", null ],
    [ "Content Moved", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_docs_README.html", null ],
    [ "Customization Points", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html", [
      [ "Header <tt>gtest.h</tt>", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md259", [
        [ "Overview", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googlemock_README.html#autotoc_md255", null ],
        [ "The following macros can be defined:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md260", null ]
      ] ],
      [ "Header <tt>gtest-port.h</tt>", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md261", [
        [ "Flag related macros:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md262", null ],
        [ "Logging:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md263", null ],
        [ "Threading:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md264", null ],
        [ "Underlying library support features", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md265", null ],
        [ "Exporting API symbols:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md266", null ]
      ] ],
      [ "Header <tt>gtest-printers.h</tt>", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_include_gtest_internal_custom_README.html#autotoc_md267", null ]
    ] ],
    [ "Generic Build Instructions", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html", null ],
    [ "Please Note:", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_scripts_README.html", null ],
    [ "GoogleTest", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html", [
      [ "Welcome to <strong>GoogleTest</strong>, Google's C++ test framework!", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md286", [
        [ "Build with CMake", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md270", [
          [ "Setup", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md269", null ],
          [ "Standalone CMake Project", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md271", null ],
          [ "Incorporating Into An Existing CMake Project", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md272", [
            [ "Visual Studio Dynamic vs Static Runtimes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md273", null ]
          ] ],
          [ "C++ Standard Version", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md274", null ]
        ] ],
        [ "Tweaking GoogleTest", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md275", null ],
        [ "Multi-threaded Tests", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md276", null ],
        [ "As a Shared Library (DLL)", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md277", null ],
        [ "Avoiding Macro Name Clashes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_googletest_README.html#autotoc_md278", [
          [ "OSS Builds Status", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md282", null ]
        ] ],
        [ "Announcements", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md283", [
          [ "Release 1.10.x", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md284", null ],
          [ "Coming Soon", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md285", null ]
        ] ],
        [ "Getting Started", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md287", null ]
      ] ],
      [ "Features", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md288", null ],
      [ "Supported Platforms", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md289", [
        [ "Operating Systems", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md290", null ],
        [ "Compilers", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md291", null ],
        [ "Build Systems", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md292", null ]
      ] ],
      [ "Who Is Using GoogleTest?", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md293", null ],
      [ "Related Open Source Projects", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md294", null ],
      [ "Contributing Changes", "md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_README.html#autotoc_md295", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Bin_8hpp_source.html",
"classMediaReader.html",
"classcpp_1_1ast_1_1AstBuilder.html#ab183aa48e4b6e116379f95eb3d11039c",
"classgoogletest-env-var-test_1_1GTestEnvVarTest.html#ae0165e3c30ce525c4d2f653e8f27ed3c",
"classtesting_1_1CodeLocationForTYPEDTESTP.html",
"classtesting_1_1gmock__function__mocker__test_1_1LegacyMockB.html",
"classtesting_1_1internal_1_1EventRecordingListener2.html#aefd67a80de94cdd8e2e43c0fad812bd2",
"classtesting_1_1internal_1_1TypeParameterizedTestSuite.html",
"gmock-function-mocker__test_8cc.html#a31ed39f17667619d800693a7eb90682e",
"gmock-more-actions__test_8cc.html#af5a15dbcba14010a5463f3ac9451ab8a",
"googletest-output-test___8cc.html#a23b15d10e91f920a5ed07ae9f27916df",
"gtest-internal_8h.html#a26323b4c2a29ea8e187aafbd4d2275db",
"gtest_8cc.html#a19b35b39782d41e6ef76e1910a3a502e",
"keywords_8py.html#a2665fb8a25a4dae03fa5d3dc975c537c",
"md__home_pi_medienserver_local_app_UILD_DOC_On__deps_googletest_src_docs_gmock_cook_book.html#autotoc_md185",
"structtesting_1_1Flags.html#a89b3c85eff50bc081ab6c590359ad190",
"structtesting_1_1internal_1_1is__proxy__type__list.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';