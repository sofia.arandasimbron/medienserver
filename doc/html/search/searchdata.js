var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwx~",
  1: "_abcdefghijklmnopqrstuvwx",
  2: "g",
  3: "dfjmp",
  4: "_abcdfgimnprstuv~",
  5: "cil",
  6: "v",
  7: "d",
  8: "acghlmptu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Pages"
};

