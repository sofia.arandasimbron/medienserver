# Code Review
durchgeführt von Paul Hermann am 02.02.2022 in Iteration 5

## Reviewziel
Gereviewed wurden folgende Codedateien:
/web_app/backend/ListManager.ts
/web_app/frontend/src/app/component/*
/web_app/frontend/src/app/services/api/api/api.ts
/web_app/frontend/src/app/services/api/.gitignore
/web_app/frontend/src/app/List.ts
/web_app/frontend/src/index.html
/web_app/frontend/angular.json
/web_app/frontend/package.json
/web_app/openapi.yml

Außerdem wurde die Ausführung des gesamten Webservers getestet.

## Anmerkungen
Zusammenfassung von Anmerkungen in Merge Request

Weiterer Fehler bei `npm install`: Versionskonflikt mit acorn.
```
sudo npm uninstall -g @angular/cli
sudo npm uninstall -g @angular-devkit
sudo npm cache clean --force
sudo npm install -g @angular/cli@13.0.3
sudo npm install -g @angular-devkit/build-angular@13.1.4
sudo npm install
```

## Maßnahmen
Die angemerkten Punkte wurden vom Entwickler direkt im Anschluss an das Review überarbeitet. Nach Rücksprache mit dem Reviewer konnte dann der Branch gemerged werden.