# Code Review
durchgeführt von Leena Yamil am 16.03.2022 in Iteration 8

## Reviewziel
Gereviewed wurden:

- apache2.conf
- mediaserver-start
- mediaserver.conf
- CMakeLists.txt


Außerdem wurde die Ausführung von mediaserver-start getestet


## Anmerkungen
gstreamer:  Schwarzes Bild erscheint kurz(nicht Vollbild!) und verschwindet dann
ansonsten läuft es

## Maßnahmen
WICHTIG: muss in home gespeichert sein
mkdir media
cd medienserver
sudo git submodule init
sudo git submodule update
sudo cmake -B build
sudo cmake --build build
cd build
sudo make install
sudo mediaserver-start

