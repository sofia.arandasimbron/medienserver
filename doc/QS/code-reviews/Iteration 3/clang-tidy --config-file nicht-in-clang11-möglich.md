
# Code Review
durchgeführt  von Paul Hermann am 05.01.2022 in Iteration 3

## Reviewziel
local_app/core/CMakeLists.txt
local_app/dmx_framework/include/DMXManager.hpp
local_app/dmx_framework/src/DMXManager.cpp
local_app/dmx_framework/test/CMakeLists.txt
local_app/dmx_framework/test/main.cpp
local_app/dmx_framework/CMakeLists.txt
local_app/FormatFile.cmake


## Anmerkungen

 ### "local_app/core/CMakeLists.txt " Zeile 15
 Kommentare entfernen
 ### " local_app/dmx_framework/CMakeLists.txt" Zeile 14- 16
 Kann entfernt werden
 ### " local_app/FormatFile.cmake" Zeile 29
 Kann entfernt werden
## Maßnahmen
Nach Anpassungen der Anmerkungen  sind die Code der oben genannten Files  ausführlich richtig implementiert, kommentiert und getestet.