# Code Review
durchgeführt von Paul Hermann am 03.01.2022 in Iteration 3

## Reviewziel

local_app/media_framework/gstreamer-cpp/include/core/CMakeLists.txt
local_app/media_framework/gstreamer-cpp/include/core/Bin.hpp
local_app/media_framework/gstreamer-cpp/include/core/Bus.hpp
local_app/media_framework/gstreamer-cpp/include/core/Caps.hpp
local_app/media_framework/gstreamer-cpp/include/core/Clock.hpp
local_app/media_framework/gstreamer-cpp/include/core/Element.hpp
local_app/media_framework/gstreamer-cpp/include/core/ElementFactory.hpp
local_app/media_framework/gstreamer-cpp/include/core/Message.hpp
local_app/media_framework/gstreamer-cpp/include/core/Object.hpp
local_app/media_framework/gstreamer-cpp/include/core/Pad.hpp
local_app/media_framework/gstreamer-cpp/include/core/Pipeline.hpp
local_app/media_framework/gstreamer-cpp/include/core/Structure.hpp
local_app/media_framework/gstreamer-cpp/include/CMakeLists.txt
local_app/media_framework/gstreamer-cpp/include/Prelude.hpp
local_app/media_framework/gstreamer-cpp/include/core.hpp
local_app/media_framework/gstreamer-cpp/src/core/Bin.cpp
local_app/media_framework/gstreamer-cpp/src/core/Bus.cpp
local_app/media_framework/gstreamer-cpp/src/core/Caps.cpp
local_app/media_framework/gstreamer-cpp/src/core/Clock.cpp
local_app/media_framework/gstreamer-cpp/src/core/Element.cpp
local_app/media_framework/gstreamer-cpp/src/core/ElementFactory.cpp
local_app/media_framework/gstreamer-cpp/src/core/Message.cpp
local_app/media_framework/gstreamer-cpp/src/core/Object.cpp
local_app/media_framework/gstreamer-cpp/src/core/Pad.cpp
local_app/media_framework/gstreamer-cpp/src/core/Pipeline.cpp
local_app/media_framework/gstreamer-cpp/src/core/Structure.cpp
local_app/media_framework/gstreamer-cpp/src/CMakeLists.txt
local_app/media_framework/gstreamer-cpp/src/core.cpp
local_app/media_framework/gstreamer-cpp/.clang-format
local_app/media_framework/gstreamer-cpp/.clang-tidy
local_app/media_framework/gstreamer-cpp/CMakeLists.txt
local_app/media_framework/gstreamer-cpp/FormatFile.cmake
local_app/media_framework/gstreamer-cpp/test/demo.cpp
local_app/media_framework/gstreamer-cpp/SetupDoxygen.cmake
local_app/media_framework/gstreamer-cpp/SetupGStreamer.cmake
local_app/media_framework/gstreamer-cpp/SetupGoogleTest.cmake
local_app/media_framework/gstreamer-cpp/.gitignore
local_app/media_framework/include/MediaFramework.hpp
local_app/media_framework/src/MediaBlackbox.cpp
local_app/media_framework/test/demo.cpp
local_app/media_framework/CMakeLists.txt
local_app/media_framework/SetupGStreamer.cmake
local_app/.clang-tidy
local_app/SetupDoxygen.cmake

## Anmerkungen

 ### " local_app/media_framework/src/MediaBlackbox.cpp" Zeile 11 - 101
 (Zeile 11) MediaBlackbox.cpp included MediaFramework.hpp.
  Ich würde MediaFramework.hpp auch zu MediaBlackbox.hpp umbenennen (oder andersrum).
  MediaBlackbox finde ich als Namen nicht soo aussagekräftig.
 (Zeile 30) TODO
 (Zeile 35) Funktioniert nicht in Raspbian ohne GUI. FBDevSink oder KMSSink wären eine Option. Wir sollten uns hier festlegen und nicht automatisch sinks wählen lassen. (Potentiell aber in kommenden Iterationen!).
 Erstmal fbdevsink, damit die Tests auf dem Pi laufen. Es Kann in Zukunft geändert werden, Scaling kann in kommenden US noch kommen .
 (Zeile 49) Diese Variable wird nie verwendet
 (Zeile 50) Lokale Dateien im Uridecodebin brauchen "file://" vor absolutem Pfad.
 Vorher also Codezeile einfügen mit file_path = "file://" + file_path
 (Zeile 52) Wenn ich das richtig verstehe, kann man hier nur Medien starten, wenn kein Medienpfad gesetzt ist.
 Das gibt keinen Sinn, weil keine Medien gestartet werden können, wenn kein Pfad gesetzt ist.
 Die start_media()-Methode ist in dieser Implementierung redundant mit der setMediaSrc() Methode.
 
 (Zeile 55) TODO Pipeline stoppen.Damit ist gemeint: Wenn Dateipfad ungültig, soll Wiedergabe gestoppt werden
 (Zeile 62 - 68) Funktion ist nicht nach Akzeptanzkriterium gefordert und wird auch innerhalb der Klasse nicht benötigt.
 Wird im Akzeptanzkriterium ergänt...Pause_media() ist das im Akzeptanzkriterium geforderte stop_media()
 (Zeile 87) Klingt sinnvoll! Kann bei der großen Medienwiedergabe-Userstory umgebaut werden.
 Ich würde allerdings das file:// nicht in die Medien-JSON auslagern, weil das dann jeweils schon im Webserver generiert werden muss.Absolute Pfade finde ich gut.
 (Zeile 91) sinnvoll!
 (Zeile 93) Wäre vielleicht etwas für das MediaJSON Framework, dass die Existenz geprüft wird. Ansonsten ist das hier auch okay!
 (Zeile 101) Hier lieber Stoppen als Pausieren? Oder ist das von der Performance schwierig, wenn man die Pipeline auf NULL setzt?
 Pipeline NULL nicht gut für Performance, weil ganze Pipeline abgeschossen wird
 ### " local_app/media_framework/gstreamer-cpp/.gitignore" Zeile 1
 GStreamer-CPP ist ein eigenständiges Repo. Kann das als Submodule eingebunden werden?
 Wird nicht als Submodule eingebunden, damit Gruppe aktiv daran mitarbeiten kann.
 ### " local_app/media_framework/src/MediaBlackbox.cpp" Zeile 11
 MediaBlackbox.cpp included MediaFramework.hpp.
Ich würde MediaFramework.hpp auch zu MediaBlackbox.hpp umbenennen (oder andersrum).
MediaBlackbox finde ich als Namen nicht soo aussagekräftig.


## Maßnahmen
Nach Anpassungen der Anmerkungen  sind die Code der oben genannten Files  ausführlich implementiert, kommentiert und getestet.