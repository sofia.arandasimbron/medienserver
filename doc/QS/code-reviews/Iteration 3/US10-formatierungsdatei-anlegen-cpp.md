# Code Review
durchgeführt von Boubacar Siby am 05.01.2022 in Iteration 3

## Reviewziel


local_app/.clang-tidy
local_app/.clang-format

## Anmerkungen
Keine Anmerkung
Die Code der oben genannten Files sind ausführlich richtig implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Keine Maßnahme