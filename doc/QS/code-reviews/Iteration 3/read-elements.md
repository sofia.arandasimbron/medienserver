# Code Review
durchgeführt von Sofia Aranda Simbron  am 05.01.2022 in Iteration 3

## Reviewziel



web_app/frontend/src/app/components/element-item/element-item.component.css
web_app/frontend/src/app/components/element-window/element-window.component.css
web_app/frontend/src/app/components/element-window/element-window.component.html
web_app/frontend/src/app/components/element-window/element-window.component.ts
web_app/frontend/src/app/components/list-window/list-window.component.html
web_app/frontend/src/app/components/list-window/list-window.component.ts
web_app/frontend/src/app/services/element.service.ts
web_app/frontend/package.json


## Anmerkungen
 ### "web_app/frontend/src/app/components/element-window/element-window.component.ts" Zeile 28

 I think the name of the lambda parameter (lists) can be changed to another one that suits better what the code does. It is a list and not many lists (?)
 ### " web_app/frontend/package-lock.json" Zeile 19-20

 Wir haben diese Datei beim Reviewen ignoriert, denn die Datei wird mit npm install für jede Angular installation überschrieben. Datei wird trotzdem ins Main gemerged, denn dort findet man benötigte dependencies für das npm install.



## Maßnahmen
Nach Anpassungen der Anmerkungen  sind die Code der oben genannten Files  ausführlich implementiert, kommentiert und getestet.