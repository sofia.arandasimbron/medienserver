# Code Review
durchgeführt von Sofia Aranda Simbron am 05.01.2022 in Iteration 3

## Reviewziel

local_app/dmx_framework/external/sACN
local_app/dmx_framework/include/DMXManager.hpp
local_app/dmx_framework/src/DMXManager.cpp
local_app/dmx_framework/test/CMakeLists.txt
local_app/dmx_framework/test/main.cpp
local_app/dmx_framework/CMakeLists.txt



## Anmerkungen 

 ### " local_app/dmx_framework/test/main.cpp " Zeile 1-3
 Folgende klären wir eventuell noch mit alle andere Developers: eventuell auch immer für jede Datei author oder authors hinzufügen. Obwohl das ist ja auch main.cpp also eventuell passt es so auch :D
 ### "local_app/dmx_framework/include/DMXManager.hpp" Zeile 7
 Struct noch Dokumentieren
 ### " local_app/dmx_framework/include/DMXManager.hpp" Zeile 1
 TODO: Doku der hpp Datei
 ### " local_app/dmx_framework/include/DMXManager.hpp"
 TODO: Doku der Klasse (Zeile 15)
 Bei der Doku der Klasse noch kurz erklären was alle diese Atributte sind (Von Zeile 18 bis Zeile 23) 
 ### " local_app/dmx_framework/src/DMXManager.cpp" Zeile 3
 Hier wieder eventuell noch author/authors eingeben 


## Maßnahmen
Nach Anpassungen der Anmerkungen  sind die Code der oben genannten Files  ausführlich implementiert, kommentiert und getestet.