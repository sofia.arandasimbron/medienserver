# Code Review
durchgeführt von Paul Hermann am 05.01.2022 in Iteration 3

## Reviewziel

local_app/CMakeLists.txt
local_app/CMakeOptions.txt
local_app/FormatFile.cmake
local_app/SetupDoxygen.cmake
local_app/SetupGoogleTest.cmake

## Anmerkungen
Keine Anmerkungen 
Die Code der oben genannten Files sind  ausführlich  implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Keine Maßnahme