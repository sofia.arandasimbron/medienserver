# Code Review
durchgeführt von Paul Hermann am 15.12.2021 in Iteration 2

## Reviewziel

local_app/core/src/main.cpp
local_app/CMakeLists.txt
local_app/CMakeOptions.txt

## Anmerkungen
Keine Anmerkungen 
Die Code der oben genannten Files sind  ausführlich  implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Keine Maßnahme