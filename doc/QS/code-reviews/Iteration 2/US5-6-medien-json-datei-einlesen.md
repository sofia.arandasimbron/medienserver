# Code Review
durchgeführt Paul Hermann am 15.12.2021 in Iteration 2

## Reviewziel

local_app/general_tools/extern/json.hpp
local_app/general_tools/include/reader.hpp
local_app/general_tools/src/reader.cpp
local_app/general_tools/test/json_demo.cpp
local_app/general_tools/CMakeLists.txt
local_app/SetupDoxygen.cmake

## Anmerkungen

 ### " local_app/general_tools/src/reader.cpp" 
 (Zeile 20) Hier allgemein lieber Verwendung als Datentyp angeben, Datentyp wird automatisch erkannt 
 (Zeile 23 - 41) Gut abgesichert! 
 (Zeile 46) Hier allgemein lieber Verwendung als Datentyp angeben, Datentyp wird automatisch erkannt
 (Zeile 94) Hier allgemein lieber Verwendung als Datentyp angeben, Datentyp wird automatisch erkannt
 (Zeile 106) Hier allgemein lieber Verwendung als Datentyp angeben, Datentyp wird automatisch erkannt
 


## Maßnahmen
Nach Anpassungen der Anmerkungen  sind die Code der oben genannten Files  ausführlich implementiert, kommentiert und getestet.