# Code Review
durchgeführt von Boubacar Siby  am 15.12.2021 in Iteration 2

## Reviewziel


local_app/general_tools/MediaReader/include/reader.hpp
local_app/general_tools/MediaReader/src/reader.cpp


## Anmerkungen
Schöne Struktur der Code. Ich denke dass, die Klassen zu kommentieren,  die Verständlichkeit der Code vereinfachen könnte.
Ansonsten sind die Code der oben genannten Files  ausführlich  implementiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Die Code  ist kommentiert!!