# Code Review
durchgeführt von Pawlowski Mateusz am 05.01.2022 in Iteration 2

## Reviewziel


local_app/general_tools/gst-performance-test/performancetest-automation.sh
local_app/general_tools/gst-performance-test/performancetest.sh


## Anmerkungen
 Im Pfad "local_app/general_tools/gst-performance-test/performancetest.sh "  auf der Zeile 7 ist ein unbenutztes Kommando "echo", da sollte eine Ausgabe fehlen. Deswegen denke ich, dass man es löschen könnte.
Ansonsten sind die Code der oben genannten Files  ausführlich  implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Unbenutztes Kommando "echo" ist gelöscht!!!