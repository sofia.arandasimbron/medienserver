# Code Review
durchgeführt von Paul Hermann am 28.02.2022 in Iteration 7

## Reviewziel
Gereviewed wurden folgende Codedateien:
web_app/backend/.node-dev.json
web_app/backend/ConfigManager.ts
web_app/backend/index.ts
web_app/backend/package.json
web_app/files/config.json
web_app/files/medien.json
web_app/frontend/src/app/components/button/button.component.html
web_app/frontend/src/app/components/config-window/config-window.component.html
web_app/frontend/src/app/components/config-window/config-window.component.ts
web_app/frontend/src/app/components/header/header.component.html
web_app/frontend/src/app/components/header/header.component.ts
web_app/frontend/src/app/components/list-window/list-window.component.ts
web_app/openapi.yml

Außerdem wurde die Ausführung des gesamten Webservers getestet.

## Anmerkungen
web_app/backend/ConfigManager.ts (Zeile 48): "Hier könnte auch die Validierung mittels Footprint-Size eingebaut werden, um fehlerhafte Eingaben zu vermeiden"
web_app/backend/index.ts (Zeile 204): Console Log kann weg
web_app/backend/index.ts (Zeile 208): Console Log kann weg
web_app/backend/index.ts (Zeilen 232-242): Kommentar kann weg
web_app/backend/index.ts (Zeile 8): Validierungsfunktion notwendig, falls Datei nicht existiert
web_app/files/config.json: Datei kann in .gitignore
web_app/files/medien.json: Datei kann in .gitignore
web_app/openapi.yml (Zeilen 419-425): Kommentar kann weg

## Maßnahmen
Die angemerkten Punkte wurden vom Entwickler direkt im Anschluss an das Review überarbeitet. Nach Rücksprache mit dem Reviewer konnte dann der Branch gemerged werden.