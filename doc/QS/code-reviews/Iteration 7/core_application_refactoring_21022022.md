# Code Review
durchgeführt von Sofia Aranda am 21.02.2022 in Iteration 7

## Reviewziel
Gereviewed wurden 

- ./local_app/core/src/main.cpp
- ./local_app/dmx_framework/include/dmx_framework.hpp
- ./local_app/dmx_framework/src/dmx_framework.cpp
- ./local_app/dmx_framework/test/dmx_framework_automatic_tests.cpp
- ./local_app/dmx_framework/test/dmx_framework_test1.cpp
- ./local_app/dmx_framework/test/dmx_framework_test2.cpp
- ./local_app/dmx_framework/test/dmx_framework_test3.cpp
- ./local_app/dmx_framework/test/dmx_framework_test4.cpp
- ./local_app/dmx_framework/test/dmx_framework_test5.cpp
- ./local_app/general_tools/include/file_watcher.hpp
- ./local_app/general_tools/scr/file_watcher.cpp
- ./local_app/media_framework/include/media_framework.hpp
- ./local_app/media_framework/src/media_framework.cpp
- ./local_app/media_framework/test/media_framework_demo.cpp
- ./local_app/media_framework/test/media_framework_manual_tests.cpp
- ./local_app/media_framework/test/media_framework_thread_demo.cpp

## Anmerkungen

(main.cpp) In main.cpp ist eventuell die Initialisierung der Objekte notwendig.
(file_watcher.cpp) Der Konstruktor in file_watcher.cpp kann tendenziell gelöscht werden.
(dmx_framework.hpp & dmx_framework.cpp) Außerdem soll noch geprüft werden, ob beim starten man die Adresse noch ändern kann.
(dmx_framework.cpp) Nur eine Gedanke: um den User zu warnen -> eventuell in dmx_framework.cpp in die setter methoden noch abfragen, ob footprint und addresierung passt.
Ansonsten danke, dass du das Refactoring gemacht hast, dadurch ist der zugriff zu den einzelnen Komponenten jetzt viel angenehmer! :) 

## Maßnahmen
Die Anmerkungen zu main.cpp und file_watcher.cpp konnten direkt vom Entwickler angepasst werden.
Die Änderungsvorschläge zum dmx_framework sind umfangreicher und werden daher in der gleichen Iteration im Zuge einer eigenen Userstory zur DMX-Adressierung bearbeitet.
Da die Anwendung läuft, kann der Merge Request gemerged werden.