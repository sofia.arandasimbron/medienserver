# Code Review
durchgeführt von Sofia Aranda am 22.02.2022 in Iteration 7

## Reviewziel
Gereviewed wurden 

- ./local_app/core/src/main.cpp
- ./local_app/dmx_framework/src/dmx_framework.cpp
- ./local_app/general_tools/include/config_reader.hpp
- ./local_app/general_tools/scr/config_reader.cpp
- ./local_app/general_tools/test/config_demo.cpp
- ./local_app/general_tools/CMakeLists.txt
- ./web_app/files/config.json

## Anmerkungen

(main.cpp) Hier Kommentar anpassen: der receiver wird nicht neu gestartet.
Ansonsten ist die Implementierung super! Vor allem sehr verständlich.

## Maßnahmen

Kommentar wurde angepasst.


