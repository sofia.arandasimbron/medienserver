# Code Review
durchgeführt von Sofia Aranda am 01.03.2022 in Iteration 7

## Reviewziel
Gereviewed wurden:


- ./local_app/general_tools/gst-performance-test/performancetest.sh
- ./local_app/media_framework/include/media_framework.hpp
- ./local_app/media_framework/src/media_framework.cpp
- ./local_app/media_framework/test/media_framework_demo.cpp


## Anmerkungen
Super, dass du alles so ausführlich kommentiert hast! 
(media_framework.cpp) Zeile 228 kann weg
Probleme, die in die Zukunft gelöst werden:
Performance: auch keine FULL HD Videos sollen rückelfrei laufen
Skalierung: auch keine FULL HD Videos sollen in die Mitte erscheinen und nicht um die Ecke.

## Maßnahmen
Zeile 228 wurde gelöscht
Performance und Skalierung Probleme werden in weiteren User Storys gelöst.

