# Code Review
durchgeführt von Mateusz Pawlowski am 08.02.2022 in Iteration 6

## Reviewziel
Gereviewed wurden 
- ./local_app/core/src/main.cpp
- ./local_app/core/CMakeLists.txt
- ./local_app/general_tools/include/file_watcher.hpp
- ./local_app/general_tools/src/file_watcher.cpp
- ./local_app/general_tools/src/json_reader.cpp
- ./local_app/media_framework/gstreamer-cpp/CMakeLists.txt
- ./local_app/media_framework/gstreamer-cpp/FormatFile.cmake 
- ./local_app/media_framework/include/media_framework.hpp
- ./local_app/media_framework/src/media_framework.cpp
- ./local_app/media_framework/CMakeLists.txt


Außerdem wurde die Ausführung von _main.cpp_  manuell getestet, indem die _./core_-Datei nachdem **cmake -B build** und **cmake --build build** ausgeführt wurden.
Bei dem Unversum 1 wurde die erste Liste auf Kanal 1 gesetzt und es wurden 8 Testdateien über Kanal 2 abgespielt. Die Liste und deren Elemente wurden manuell in der _medien.json_-Datei konfiguriert. Beim Aufruf eines leeren Elementes (DMX-Wert 8 in Kanal 2) wurde die Wiedergabe gestoppt. 
Somit wurde auch das Akzeptanzkriterium erfüllt.

## Anmerkungen

Kleine Fehlerbehebungen wurden in Rücksprache mit dem Entwickler bereits bei der Einrichtung getroffen. So gab es mit der CMake-Konfiguration kleinere Probleme, welche schnell behoben werden konnten.
Es gibt keine Anmerkungen zur Implementierung / Umsetzung.
MediaBlackbox.cpp - Code sollte ausführlicher dokumentiert werden
core/src/main.cpp - Code sollte ausführlicher dokumentiert werden

## Maßnahmen
Die benannten Codeabschnitte wurden vom Entwickler ausführlicher dokumentiert und die Ergebnisse committed.