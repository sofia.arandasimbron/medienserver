# Code Review
durchgeführt von Paul Hermann am 16.02.2022 in Iteration 6

## Reviewziel
Gereviewed wurden folgende Codedateien:
/web_app/backend/ListManager.ts
/web_app/frontend/src/app/component/*
/web_app/frontend/src/app/services/api/api/api.ts
/web_app/frontend/src/app/services/api/.gitignore
/web_app/frontend/src/index.html
/web_app/frontend/package.json
/web_app/openapi.yml

Außerdem wurde die Ausführung des gesamten Webservers getestet.

## Anmerkungen
Zusammenfassung von Anmerkungen in Merge Request

web_app/backend/ListManager.ts Zeile 193
"Dateipfad in (globale) Variable auslagern"

web_app/backend/ListManager.ts Zeile 317
"Console-Logs können weg (vermutlich Debug Methoden, die übrig geblieben sind)"


## Maßnahmen
Die angemerkten Punkte wurden vom Entwickler direkt im Anschluss an das Review überarbeitet. Nach Rücksprache mit dem Reviewer konnte dann der Branch gemerged werden.