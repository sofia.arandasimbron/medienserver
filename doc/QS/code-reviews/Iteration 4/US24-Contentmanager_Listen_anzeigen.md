# Code Review
durchgeführt von Paul Hermann am 19.01.2022 in Iteration 4

## Reviewziel
Gereviewed wurden folgende Codedateien:
/web_app/backend/ListManager.ts
/web_app/frontend/src/app/component/*
/web_app/frontend/src/app/services/api/api/api.ts
/web_app/frontend/src/app/services/api/.gitignore
/web_app/frontend/src/app/List.ts
/web_app/frontend/src/index.html
/web_app/frontend/angular.json
/web_app/frontend/package.json
/web_app/openapi.yml

Außerdem wurde die Ausführung des gesamten Webservers getestet.

## Anmerkungen
Zusammenfassung von Anmerkungen in Merge Request

web_app/frontend/src/app/components/list-window/list-window.component.html Zeile 3
"Wir alle lieben HTML-For-Schleifen! <3"

Es gab einen Angular/compiler Versionskonflikt bei der Installation.
npm install --force hat geholfen.

## Maßnahmen
Die angemerkten Punkte wurden vom Entwickler direkt im Anschluss an das Review überarbeitet. Nach Rücksprache mit dem Reviewer konnte dann der Branch gemerged werden.