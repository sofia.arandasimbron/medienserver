# Code Review
durchgeführt von Paul Hermann  am 19.01.2022 in Iteration 2

## Reviewziel


web_app/frontend/src/app/components/file-manager/file-manager.component.html
web_app/frontend/src/app/components/file-manager/file-manager.component.ts
web_app/frontend/src/app/app.module.ts
web_app/frontend/src/styles.css
web_app/frontend/src/app/app.module.ts
web_app/frontend/package.json

## Anmerkungen
Keine Anmerkungen
Die Code der oben genannten Files sind ausführlich  implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Keine Maßnahme