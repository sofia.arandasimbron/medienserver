# Code Review
durchgeführt von Mateusz Pawlowski am 20.01.2022 in Iteration 4

## Reviewziel


local_app/media_framework/include/MediaFramework.hpp
local_app/media_framework/src/MediaBlackbox.cpp
local_app/media_framework/test/demo.cpp
local_app/media_framework/test/thread_demo.cpp
local_app/media_framework/CMakeLists.txt

## Anmerkungen
Keine Anmerkungen...
Die Code der oben genannten Files sind ausführlich richtig implementiert kommentiert und getestet.

## Maßnahmen
Keine maßnahme