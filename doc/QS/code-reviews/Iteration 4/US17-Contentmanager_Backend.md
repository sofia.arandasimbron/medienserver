# Code Review
durchgeführt von Paul Hermann am 19.01.2022 in Iteration 4

## Reviewziel
Gereviewed wurden folgende Codedateien:
/web_app/backend/.gitignore
/web_app/backend/.node-dev.json
/web_app/backend/ListManager.ts
/web_app/backend/index.ts
/web_app/backend/init.ts
/web_app/openapi.yml

Außerdem wurde die Ausführung des gesamten Webservers getestet.

## Anmerkungen
Zusammenfassung von Anmerkungen in Merge Request:

web_app/backend/ListManager.ts Zeile 130
"Kommentar kann weg"

web_app/backend/ListManager.ts Zeile 196
"Super Umsetzung mit der Errorliste! Sauber gelöst und sehr hilfreiche Prints eingebaut!"

web_app/backend/ListManager.ts Zeile 288
"Richtig coole Umsetzung mit dem Errorobjekt! Ich bin begeistert"

web_app/backend/ListManager.ts Zeile 298
"Grobe Beschreibung notwendig für Methode"

web_app/backend/ListManager.ts Zeilen 313-328
"Grobe Beschreibung der Funktionalität notwendig"

web_app/backend/index.ts Zeile 5
"Könnte in Zukunft mit Environment Variablen umgesetzt werden, wäre aber eher was für eine eigene Userstory in der kommenden Iteration. (ToDo schreiben)"

web_app/backend/index.ts Zeilen 211-225
"Kann weg"

web_app/openapi.yml Zeile 164
"Type, doppeltes 'the'"

## Maßnahmen
Die angemerkten Punkte wurden vom Entwickler direkt im Anschluss an das Review überarbeitet. Nach Rücksprache mit dem Reviewer konnte dann der Branch gemerged werden.