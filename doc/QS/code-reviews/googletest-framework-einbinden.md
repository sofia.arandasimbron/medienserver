# Code Review
durchgeführt von Boubacar Siby am TT.MM.YYYY in Iteration XY

## Reviewziel

local_app/core/src/main.cpp
local_app/core/test/tests.cpp
local_app/core/CMakeLists.txt
local_app/dmx_framework/CMakeLists.txt
local_app/general_tools/CMakeLists.txt
local_app/media_framework/CMakeLists.txt
local_app/CMakeLists.txt

## Anmerkungen
Keine Anmerkungen
Die Code der oben genannten Files sind ausführlich richtig implementiert, kommentiert und getestet.
Die Checkliste ist in der Dokumentation angehängt.

## Maßnahmen
Keine Maßnahme