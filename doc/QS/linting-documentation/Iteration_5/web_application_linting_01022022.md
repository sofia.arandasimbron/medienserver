# Linting Web Application 
durchgeführt von Boubacar Siby am 01.02.2022 in Iteration 5

## Maßnahmen
Linting wurde lokal manuel ausgeführt und die Fehler behoben. 

## Frontend 
### Konsolenoutput
✖ 438 problems (432 errors, 6 warnings)
421 errors and 0 warnings potentially fixable with the --fix option.
✖ Nachdem Fixen existieren noch folgende Errors und Warnings, wobei die Warnings zu ignorieren sind: 17 problems (11 errors, 6 warnings)
siehe `web_application_linting_frontend_console_output_01022022.log`
## Backend
### Konsolenoutput
✖ 76 problems (67 errors, 9 warnings)
67 errors and 0 warnings potentially fixable with the --fix option.
✖ Nach dem Fixen existieren noch folgende Warnings, die man ignorieren kann: 9 problems (0 errors, 9 warnings)
siehe `web_application_linting_backend_console_output_01022022.log`