# Linting Core Application 
durchgeführt von Sofía Aranda am 16.02.2022 in Iteration 6

## Maßnahmen
Linting wurde lokal manuell ausgefürht und die Fehler wurden behoden

## Konsolenoutput

siehe `core_application_linting_console_output_16022022.txt`

## Fixes

siehe `core_application_linting_fixes_16022022.yaml`