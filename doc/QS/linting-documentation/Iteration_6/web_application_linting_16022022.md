# Linting Web Application 
durchgeführt von Mateusz Pawlowski am 16.02.2022 in Iteration 6

## Maßnahmen
Linting wurde lokal manuel ausgeführt und die Fehler behoben. 

## Frontend 
### Konsolenoutput
siehe `web_application_linting_frontend_console_output_16022022.log`
## Backend
### Konsolenoutput
siehe `web_application_linting_backend_console_output_16022022.log`