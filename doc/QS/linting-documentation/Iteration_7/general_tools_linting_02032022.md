# Linting Core Application 
durchgeführt von Sofía Aranda am 02.03.2022 in Iteration 7

## Maßnahmen
Linting wurde lokal manuell ausgefürht und die Fehler wurden ignoriert, denn ein Fix wurde die Funktionalität der header Datei ändern. Es sind keine weitere Maßnahmen notwendig.

## Konsolenoutput

siehe `general_tools_linting_console_output_02032022.txt`

## Fixes

siehe `general_tools_linting_fixes_02032022.yaml`