# Linting Media Framework 
durchgeführt von Sofía Aranda am 02.03.2022 in Iteration 7

## Maßnahmen
Linting wurde lokal manuell ausgefürht und die Anmerkungen wurden ignoriert, denn die datei existiert und is aus eine vorhandene Library. Außerdem sind die Attribute protected. Es sind keine weitere Maßnahmen notwendig.

## Konsolenoutput

siehe `media_framework_linting_console_output_02032022.txt`

## Fixes

siehe `media_framework_linting_fixes_02032022.yaml`