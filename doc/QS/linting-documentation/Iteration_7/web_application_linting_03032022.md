# Linting Web Application 
durchgeführt von Mateusz Pawlowski am 03.03.2022 in Iteration 7

## Maßnahmen
Linting wurde lokal manuel ausgeführt und die Fehler behoben. 

## Frontend 
### Konsolenoutput
siehe `web_application_linting_frontend_console_output_93932022.log`
## Backend
### Konsolenoutput
siehe `web_application_linting_backend_console_output_03032022.log`