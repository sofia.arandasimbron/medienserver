# Linting Media Framework 
durchgeführt von Sofía Aranda am 19.01.2022 in Iteration 4

## Maßnahmen
Linting wurde lokal manuell ausgefürht und der Fehler wird ignoriert, denn die datei existiert und is aus eine vorhandene Library

## Konsolenoutput

siehe `media_framework_linting_console_output_19012022.txt`

## Fixes

siehe `media_framework_linting_fixes_19012022.yaml`