# Linting DMX Framework 
durchgeführt von Sofía Aranda am 05.01.2022 in Iteration 3

## Maßnahmen
Linting wurde lokal manuell ausgefürht und die Fehler wurden behoden

## Konsolenoutput

siehe `dmx_framework_linting_console_output_05012022.txt`

## Fixes

siehe `dmx_framework_linting_fixes_05012022.yaml`