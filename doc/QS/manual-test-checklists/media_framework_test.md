# Manuelle Testcheckliste - DMX Empfang API

## Testvoraussetzungen
- Raspberry Pi benötigt Medienserver Git Repository auf aktuellstem Stand
- Raspberry Pi benötigt per HDMI angeschlossenen Bildschirm
- Folgende Mediendateien müssen vorhanden sein:
  - `/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4`
  - `/home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4`
- Anwendung muss mit Cmake gebaut sein
    - Navigiere in Verzeichnis /local_app/media_framework
    - Führe `cmake -B build` aus
    - Führe `cmake --build build` aus
    - Die ausführbaren Testdateien liegen jetzt in /local_app/media_framework/build

## Testablauf und Kriterien
- Schritt 1: Testprogramm starten mit `./manual_tests` im Ordner `/local_app/media_framework/build`
  - Erwartetes Ergebnis: Bildschirm wird schwarz
- Schritt 2: Enter drücken (start_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 3: Enter drücken (stop_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 4: Enter drücken (pause_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 5: Enter drücken (set_media_src() with valid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video startet auf Bildschirm von vorne
- Schritt 6: Enter drücken (set_media_src() with valid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video startet auf Bildschirm von vorne
- Schritt 7: Enter drücken (start_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video läuft unverändert weiter
- Schritt 8: Enter drücken (stop_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Videowiedergabe wird gestoppt, Video ist nicht mehr sichtbar
- Schritt 9: Enter drücken (set_media_src() with invalid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 10: Enter drücken (set_media_src() with invalid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 11: Enter drücken (pause_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 11: Enter drücken (start_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm
- Schritt 12: Enter drücken (set_media_src() with valid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video startet auf Bildschirm von vorne
- Schritt 13: Enter drücken (pause_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video wird pausiert
- Schritt 13: Enter drücken (start_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Video läuft von pausierter Stelle aus weiter
- Schritt 14: Enter drücken (set_media_src() with invalid path wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Videowiedergabe wird gestoppt, Video ist nicht mehr sichtbar
- Schritt 15: Enter drücken (stop_media() wird in Konsole ausgegeben)
  - Erwartetes Ergebnis: Keine Änderung auf dem Bildschirm

## Coverage
Nach Stand vom 02.02.2022 bieten diese Tests 100% Branch Coverage für die public-Methoden der Datei MediaBlackbox.cpp.

## Maßnahmen
Die erwarteten Testergebnisse sind nur gültig, sofern alle Voraussetzungen erfüllt wurden.
Wenn ein Ergebnis nicht erfüllt werden kann, ist ein Issue auf Gitlab anzulegen, welches Nummer des Tests und eine Beschreibung der Abweichung vom erwarteten Ergebnis enthält.
Das Team legt dann zeitnah eine*n Entwickler*in fest, der/die die Implementierung anpasst, um die Tests zu erfüllen.
Dies geschieht spätestens bis zum Ende der nächsten Iteration.