# Manuelle Testcheckliste - DMX Empfang API

## Testvoraussetzungen
- Raspberry Pi und Testcomputer im gleichen Netzwerk
- Netzwerk muss Multicast zulassen
- Raspberry Pi benötigt Medienserver Git Repository auf aktuellstem Stand
- Anwendung muss mit Cmake gebaut sein
    - Navigiere in Verzeichnis /local_app/dmx_framework/build
    - Führe `cmake ..` aus
    - Führe `make` aus
    - Die ausführbaren Testdateien liegen jetzt in /local_app/dmx_framework/build/test
- Testcomputer benötigt die Anwendung sACNView (https://sacnview.org/)
## Testablauf und Kriterien
### Test 1 (Channels)
- `./test1` starten
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 1
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: In der Konsole ist 512 mal der Wert “0” zu sehen.
- Fader 1 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Der erste Wert in der Konsole zeigt ohne menschlich wahrnehmbare Latenz den über den Fader eingestellten Wert an. Alle anderen Werte zeigen 0 an. Jedes mal, wenn sich der Wert ändert, wird ein “*” hinter dem Wert angezeigt.
- Fader 512 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Der letzte Wert in der Konsole zeigt ohne menschlich wahrnehmbare Latenz den über den Fader eingestellten Wert an. Die Werte 2-511 zeigen 0 an. Jedes mal, wenn sich der Wert ändert, wird ein “*” hinter dem Wert angezeigt.
- “Bereich überblenden” in sACNView aufrufen und auf Rampe umschalten, Geschwindigkeit auf 25Hz stellen
    - Erwartetes Ergebnis: Alle Werte in der Konsole steigen in Endlosschleife mit etwa 25 Schritten pro Sekunde von 0 auf 255. Es wird immer ein “*” hinter jedem Wert angezeigt. Alle Werte in einer Zeile sind immer alle gleich. Zwischen den einzelnen ausgegebenen Zeilen liegt nie mehr als eine ganze Zahl zwischen den Werten (wenn in einer Zeile für alle Werte 100 ausgegeben wird, muss also in der nächsten 100 oder 101 für alle Werte ausgegeben werden).


### Test 2 (Footprint 1)
- `./test2` starten
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 1
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: In der Konsole ist 10 mal der Wert “0” zu sehen.
- Fader 1 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Der erste Wert in der Konsole zeigt ohne menschlich wahrnehmbare Latenz den über den Fader eingestellten Wert an. Alle anderen Werte zeigen 0 an. Jedes mal, wenn sich der Wert ändert, wird ein “*” hinter dem Wert angezeigt.
- Fader 10 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Der letzte Wert in der Konsole zeigt ohne menschlich wahrnehmbare Latenz den über den Fader eingestellten Wert an. Die Werte 2-9 zeigen 0 an. (Wert 1 ist abhängig vom vorher eingestellten Fader 1). Jedes mal, wenn sich der Wert ändert, wird ein “*” hinter dem Wert angezeigt.

### Test 3 (Footprint 2)
- `./test3` starten
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 1
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: In der Konsole ist ein mal der Wert “0” zu sehen.
- Fader 511 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Die ausgegebenen Werte bleiben unverändert. Wenn ein Wert außerhalb des Footprint-Bereiches verändert wird, kommen weiterhinr sekündlich Pakete, nicht häufiger.
- Fader 512 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Der erste Wert in der Konsole zeigt ohne menschlich wahrnehmbare Latenz den über den Fader eingestellten Wert an. Jedes mal, wenn sich der Wert ändert, wird ein “*” hinter dem Wert angezeigt.

### Test 4 (Universum)
- `./test4` starten
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 63999
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: In der Konsole ist 512 mal der Wert “0” zu sehen.
- sACNView senden stoppen
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 42
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: Es werden keine neuen Werte ausgegeben.

### Test 5 (Unicast)
- `./test5` starten
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 1
    - Anzahl Slots: 512
    - Unicast (IP Adresse des Raspberry Pis)
- sACNView senden starten
    - Erwartetes Ergebnis: In der Konsole ist 512 mal der Wert “0” zu sehen.

## Maßnahmen
Die erwarteten Testergebnisse sind nur gültig, sofern alle Voraussetzungen erfüllt wurden.
Wenn ein Ergebnis nicht erfüllt werden kann, ist ein Issue auf Gitlab anzulegen, welches Nummer des Tests und eine Beschreibung der Abweichung vom erwarteten Ergebnis enthält.
Das Team legt dann zeitnah eine*n Entwickler*in fest, der/die die Implementierung anpasst, um die Tests zu erfüllen.
Dies geschieht spätestens bis zum Ende der nächsten Iteration.