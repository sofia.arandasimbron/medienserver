# Manuelle Testcheckliste - Core Anwendung

## Testvoraussetzungen
- Raspberry Pi und Testcomputer im gleichen Netzwerk
- Netzwerk muss Multicast zulassen
- Raspberry Pi benötigt Medienserver Git Repository auf aktuellstem Stand
- Anwendung muss mit Cmake gebaut sein
    - Navigiere in Verzeichnis /local_app/
    - Führe `cmake -B build` aus
    - Führe `cmake --build build` aus
    - Die ausführbare Core-Anwendung liegt jetzt in /local_app/build/core

- Folgende Mediendateien müssen vorhanden sein:
  - `/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4`
  - `/home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4`
- Testcomputer benötigt die Anwendung sACNView (https://sacnview.org/)
- Die Dateien config.json und medien.json existieren nicht.

## Testablauf und Kriterien
### Test 1 (Start ohne mediafile)
- `./core` starten
  - Erwartete Ausgabe: "[CORE] Couldn't find mediafile" jede Sekunde in der Konsole und Bildschirm wird schwarz

### Test 2 (Fehlende configfile)
- `medien.json` Datei aus Testordner in Ordner `/web_app/files/` kopieren
  - Erwartete Ausgabe: "[CORE] Couldn't find configfile" jede Sekunde in der Konsole

### Test 3 (Korrektes Einlesen der configfile)
- `config.json` Datei aus Testordner in Ordner `/web_app/files/` kopieren
  - Erwartete Ausgabe: "[CORE] Updating dmx-config to universe: 1, start_address: 1"

### Test 4 (DMX Start)
- sACNView starten und sACN-Sender folgendermaßen konfigurieren:
    - Protokollversion Ratified
    - Universum: 1
    - Anzahl Slots: 512
    - Multicast
- sACNView senden starten
    - Erwartetes Ergebnis: "[CORE] Received new DMX Values [0;0]"
    - Erwartetes Ergebnis: "[CORE] No corresponding element in media.json: stop_media()"

### Test 5 (Invalid URI)
- DMX Kanal 2 auf Wert 1 setzen
  - Erwartetes Ergebnis: Nichts passiert, Bildschirm bleibt schwarz

### Test 6 (Valid URI)
- DMX Kanal 1 auf Wert 1 setzen
  - Erwartetes Ergebnis: 30FPS Video wird abgespielt

### Test 7 (Invalid URI)
- DMX Kanal 1 auf Wert 0 setzen
  - Erwartetes Ergebnis: Wiedergabe wird beendet (schwarzer Bildschirm)

### Test 8 (Valid URI)
- DMX Kanal 2 auf Wert 2 setzen
  - Erwartetes Ergebnis: 30FPS Video wird abgespielt

### Test 9 (Valid URI Switch)
- DMX Kanal 2 auf Wert 3 setzen
  - Erwartetes Ergebnis: 60FPS Video wird abgespielt

### Test 10 (Empty Element)
- DMX Kanal 2 auf Wert 4 setzen
  - Erwartetes Ergebnis: Wiedergabe wird beendet (schwarzer Bildschirm)

### Test 11 (DMX Reconnect)
- sACN View Stoppen und wieder starten
  - Erwartetes Ergebnis: Nichts ändert sich
- DMX Kanal 2 auf Wert 3 setzen
  - Erwartetes Ergebnis: 60FPS Video wird abgespielt

## Maßnahmen
Die erwarteten Testergebnisse sind nur gültig, sofern alle Voraussetzungen erfüllt wurden.
Wenn ein Ergebnis nicht erfüllt werden kann, ist ein Issue auf Gitlab anzulegen, welches Nummer des Tests und eine Beschreibung der Abweichung vom erwarteten Ergebnis enthält.
Das Team legt dann zeitnah eine*n Entwickler*in fest, der/die die Implementierung anpasst, um die Tests zu erfüllen.
Dies geschieht spätestens bis zum Ende der nächsten Iteration.