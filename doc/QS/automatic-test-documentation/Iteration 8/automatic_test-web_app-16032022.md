# Testausführung
durchgeführt von Sofia Aranda am 16.03.2022 in Iteration 8

## Testziele
Quelldateien:

- button.component.ts
- content-manager.component.ts
- content-window.component.ts
- dialog-window.component.ts
- element-item.component.ts
- element-window.component.ts
- footer.component.ts
- header.component.ts
- list-item.component.ts
- list-window.component.ts

## Code Coverage
Das Code Coverage liegt bei 95%

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)

siehe `automatic_test-web_app-console_output-16032022.log`