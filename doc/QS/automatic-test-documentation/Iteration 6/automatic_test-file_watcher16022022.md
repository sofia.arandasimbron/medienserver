# Testausführung
durchgeführt von Sofia Aranda am 16.02.2022 in Iteration 6

## Testziele
Klasse              :     FileWatcher
Dazugehörige Dateien:     file_watcher.hpp, file_watcher.cpp

## Code Coverage
Das Code Coverage liegt bei 100%

## Maßnahmen
Ein test ist Fehlgeschlagen, dadurch wurde ein Bug im code gefunden: der Filewatcher wirft eine Exception, wenn Datei nach start(...) gelöscht wird. Der Bug wurde gefix und wieder getestet. Da alle Testergebnisse nach dem Bug-Fixen erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput mit Bug)
[==========] Running 5 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 5 tests from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_resultFailed_ifInvalidFile
Invalid argument
[       OK ] FileWatcherTest.test_start_resultFailed_ifInvalidFile (1 ms)
[ RUN      ] FileWatcherTest.test_start_updatedTrue_ifFiledeleted
/home/pi/medienserver/local_app/general_tools/test/file_watcher_automatic_tests.cpp:93: Failure
Expected equality of these values:
  updated
    Which is: false
  true

## Testoutput (Konsolenoutput nach dem Bug-Fix)
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_resultFailed_ifInvalidFile
Invalid argument
[       OK ] FileWatcherTest.test_start_resultFailed_ifInvalidFile (1 ms)
[----------] 1 test from FileWatcherTest (1 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (1 ms total)
[  PASSED  ] 1 test

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_updatedTrue_ifFiledeleted
[FILEWATCHER]: File test_media.json does not exist. Calling callback...
[       OK ] FileWatcherTest.test_start_updatedTrue_ifFiledeleted (208 ms)
[----------] 1 test from FileWatcherTest (208 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (208 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_updatedTrue_ifValidFileAndModified
[       OK ] FileWatcherTest.test_start_updatedTrue_ifValidFileAndModified (208 ms)
[----------] 1 test from FileWatcherTest (208 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (209 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_updateFalse_ifValidFileAndNotModified
[       OK ] FileWatcherTest.test_start_updateFalse_ifValidFileAndNotModified (139 ms)
[----------] 1 test from FileWatcherTest (140 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (140 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from FileWatcherTest
[ RUN      ] FileWatcherTest.test_start_updateTrue_ifStartedAgainOnValidFile
[       OK ] FileWatcherTest.test_start_updateTrue_ifStartedAgainOnValidFile (378 ms)
[----------] 1 test from FileWatcherTest (378 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (378 ms total)
[  PASSED  ] 1 test.