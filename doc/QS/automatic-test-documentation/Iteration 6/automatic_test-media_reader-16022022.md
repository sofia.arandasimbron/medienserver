# Testausführung
Durchgeführt von Sofia Aranda am 16.02.2022 in Iteration 6

## Testziele
Klasse              :     MediaReader
Dazugehörige Dateien:     reader.hpp, reader.cpp

## Code Coverage
Das Branch Coverage liegt bei 100%

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)
[==========] Running 6 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from general_tools
[ RUN      ] general_tools.ReadBasicJson
[       OK ] general_tools.ReadBasicJson (1 ms)
[----------] 1 test from general_tools (2 ms total)

[----------] 5 tests from MediaReaderTest
[ RUN      ] MediaReaderTest.set_file
[       OK ] MediaReaderTest.set_file (4 ms)
[ RUN      ] MediaReaderTest.contains_value
[       OK ] MediaReaderTest.contains_value (2 ms)
[ RUN      ] MediaReaderTest.is_empty
[       OK ] MediaReaderTest.is_empty (3 ms)
[ RUN      ] MediaReaderTest.get_path
[       OK ] MediaReaderTest.get_path (3 ms)
[ RUN      ] MediaReaderTest.get_identifier
[       OK ] MediaReaderTest.get_identifier (3 ms)
[----------] 5 tests from MediaReaderTest (52 ms total)

[----------] Global test environment tear-down
[==========] 6 tests from 2 test suites ran. (79 ms total)
[  PASSED  ] 6 tests.