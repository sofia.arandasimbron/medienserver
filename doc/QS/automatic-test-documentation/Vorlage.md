# Testausführung
durchgeführt von XY am DD.MM.YYYY in Iteration ZZ

## Testziele
Klasse XY / Quelldatei ZZ, ...

## Code Coverage
Das Code Coverage liegt bei XX%

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)