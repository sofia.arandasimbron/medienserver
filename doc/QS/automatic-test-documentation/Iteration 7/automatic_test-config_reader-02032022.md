# Testausführung
durchgeführt von Sofia Aranda am 02.03.2022 in Iteration 7

## Testziele
Klasse              :     CofigReader
Dazugehörige Dateien:     config_reader.hpp, config_reader.cpp

## Code Coverage
Das Code Coverage liegt bei 100%

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)
[==========] Running 8 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 8 tests from ConfigReaderTest
[ RUN      ] ConfigReaderTest.test_set_file_resultOk_ifJsonValid
[       OK ] ConfigReaderTest.test_set_file_resultOk_ifJsonValid (2 ms)
[ RUN      ] ConfigReaderTest.test_set_file_resultFailed_ifJsonInvalid
[       OK ] ConfigReaderTest.test_set_file_resultFailed_ifJsonInvalid (2 ms)
[ RUN      ] ConfigReaderTest.test_read_universe_resultOk_ifJsonValidAndElementExists
[       OK ] ConfigReaderTest.test_read_universe_resultOk_ifJsonValidAndElementExists (2 ms)
[ RUN      ] ConfigReaderTest.test_read_universe_resultFailed_ifJsonValidAndElementDoesNotExists
[       OK ] ConfigReaderTest.test_read_universe_resultFailed_ifJsonValidAndElementDoesNotExists (1 ms)
[ RUN      ] ConfigReaderTest.test_read_start_address_resultOk_ifJsonValidAndElementExists
[       OK ] ConfigReaderTest.test_read_start_address_resultOk_ifJsonValidAndElementExists (2 ms)
[ RUN      ] ConfigReaderTest.test_read_start_address_resultFailed_ifJsonValidAndElementDoesNotExists
[       OK ] ConfigReaderTest.test_read_start_address_resultFailed_ifJsonValidAndElementDoesNotExists (2 ms)
[ RUN      ] ConfigReaderTest.test_read_footprint_size_resultOk_ifJsonValidAndElementExists
[       OK ] ConfigReaderTest.test_read_footprint_size_resultOk_ifJsonValidAndElementExists (2 ms)
[ RUN      ] ConfigReaderTest.test_read_footprint_size_resultFailed_ifJsonValidAndElementDoesNotExists
[       OK ] ConfigReaderTest.test_read_footprint_size_resultFailed_ifJsonValidAndElementDoesNotExists (1 ms)
[----------] 8 tests from ConfigReaderTest (14 ms total)

[----------] Global test environment tear-down
[==========] 8 tests from 1 test suite ran. (14 ms total)
[  PASSED  ] 8 tests.