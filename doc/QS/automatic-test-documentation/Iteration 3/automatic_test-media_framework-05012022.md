# Testausführung
durchgeführt von Sofia Aranda am 05.01.2022 in Iteration 3

## Testziele
Klasse              :     MediaBlackBox
Dazugehörige Dateien:     media_framework.hpp, media_framework.cpp

## Code Coverage
Das Code Coverage liegt bei 100%

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_start_media_stateIsNull_ifNoMediaSet
[       OK ] MediaBlackboxTest.test_start_media_stateIsNull_ifNoMediaSet (394 ms)
[----------] 1 test from MediaBlackboxTest (395 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (395 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_start_media_stateIsPlaying_ifValidUri
[       OK ] MediaBlackboxTest.test_start_media_stateIsPlaying_ifValidUri (3223 ms)
[----------] 1 test from MediaBlackboxTest (3223 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (3223 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_start_media_stateIsNull_ifInvalidUri
[       OK ] MediaBlackboxTest.test_start_media_stateIsNull_ifInvalidUri (2067 ms)
[----------] 1 test from MediaBlackboxTest (2067 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (2069 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_stop_media_stateIsNull_ifNoMediaSet
[       OK ] MediaBlackboxTest.test_stop_media_stateIsNull_ifNoMediaSet (71 ms)
[----------] 1 test from MediaBlackboxTest (71 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (71 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_stop_media_stateIsNull_ifValidUri
[       OK ] MediaBlackboxTest.test_stop_media_stateIsNull_ifValidUri (3112 ms)
[----------] 1 test from MediaBlackboxTest (3112 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (3113 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_stop_media_stateIsNull_ifInvalidUri
[       OK ] MediaBlackboxTest.test_stop_media_stateIsNull_ifInvalidUri (2088 ms)
[----------] 1 test from MediaBlackboxTest (2088 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (2088 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_pause_media_stateIsNull_ifNoMediaSet
[       OK ] MediaBlackboxTest.test_pause_media_stateIsNull_ifNoMediaSet (85 ms)
[----------] 1 test from MediaBlackboxTest (85 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (85 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_pause_media_stateIsPaused_ifValidUri
[       OK ] MediaBlackboxTest.test_pause_media_stateIsPaused_ifValidUri (3095 ms)
[----------] 1 test from MediaBlackboxTest (3095 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (3095 ms total)
[  PASSED  ] 1 test.

[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from MediaBlackboxTest
[ RUN      ] MediaBlackboxTest.test_pause_media_stateIsNull_ifInvalidUri
[       OK ] MediaBlackboxTest.test_pause_media_stateIsNull_ifInvalidUri (2092 ms)
[----------] 1 test from MediaBlackboxTest (2092 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (2093 ms total)
[  PASSED  ] 1 test.





