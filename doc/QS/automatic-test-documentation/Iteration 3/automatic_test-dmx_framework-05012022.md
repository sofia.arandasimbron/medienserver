# Testausführung
Durchgeführt von Paul Hermann am 05.01.2022 in Iteration 3

## Testziele
Klasse              :     DMXManager
Dazugehörige Dateien:     DMXManager.hpp, DMXManager.cpp

## Code Coverage
Das Branch Coverage liegt für den Konstruktur bei 100%.
Die anderen Methoden werden mit manuellen Tests abgedeckt.

## Maßnahmen
Da alle Maßnahmen erfüllt und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.

## Testoutput (Konsolenoutput)
[==========] Running 10 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 10 tests from DMXFrameworkTests
[ RUN      ] DMXFrameworkTests.WrongChannelTest1
[       OK ] DMXFrameworkTests.WrongChannelTest1 (5 ms)
[ RUN      ] DMXFrameworkTests.WrongChannelTest2
[       OK ] DMXFrameworkTests.WrongChannelTest2 (0 ms)
[ RUN      ] DMXFrameworkTests.WrongUniverseTest1
[       OK ] DMXFrameworkTests.WrongUniverseTest1 (0 ms)
[ RUN      ] DMXFrameworkTests.WrongUniverseTest2
[       OK ] DMXFrameworkTests.WrongUniverseTest2 (0 ms)
[ RUN      ] DMXFrameworkTests.WrongFootprintTest1
[       OK ] DMXFrameworkTests.WrongFootprintTest1 (0 ms)
[ RUN      ] DMXFrameworkTests.WrongFootprintTest2
[       OK ] DMXFrameworkTests.WrongFootprintTest2 (1 ms)
[ RUN      ] DMXFrameworkTests.RightChannelTest1
[       OK ] DMXFrameworkTests.RightChannelTest1 (0 ms)
[ RUN      ] DMXFrameworkTests.RightChannelTest
[       OK ] DMXFrameworkTests.RightChannelTest (0 ms)
[ RUN      ] DMXFrameworkTests.RightUniverseTest
[       OK ] DMXFrameworkTests.RightUniverseTest (4 ms)
[ RUN      ] DMXFrameworkTests.RightFootprintTest
[       OK ] DMXFrameworkTests.RightFootprintTest (4 ms)
[----------] 10 tests from DMXFrameworkTests (15 ms total)

[----------] Global test environment tear-down
[==========] 10 tests from 1 test suite ran. (15 ms total)
[  PASSED  ] 10 tests.