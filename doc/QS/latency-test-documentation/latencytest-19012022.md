# Latenzmessung
durchgeführt von Paul Hermann am 19.01.2022 in Iteration 4

## Messung
Die Core-Anwendung ist noch nicht einsatzbereit.
Daher wird nur die Latenz vom DMX-Framework mit einer Testanwendung gemessen.
Die Messung läuft von Umspringen des Wertes in sACNView bis zur Ausgabe des entsprechenden Wertes in der Konsole.

## Latenz
- 17 Frames bei 960FPS
- entspricht ca. 17,7ms

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.