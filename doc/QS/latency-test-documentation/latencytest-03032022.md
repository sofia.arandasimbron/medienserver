# Latenzmessung
durchgeführt von Paul Hermann am 03.03.2022 in Iteration 7

## Messungen
Gemessen wird in der Core-Anwendung.
Es wird das Big Buck Bunny Video im H264 Codec bei 30FPS und 60FPS in den Auflösungen 1080p und 720p zum testen eingesetzt.
Da das Video schwarz beginnt, wurde die erste Sekunde herausgeschnitten, um besser den Anfang des Videos erkennen zu können.

Bei beiden Videos wird die Latenz in folgendem Bereich gemessen:
Beginn: Umsprung der Zahl für einen DMX-Wert in sACNView.
Ende: Erste Änderung des Videos auf dem Bildschirm des Raspberry Pi.

## Latenzen

### Keine Wiedergabe --> 720p 30FPS
- 128 Frames bei 960FPS
- entspricht ca. 133ms bzw. 0,13s

### Keine Wiedergabe --> 1080p 30FPS
- 213 Frames bei 960FPS
- entspricht ca. 221ms bzw. 0,22s

### Keine Wiedergabe --> 1080p 60FPS
- 273 Frames bei 960FPS
- entspricht ca. 284ms bzw. 0,28s

### 1080p 60FPS --> 720p 30FPS
- 224 Frames bei 960FPS
- entspricht ca. 233ms bzw. 0,23s

### 720p 30FPS --> 1080p 30FPS
- 335 Frames bei 960FPS
- entspricht ca. 349ms bzw. 0,35s
- davon die letzten 179 Frames also 0,19s schwarz

### 1080p 30FPS --> 1080p 60FPS
- 297 Frames bei 960FPS
- entspricht ca. 309ms bzw. 0,31s
- davon die letzten 136 Frames also 0,14s schwarz
### 1080p 60FPS --> Keine Wiedergabe
- 23 Frames bei 960FPS
- entspricht ca. 24ms bzw. 0,02s

## Anmerkung
Die Zeiten vom Wechsel auf 720p 30FPS haben sich von 0,18s (Iteration 6) auf 0,23s (Iteration 7) verschlechtert.
Die Zeiten vom Wechsel auf 1080p 30FPS haben sich von 0,16s (Iteration 6) auf 0,22s (Iteration 7) verschlechtert.
Grund dafür ist mit hoher wahrscheinlichkeit die länger gewordene Pipeline mit mehr Pipelinestufen. Hierdurch erhöht sich die Latenz.
In kommenden Iterationen kann diese Latenz ggf. noch an einigen Stellen innerhalb einer eigenen Userstory optimiert werden.

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.