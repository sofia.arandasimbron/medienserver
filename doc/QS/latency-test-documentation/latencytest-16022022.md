# Latenzmessung
durchgeführt von Paul Hermann am 16.02.2022 in Iteration 6

## Messungen
Die Core Anwendung ist ab dieser Iteration einsatzbereit.
Es wird das Big Buck Bunny Video im H264 Codec bei 30FPS in den Auflösungen 1080p und 720p zum testen eingesetzt.
Zur Messung wird von einem anderen Video auf das zu messende Video gewechselt.

Bei beiden Videos wird die Latenz in folgendem Bereich gemessen:
Beginn: Umsprung der Zahl für einen DMX-Wert in sACNView.
Ende: Erste Änderung des Videos auf dem Bildschirm des Raspberry Pi.

## Latenzen

### 720p
- 174 Frames bei 960FPS
- entspricht ca. 181ms bzw. 0,18s

### 1080p
- 152 Frames bei 960FPS
- entspricht ca. 158ms bzw. 0,16s

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.

Weiterer Vorschlag: Das Video sollte am Anfang gekürzt werden, um die kurze Schwarz-Phase zu Videobeginn nicht mit in der Messung zu haben.
Für diese Messreihe konnte das ganze rausgerechnet werden.