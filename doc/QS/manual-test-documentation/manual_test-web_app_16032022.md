# Testausführung - Web app Test
durchgeführt von Sofia Aranda am 16.03.2022 in Iteration 8

## Testvoraussetzungen
- Raspberry Pi und Laptop befinden sich im gleichen Netzwerk
- Raspberry Pi hat Git Repo Main Branch auf aktuellstem Stand (am 16.03.2022)

## Testablauf

### Test 1 (Header)
Beschreibung Test 1 (Header) Schritt für Schritt mit Ergebnisse:

1) Durch den Zahnrad öffnet sich ein Popup-Fenster. (Passed!)
2) Somit können Nutzer*innen `Universe` und `Start Adress` eingeben, editieren und speichern. (Passed!)
3) Nun wird diese Änderung auf das GUI sichtbar (im Header Oben) und in `config.json` gespeichert. (Passed!)
    3.1) Kleiner Bug: Wenn 2 Nutzer*innen auf dem Webserver mit 2 unterschiedliche IP-Adressen und Endgeräte zugreifen, werden nur die Änderung vom Nutzer*in übernommen, die/der gerade die Visual Studio Code Instanz offen hat. (Failed!)  
4) Die Eingaben sind jedoch begrenzt: (Passed!)
    `Universe range (1, 63999)`
    `Start Adress range (1, 511)` <- dies hängt von die gegebene Footprint an: wird dynamisch gerechnet. In diesem Fall ist Footprint = 2.
5) Wenn Nutzer*innen eine ungültige `Universe` und/oder `Start Adress` eingeben, bekommen sie ein Hinweis für die falsche Eingabe. (Passed!)
6) Ungültige eingaben werden nicht gespeichert. (Passed!)
7) Nach dem canceln bleibt alles unverändert: die letzte gültige Eingaben bleiben gespeichert. (Passed!)
8) Nach dem Speichern oder "canceln" schließt sich das Popup-Fenster. (Passed!)

### Test 2 (File Manager)
Beschreibung Test 2 (File Manager) Schritt für Schritt mit Ergebnisse:

1) File Manager ist nach dem starten in Frontend sichtbar. (Passed!)
2) Funktionalitäten:
    2.1) Navigation: Es ist möglich, zwischen Lokale Ordnern aus dem Pi zu wechseln um Dateien und Ordnern zu lesen. (Passed!)
    2.2) New Folder: Es ist möglich, ein neuer Ordner in ein von Nutzer*in ausgewählter Ort zu erzeugen. (Passed!)
    2.4) Upload: Es ist möglich, Dateien in ein von Nutzer*in ausgewählter Ort hochzuladen. (Passed!)
    2.5) Sort by: Nutzer*innen können Dateien und Ordnern im File Manager nach Name, Size, zuletzt Modified, Ascending oder Descending anordnen. (Passed!)
    2.6) Refresh: Dateien und Ordnern werden nach dem clicken der "Refresh" Button aktuallisiert. (Passed!)
    2.7) View: Nutzer*innen können zwischen "Large Icons - Ansicht" und "Details - Ansicht" auswählen und jederzeit von Ansicht wechseln. (Passed!)
    2.8) Details: Nutzer*innen können sich jederzeit die Details von dem ausgewähltem Ordner anschauen. (Passed!)
    2.9) Add Element: Nutzer*innen können ausgewählte Dateien unter die Elementen-Liste der ausgewählte Liste hinzufügen. (Passed!)
    2.10) Delete: Es ist möglich, ausgewählte (Auswahl erfolgt mit ein click auf das Element) Dateien/Ordnern zu löschen. (Passed!)
    
### Test 3 (Lists)
Beschreibung Test 3 (Lists) Schritt für Schritt mit Ergebnisse:

1) Lists ist nach dem starten von backend sichtbar. (Passed!)
2) Das Button create Lists erzeugt ein Popup Fenster. (Passed!)
    2.1) Im Popup Fenster können Nutzer*innen eine neue Liste erzeugen, indem sie ein Name eingeben und auf "save" drücken. (Passed!)
    2.2) Jedoch darf der Name der Liste nicht ein empty String sein. (Passed!)
    2.3) Nach dem canceln bleibt alles unverändert: es wird keine neue Liste generiert. (Passed!)
    2.4) Nach dem Speichern oder "canceln" schließt sich das Popup-Fenster. (Passed!)
    2.5) Drag and drop: Nutzer*innen können die Reihenfolge der listen per Drag and Drop ändern. Alle Elemente aus den Listen bleiben jedoch unverändert in die respektiven Listen. Die Reihenfolge der Element-List der Liste wird nicht beeinflüsst. (Passed!)
    2.6) Delete: Nutzer*innen können Listen aus der Lists Block entfernen, indem sie auf das Kreuz clicken. (Passed!)

### Test 4 (Elements)
Beschreibung Test 4 (Elements) Schritt für Schritt mit Ergebnisse: 

1) Elements ist nach dem starten von backend sichtbar. (Passed!)
2) Drag and drop: Nutzer*innen können die Reihenfolge der Elementen per Drag and Drop ändern. (Passed!)
3) Delete: Nutzer*innen können Elemente aus der Elements Block entfernen, indem sie auf das Kreuz clicken. (Passed!)

## Maßnahmen
Da alle Voraussetzungen und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.
