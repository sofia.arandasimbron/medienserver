# Testausführung - DMX Framework Test
durchgeführt von Paul Hermann am 02.03.2022 in Iteration 7

## Testvoraussetzungen
- Raspberry Pi und Macbook befinden sich im gleichen Netzwerk
    - Macbook über WLAN angebunden
- Multicast ist im Netzwerk zugelassen
- Raspberry Pi hat Git Repo Main Branch auf aktuellstem Stand (am 02.03.2022)
- Macbook hat sACNView v2.1.2 installiert
- Mediendateien sind vorhanden
- config.json und medien.json existieren nicht.

## Testablauf

### Test 1
- Alle Ergebnisse wie erwartet

### Test 2
- Alle Ergebnisse wie erwartet

### Test 3
- Alle Ergebnisse wie erwartet

### Test 4
- Alle Ergebnisse wie erwartet

### Test 5
- Alle Ergebnisse wie erwartet

### Test 6
- Alle Ergebnisse wie erwartet

### Test 7
- Alle Ergebnisse wie erwartet

### Test 8
- Alle Ergebnisse wie erwartet

### Test 9
- Alle Ergebnisse wie erwartet

### Test 10
- Alle Ergebnisse wie erwartet

### Test 11
- Alle Ergebnisse wie erwartet

## Maßnahmen
Da alle Voraussetzungen und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.