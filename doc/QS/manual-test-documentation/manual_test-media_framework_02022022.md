# Testausführung - Medienframework Test
durchgeführt von Paul Hermann am 02.02.2022 in Iteration 5

## Testvoraussetzungen
- Raspberry Pi benötigt Medienserver Git Repository sind auf dem aktuellsten Stand
- Raspberry Pi hat einen externen Bildschirm per HDMI angeschlossen
- Folgende Mediendateien sind vorhanden:
  - `/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4`
  - `/home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4`
- Anwendung ist mit CMake gebaut

## Testablauf
Alle 15 Testschritte liefern das erwartete Ergebnis.

## Maßnahmen
Da alle Voraussetzungen und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.