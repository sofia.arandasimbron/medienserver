# Testausführung - DMX Framework Test
durchgeführt von Paul Hermann am 02.02.2022 in Iteration 5

## Testvoraussetzungen
- Raspberry Pi und Macbook befinden sich im gleichen Netzwerk
    - Macbook über WLAN angebunden
- Multicast ist im Netzwerk zugelassen
- Raspberry Pi hat Git Repo Main Branch auf aktuellstem Stand (am 02.02.2022)
- Macbook hat sACNView v2.1.2 installiert

## Testablauf

### Test 1
- Alle Ergebnisse wie erwartet

### Test 2
- Alle Ergebnisse wie erwartet

### Test 3
- Alle Ergebnisse wie erwartet

### Test 4
- Alle Ergebnisse wie erwartet

### Test 5
- Alle Ergebnisse wie erwartet

## Maßnahmen
Da alle Voraussetzungen und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.