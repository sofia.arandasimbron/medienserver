# Testausführung - Medienframework Test
durchgeführt von Paul Hermann am 05.01.2022 in Iteration 3

## Testvoraussetzungen
- Raspberry Pi benötigt Medienserver Git Repository sind auf dem aktuellsten Stand
- Raspberry Pi hat einen externen Bildschirm per HDMI angeschlossen
- Folgende Mediendateien sind vorhanden:
  - `/home/pi/media/bbb_sunflower_720p_30fps_h264.mp4`
  - `/home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4`
- Anwendung ist mit CMake gebaut

## Testablauf
Bei Tests 11 und 13 in der Checkliste tritt nicht das erwartete Ergebnis auf.

Bei Test 11 wird ein Video abgespielt, welches zuletzt mit validem Pfad gesetzt wurde.

Bei Test 13 wird das Video nicht pausiert sondern läuft weiter.

## Maßnahmen
Es wurde ein Issue in GitLab angelegt und die Entwickler wurden informiert, welche die Fehler beheben konnten.
Im Anschluss wurden sämtliche Tests noch einmal ausgeführt.
In diesem Durchlauf lieferten alle Tests das erwartete Ergebnis.
Da ab diesem Punkt alle Voraussetzungen und alle Testergebnisse erfüllt sind, sind keine weiteren Maßnahmen notwendig.