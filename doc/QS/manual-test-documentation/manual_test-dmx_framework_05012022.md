# Testausführung - DMX Framework Test
durchgeführt von Paul Hermann am 05.01.2022 in Iteration 3

## Testvoraussetzungen
- Raspberry Pi und Macbook befinden sich im gleichen Netzwerk
    - Macbook über WLAN angebunden
- Multicast ist im Netzwerk zugelassen
- Raspberry Pi hat Git Repo Main Branch auf aktuellstem Stand (am 05.01.2022)
- Macbook hat sACNView v2.1.2 installiert

## Testablauf

### Test 1
- Alle Ergebnisse wie erwartet

### Test 2
- Alle Ergebnisse wie erwartet

### Test 3
- Probleme bei folgendem Schritt: Fader 511 in sACNView von 0 bis 255 hochziehen
    - Erwartetes Ergebnis: Die ausgegebenen Werte bleiben unverändert. Wenn ein Wert außerhalb des Footprint-Bereiches verändert wird, kommen weiterhin sekündlich Pakete, nicht häufiger.
    - Reales Ergebnis: Wenn ein Wert außerhalb des Footprint-Bereiches verändert wird, werden die Werte deutlich häufiger als sekündlich ausgegeben (bis ca. 40 Werte pro Sekunde).

### Test 4
- Alle Ergebnisse wie erwartet

### Test 5
- Alle Ergebnisse wie erwartet

## Maßnahmen
Es wird ein Issue angelegt, um Werte nur auszugeben, wenn Werte innerhalb des Footprints verändert werden.
Hierzu muss dafür gesorgt werden, dass die spefizifierte Callback-Funktion des Frameworks nur dann aufgerufen wird, wenn Werte innerhalb des spezifizierten Footprints aktualisiert werden.
Das Framework wurde noch innerhalb der Iteration von Paul Hermann angepasst, gereviewed und neu getestet.
Im Anschluss haben alle Tests das erwartete Ergebnis geliefert.