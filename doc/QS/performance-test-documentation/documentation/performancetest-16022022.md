# Performancetest
durchgeführt von Paul Hermann am 16.02.2022 in Iteration 6

## Ergebnisse
Aktueller Stand der Messung ist folgende Wiedergabepipeline:

```gst-launch-1.0 uridecodebin uri=file:///home/pi/media/bbb_sunflower_720p_30fps_h264.mp4 ! videobalance ! videoconvert ! kmssink```

Es wurden Pipelinegraphen und Performancestatistiken für folgende Videocodecs, Auflösungen und Framerates bei der Ausführung auf einem Raspberry Pi 4 erstellt:
- Videocodecs
  - H264
- Auflösungen
  - 1280 x 720
  - 1920 x 1080
  - 3840 x 2160 (nur 30FPS)
- Framerates
  - 30 FPS
  - 60 FPS

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.
Zusätzlich werden die Ergebnisse von den Entwickler*innen verwendet, um die Performance zu optimieren.