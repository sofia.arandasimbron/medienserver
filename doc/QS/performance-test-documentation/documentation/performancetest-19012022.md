# Performancetest
durchgeführt von Paul Hermann am 19.01.2022 in Iteration 4

## Ergebnisse
Aktueller Stand der Messung ist folgende Wiedergabepipeline:

```gst-launch-1.0 filesrc location=/media/usb0/BP-Testmedia/bbb_sunflower_720p_30fps_DNxHR.mxf ! typefind ! qtdemux ! multiqueue max-size-bytes=2097152 max-size-time=0 ! h264parse ! v4l2h264dec ! videoconvert ! deinterlace ! queue max-size-buffers=3 max-size-bytes=0 max-size-time=0 silent=TRUE ! videoconvert ! videoscale ! videobalance ! videoconvert ! fbdevsink```

Es wurden Pipelinegraphen und Performancestatistiken für folgende Videocodecs, Auflösungen und Framerates bei der Ausführung auf einem Raspberry Pi 4 erstellt:
- Videocodecs
  - H264
- Auflösungen
  - 1280 x 720
  - 1920 x 1080
- Framerates
  - 30 FPS
  - 60 FPS

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.
Zusätzlich werden die Ergebnisse von den Entwickler*innen verwendet, um die Performance zu optimieren.