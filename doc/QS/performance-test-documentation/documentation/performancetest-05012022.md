# Performancetest
durchgeführt von Paul Hermann am 05.01.2022 in Iteration 3

## Ergebnisse
Aktueller Stand der Messung ist folgende Wiedergabepipeline:

```gst-launch-1.0 playbin uri="file:///media/usb0/BP-Testmedia/bbb_sunflower_720p_30fps_DNxHR.mxf" video-sink=fbdevsink```

Es wurden Pipelinegraphen und Performancestatistiken für folgende Videocodecs, Auflösungen und Framerates bei der Ausführung auf einem Raspberry Pi 4 erstellt:
- Videocodecs
  - DNxHR
  - H264
  - H265
  - ProRes 422
  - ProRes 4444
- Auflösungen
  - 1280 x 720
  - 1920 x 1080
  - (Für H264 3840x2160)
- Framerates
  - 30 FPS
  - 60 FPS

Der HAP-Codec wird vom GStreamer-Framework nicht unterstützt, weshalb dieser nicht getestet werden konnte.

Für die Codecs DNxHR, H265, ProRes 422 und ProRes 4444 war ein Test in höherer Auflösung als FullHD nicht möglich, da die Performance hierfür so unzureichend war, dass die Ausführung nicht fertiggestellt werden konnte.

Weitere Anmerkung:
Die gute Performance des H264 Codecs ist auf Hardwarebeschleunigung im Raspberry Pi zurückzuführen.

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.

Vom Auftraggebenden wurde der H264 Codec als wichtigster Codec eingestuft.

Zusätzlich werden die Ergebnisse von den Entwickler*innen verwendet, um die Performance zu optimieren.