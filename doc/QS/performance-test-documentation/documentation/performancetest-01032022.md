# Performancetest
durchgeführt von Paul Hermann am 01.03.2022 in Iteration 7

## Ergebnisse
Aktueller Stand der Messung ist folgende Wiedergabepipeline:

```gst-launch-1.0 uridecodebin uri=file:///home/pi/media/bbb_sunflower_1080p_30fps_h264.mp4 ! videobalance ! videoconvert ! videoscale n-threads=4 ! video/x-raw, width=1920, height=1080, format=I420 ! queue ! compositor name=comp background=3 sink_0::zorder=2 sink_1::zorder=1 ! video/x-raw, width=1920, height=1080, format=I420 ! kmssink videotestsrc pattern=2 ! video/x-raw, width=1920, height=1080, framerate=1/1, format=I420 ! comp.```

Es wurden Pipelinegraphen und Performancestatistiken für folgende Videocodecs, Auflösungen und Framerates bei der Ausführung auf einem Raspberry Pi 4 erstellt:
- Videocodecs
  - H264
- Auflösungen
  - 1280 x 720
  - 1920 x 1080
- Framerates
  - 30 FPS
  - 60 FPS

## Anmerkung zu Ergebnissen
Die 720p Videos ruckeln stark, die CPU ist bei 100% Auslastung. Das liegt daran, dass durch die neue Pipeline zusätzliche Skalierung notwendig wird, wenn man andere Auflösungen als FullHD verwendet, um das Video auf FullHD zu skalieren. Die Skalierung benötigt sehr viel Rechenleistung und bremst damit die ganze Pipeline.
Das Problem wird ggf. in kommenden Iterationen in Rücksprache mit dem AG angegangen. Bis dahin wird empfohlen, nur FullHD Videos zu rendern.

## Maßnahmen
Die Ergebnisse wurden an den Auftraggebenden übermittelt.
Zusätzlich werden die Ergebnisse von den Entwickler*innen verwendet, um die Performance zu optimieren.