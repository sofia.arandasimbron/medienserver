# QS-Overview

## Übersicht Was/Wann
- Ende jeder Iteration
  - Manuelle Tests
  - Latenzmessungen
  - Ressourcenmessungen
  - Linting
- Bei jedem Merge in Main
  - Automatisierte Tests
  - Code Reviews

## Automatisierte Tests
- Wann?
  - Vor jedem Merge
- Wie?
  - Anwendung in local_app bauen und ctest ausführen
- Von wem?
  - Festlegung in Iteration
- Wie dokumentieren?
  - Dokument in `/QS/automatic-test-documentation`
  - Konsolenausgabe von Tests in Dokument abspeichern
  - Person, Zeitpunkt, Code Coverage und ggf. Maßnahmen dokumentieren

## Manuelle Tests
- Wann?
  - Ende jeder Iteration
- Wie?
  - Alle manuellen Testchecklisten in `manual-test-checklists` durchgehen
- Von wem?
  - Festlegung in Iteration
- Wie dokumentieren?
  - Dokument in `/QS/manual-test-documentation`
  - Vorlage für Testdokumentation nutzen (liegt in Ordner)
  - Person, Zeitpunkt und ggf. Maßnahmen dokumentieren

## Latenzmessungen
- Wann?
  - Ende jeder Iteration
- Wie?
  - Mit Zeitrafferaufnahme sACNView und Anwendung filmen, Frames zählen
- Von wem?
  - Paul
- Wie dokumentieren?
  - Ergebnisse in `/QS/latency-test-documentation/` ablegen
  - Vorlage für Testdokumentation nutzen (liegt in Ordner)
  - Person und Zeitpunkt dokumentieren

## Ressourcenmessungen
- Wann?
  - Ende jeder Iteration
- Wie?
  - Messung von Anwendung mit `/local_app/general_tools/gst-performance-test/performancetest.sh`
- Von wem?
  - Festlegung in Iteration
- Wie dokumentieren?
  - Ergebnisse in `/QS/performance-test-documentation/` ablegen
  - Vorlage für Testdokumentation nutzen (liegt in Ordner)
  - Person und Zeitpunkt dokumentieren

## Linting
- Wann?
  - Ende jeder Iteration
- Wie?
  - Linting-Branch anlegen, Anwendung aus local_app bauen, Änderungen committen, mergen
- Von wem?
  - Festlegung in Iteration
- Wie dokumentieren?
  - Ergebnisse in `/QS/linting-documentation/` eintragen
  - Vorlage für Testdokumentation nutzen (liegt in Ordner)
  - Person und Zeitpunkt dokumentieren, ggf. auch Maßnahmen

## Code Reviews
- Wann?
  - Bei jedem Merge in Main
- Wie?
  - Anwendung aus Branch ausführen
  - Review in Merge-Changes schreiben
  - Maßnahmen auch dokumentieren! (von Bearbeiter der Maßnahmen)
- Von wem?
  - Festlegung spontan
- Wie dokumentieren?
  - Ergebnisse in `/QS/code-reviews/` eintragen
  - Vorlage für Testdokumentation nutzen (liegt in Ordner)
  - Person und Zeitpunkt dokumentieren, ggf. auch Maßnahmen
