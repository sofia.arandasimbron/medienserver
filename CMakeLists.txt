cmake_minimum_required(VERSION 3.16)

project(Mediaserver)

set(CMAKE_INSTALL_COMPONENT "ON")

# Add sub projects
add_subdirectory(local_app)

# Copy the executable with permissions to /bin/
install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/local_app/core/core
        DESTINATION /usr/bin/mediaserver
    PERMISSIONS
        OWNER_EXECUTE
)

# Copy the apache2.config in apache2 dir
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/config_files/apache2.conf DESTINATION "/etc/apache2/sites-available")

# Copy backup for the apache2.config in .config
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/config_files/apache2.conf DESTINATION "~/.config/mediaserver" RENAME apache2.conf.default)

# Copy the medienserver.config in apache2 dir
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/config_files/medienserver.conf DESTINATION "/etc/apache2")

# Copy backup for the medienserver.config in .config
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/config_files/medienserver.conf DESTINATION "~/.config/mediaserver" RENAME medienserver.conf.default)

# Copy the medien.json in .config
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/files/medien.json DESTINATION "/usr/bin/mediaserver")

# Copy the config.json in .config
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/files/config.json DESTINATION "/usr/bin/mediaserver")

# Copy frontend files into var/www/hmtl
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/web_app/frontend/dist/frontend/ DESTINATION "/var/www/html")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/ConfigManager.ts DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/index.ts DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/init.ts DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/ListManager.ts DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/package.json DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/backend/tsconfig.json DESTINATION "/usr/bin/mediaserver/backend")

# Copy backend files /usr/bin/mediaserver/backend 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/web_app/openapi.yml DESTINATION "/usr/bin/mediaserver")

# Copy start skript file in /usr/bin 
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/config_files/mediaserver-start DESTINATION "/usr/bin"
        PERMISSIONS
            OWNER_EXECUTE
        )