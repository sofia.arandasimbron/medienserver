openapi: "3.0.2"
info:
  title: API Title
  version: "1.0"
servers:
  - url: http://localhost:4222

paths:
  #
  # CONFIG-JSON SPEC.
  #
  /api/config/:
    get:
      operationId: "getConfig"
      summary: Get config file as an array
      responses:
        "200":
          $ref: "#/components/responses/integerConfigResponse"

  /api/config/universe:
    get:
      operationId: "getUniverse"
      summary: get the current value of universe 
      responses:
        "200":
          $ref: "#/components/responses/integerUniverseResponse"

  /api/config/start-address:
    get:
      operationId: "getStartAddress"
      summary: get the current value of start address 
      responses:
        "200":
          $ref: "#/components/responses/integerStartAddressResponse"
  

  /api/config/foot-print:
    get:
      operationId: "getFootPrint"
      summary: get the current value of footprint 
      responses:
        "200":
          $ref: "#/components/responses/integerFootprintResponse"  

  /api/config/update:
    post:
      summary: "Update the config file"
      operationId: "updateConfigFile"
      requestBody:
        $ref: "#/components/requestBodies/writeConfRequest"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"      

  #
  # MEDIEN-JSON SPEC.
  #
  /api/lists/:
    get:
      operationId: "getLists"
      summary: Get all 256 Lists of the media lists as an array
      responses:
        "200":
          $ref: "#/components/responses/stringListResponse"

  /api/lists/identifier:
    get:
      operationId: "getListIdentifiers"
      summary: Get all 256 identifiers of the media lists as an array
      responses:
        "200":
          $ref: "#/components/responses/stringListResponse"

  /api/list/{list_id}/identifier:
    parameters:
      - $ref: "#/components/parameters/list_id"
    get:
      summary: "Get an identifier from a list"
      operationId: "getListIdentifier"
      responses:
        "200":
          $ref: "#/components/responses/identifierResponse"

  /api/list/{list_id}/elements:
    parameters:
      - $ref: "#/components/parameters/list_id"
    get:
      summary: "Get all elements from a list"
      operationId: "getElements"
      responses:
        "200":
          $ref: "#/components/responses/elementListReponse"

  /api/list/{list_id}/elements/identifier:
    parameters:
      - $ref: "#/components/parameters/list_id"
    get:
      summary: "Get all identifiers from elements from a list"
      operationId: "getElementIdentifiers"
      responses:
        "200":
          $ref: "#/components/responses/stringListResponse"

  /api/list/{list_id}/elements/path:
    parameters:
      - $ref: "#/components/parameters/list_id"
    get:
      summary: "Get all paths from elements from a list"
      operationId: "getElementPaths"
      responses:
        "200":
          $ref: "#/components/responses/stringListResponse"

  /api/list/{list_id}/element/{elem_id}/identifier:
    parameters:
      - $ref: "#/components/parameters/list_id"
      - $ref: "#/components/parameters/element_id"
    get:
      summary: "Get the identifier of an element in a list"
      operationId: "getElementIdentifier"
      responses:
        "200":
          $ref: "#/components/responses/identifierResponse"

  /api/list/{list_id}/element/{elem_id}/path:
    parameters:
      - $ref: "#/components/parameters/list_id"
      - $ref: "#/components/parameters/element_id"
    get:
      summary: "Get the path of an element in a list"
      operationId: "getElementPath"
      responses:
        "200":
          $ref: "#/components/responses/pathResponse"

  /api/lists/add:
    post:
      summary: "Add a new list with an identifier and get the index where it was added"
      operationId: "addList"
      requestBody:
        $ref: "#/components/requestBodies/identifierRequest"
      responses:
        "200":
          $ref: "#/components/responses/indexResponse"

  /api/list/{list_id}/delete:
    parameters:
      - $ref: "#/components/parameters/list_id"
    post:
      summary: "Delete the selected list"
      operationId: "deleteList"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"

  /api/list/{list_id}/element/add:
    parameters:
      - $ref: "#/components/parameters/list_id"
    post:
      summary: "Add a new element to the selected list"
      operationId: "addElement"
      requestBody:
        $ref: "#/components/requestBodies/elementRequest"
      responses:
        "200":
          $ref: "#/components/responses/indexResponse"

  /api/list/{list_id}/element/{elem_id}/delete:
    parameters:
      - $ref: "#/components/parameters/list_id"
      - $ref: "#/components/parameters/element_id"
    post:
      summary: "Delete the selected element"
      operationId: "deleteElement"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"
    
  /api/list/{list_id}/element/moveElement:
    parameters:
      - $ref: "#/components/parameters/list_id"
    post:
      summary: "Move the selected element"
      operationId: "moveElement"
      requestBody:
        $ref: "#/components/requestBodies/moveRequest"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"
  
  /api/list/moveList:
    post:
      summary: "Move the selected list"
      operationId: "moveList"
      requestBody:
        $ref: "#/components/requestBodies/moveRequest"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"

  /api/list/update:
    post:
      summary: "Update the list"
      operationId: "updateList"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"
    
  /api/list/{list_id}/element/{elem_id}/update:
    parameters:
      - $ref: "#/components/parameters/list_id"
      - $ref: "#/components/parameters/element_id"
    post:
      summary: "Update the element"
      operationId: "updateElement"
      responses:
        "200":
          $ref: "#/components/responses/okResponse"

components:
  schemas:
    stringList:
      type: array
      minItems: 256
      maxItems: 256
      items:
        type: string

    configObject:
      type: array
      minItems: 3
      maxItems: 3
      items:
        type: integer

    universeObject:
      type: integer
      minimum: 1
      maximum: 63999 
     
    
    startAddressObject:
      type: integer
      minimum: 1
      maximum: 512

    footprintObject:
      type: integer
      minimum: 1
      maximum: 512

    writeConfObject:
      properties:
        universe:
          type: integer
          minimum: 1
          maximum: 63999
        startAddress:
          type: integer
          minimum: 1
          maximum: 512
        
    index:
      type: integer
      minimum: 0
      maximum: 255

    indexObject:
      type: object
      properties:
        idx:
          type: integer
          minimum: 0
          maximum: 255

    identifierObject:
      type: object
      properties:
        identifier:
          type: string

    pathObject:
      type: object
      properties:
        path:
          type: string

    elementObject:
      type: object
      properties:
        identifier:
          type: string
        path:
          type: string
    
    moveObject:
      type: object
      properties:
        previous:
          type: integer
          minimum: 0
          maximum: 255
        current:
          type: integer
          minimum: 0
          maximum: 255

    elementList:
      type: array
      minItems: 256
      maxItems: 256
      items:
        $ref: "#/components/schemas/elementObject"

  responses:
    indexResponse:
      description: "A single index"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/indexObject"

    stringListResponse:
      description: "A list of 256 identifiers"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/stringList"
    
    integerConfigResponse:
      description: "A config file with univesum index and a start address"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/configObject"
    
    integerUniverseResponse:
      description: "A value of current universe"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/universeObject"
    
    integerStartAddressResponse:
      description: "A value of current start address"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/startAddressObject"
    
    integerFootprintResponse:
      description: "A value of current footprint"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/footprintObject"

    identifierResponse:
      description: "A single identifier Object"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/identifierObject"

    pathResponse:
      description: "A single path Object"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/pathObject"

    elementListReponse:
      description: "An element list"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/elementList"

    okResponse:
      description: "OK"
      content:
        text/plain:
          schema:
            type: string

  requestBodies:
    identifierRequest:
      description: "A single identifier object"
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/identifierObject"

    elementRequest:
      description: "A single element object"
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/elementObject"
    
    moveRequest:
      description: "Move single element object"
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/moveObject"
    
    writeConfRequest:
      description: "Set the value in conf file "
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/writeConfObject"

  parameters:
    list_id:
      name: list_id
      in: path
      required: true
      description: "List Index"
      schema:
        $ref: "#/components/schemas/index"

    element_id:
      name: elem_id
      in: path
      required: true
      description: "Element Index"
      schema:
        $ref: "#/components/schemas/index"
  
