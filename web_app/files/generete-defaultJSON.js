const fs = require ("fs")

let lists = { "lists": []  };
let elements = []; 

// generate element list
for(let i = 0; i < 256; i++){
    elements.push(
        {
            "identifier": "",
            "path": ""
        }
    );
}
//generate list of lists 
for(let i = 0; i < 256; i++){
    lists["lists"].push({
        "identifier": "",
        "elements": elements.copyWithin()
    })
}

// convert generated object to string 
let jsonContent = JSON.stringify(lists, null, 4); 

// write the string into a json file (medien.json)
fs.writeFileSync("medien.json",jsonContent, 'utf8', function (err) {
    if (err) {
        return console.log(err);
    }
});