/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-unused-vars */
/*
* Imports 
*/
import type { Express } from "express";
import json_medien_data from "/usr/bin/mediaserver/medien.json";
import json_config_data from "/usr/bin/mediaserver/config.json";
import ListManager, { MediaJsonError, M_List } from "./ListManager";
import ConfigManager from "./ConfigManager";
import init from "./init";

const [app, port]: [Express, number] = init();
const lm: ListManager = new ListManager(json_medien_data);
const cm: ConfigManager = new ConfigManager(json_config_data);
lm.fixErrors();
const valid = lm.isValid();
console.log("Is Valid: " + valid);


/**################################################################
 **                         List Manager                          #
 **################################################################/

/******************************************************************
 * 
 *                      GET Section Lists
 * 
 ******************************************************************/

app.get("/", (req, res) => {
    res.send("Media Server");
});

/**
 * Returns a list of all M_List identifiers
 * ["name1","name2",...]
 */
app.get("/api/lists/identifier", (req, res) => {
    res.send(lm.getListIdentifiers());
});

/**
 * Returns the identifier of the M_List at index list_id
 * {identifier: string}
 */
app.get("/api/list/:list_id(\\d+)/identifier", (req, res) => {
    const id = Number(req.params["list_id"]);
    res.send({ identifier: lm.getListIdentifier(id) });
});

/******************************************************************
 * 
 *                      GET Section Elements
 * 
 ******************************************************************/

/**
 * Returns the a list of M_Element's of the M_List at index list_id
 * [{identifier:"e1",path:""}, ...]
 */
app.get("/api/list/:list_id(\\d+)/elements", (req, res) => {
    const id = Number(req.params["list_id"]);
    res.send(lm.getElements(id));
});

/**
 * Returns a list of identifiers of all M_Elements in the list at index list_id
 */
app.get("/api/list/:list_id(\\d+)/elements/identifier", (req, res) => {
    const id = Number(req.params["list_id"]);
    res.send(lm.getElementIdentifiers(id));
});

/**
 * Returns a list of paths of all M_Elements in the list at index list_id
 */
app.get("/api/list/:list_id(\\d+)/elements/path", (req, res) => {
    const id = Number(req.params["list_id"]);
    res.send(lm.getElementPaths(id));
});


/**
 * Returns the identifier of the M_Element at index elem_id in M_List at index list_id
 */
app.get("/api/list/:list_id(\\d+)/element/:elem_id(\\d+)/identifier", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    const elem_id = Number(req.params["elem_id"]);
    res.send({ identifier: lm.getElementIdentifier(list_id, elem_id) });
});

/**
 * Returns the path of the M_Element at index elem_id in M_List at index list_id
 */
app.get("/api/list/:list_id(\\d+)/element/:elem_id(\\d+)/path", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    const elem_id = Number(req.params["elem_id"]);
    res.send({ path: lm.getElementPath(list_id, elem_id) });
});

/******************************************************************
 * 
 *                      POST Section Lists
 * 
 ******************************************************************/
/**
 * Takes an object of the form
 * {identifier: ""}
 * and returns the index where it was added
 */
app.post("/api/lists/add", (req, res) => {
    const identifier = req.body.identifier;
    if (identifier === "" || identifier === null) {
        res.sendStatus(500);
    }
    const idx: number = lm.getFreeListSpot();
    lm.setListIdentifier(idx, identifier);
    res.send({ idx: idx });
});

app.post("/api/list/:list_id(\\d+)/delete", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    lm.removeList(list_id);
    res.sendStatus(200);
});


/******************************************************************
 * 
 *                      POST Section Elements
 * 
 ******************************************************************/

/**
 * Takes an object of the form
 * {identifier: "", path: ""}
 * and returns the index where it was added
 */
app.post("/api/list/:list_id(\\d+)/element/add", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    const identifier = req.body.identifier;
    const path = req.body.path;
    if (identifier === "" || identifier === null) {
        res.sendStatus(500);
    }
    const idx: number = lm.getFreeElementSpot(list_id);
    lm.setElementIdentifier(list_id, idx, identifier);
    lm.setElementPath(list_id, idx, path);
    res.send({ idx: idx });
});

/**
 * Deletes the element in a list
 */
app.post("/api/list/:list_id(\\d+)/element/:elem_id(\\d+)/delete", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    const elem_id = Number(req.params["elem_id"]);
    lm.removeElement(list_id, elem_id);
    res.sendStatus(200);
});
app.post("/api/list/update", (req, res) => {
    lm.fixErrors();
    lm.writeToFile();
    res.sendStatus(200);
});

app.post("/api/list/:list_id(\\d+)/element/:element_id(\\d+)/update", (req, res) => {
    lm.fixErrors();
    lm.writeToFile();
    res.sendStatus(200);
});

app.post("/api/list/:list_id(\\d+)/element/moveElement", (req, res) => {
    const list_id = Number(req.params["list_id"]);
    const newIdx = req.body.current;
    const oldIdx = req.body.previous;

    lm.moveElement(list_id, newIdx, oldIdx);
    res.sendStatus(200);
});

app.post("/api/list/moveList", (req, res) => {
    const newIdx = req.body.current;
    const oldIdx = req.body.previous;
    lm.moveList(newIdx, oldIdx);
    res.sendStatus(200);
});

/**################################################################
 **                         Config Manager                        #
 **################################################################
 */

/******************************************************************
 * 
 *                         GET Section
 * 
 ******************************************************************/
app.get("/api/config", (req, res) => {
    res.send(cm.getConfig());
});
app.get("/api/config/universe", (req, res) => {
    res.send(cm.getUniverse());
});
app.get("/api/config/start-address", (req, res) => {
    res.send(cm.getStartAddress());
});
app.get("/api/config/foot-print", (req, res) => {
    res.send(cm.getFootprintSize());
});

/******************************************************************
 * 
 *                         POST Section
 * 
 ******************************************************************/
app.post("/api/config/update", (req, res) => {
    const universeIdx = req.body.universe;
    const startAddress = req.body.startAddress;
    cm.setUniverse(universeIdx);
    cm.setStartAddress(startAddress);
    cm.writeToFile();
    res.sendStatus(200);
});

app.listen(port, () => {
    console.log(`Backend listening on http://localhost:${port}`);
});