/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */

import express from "express";
import morgan from "morgan";
import bodyParser from "body-parser";
import type { Express } from "express";
import cors from "cors";
import * as OpenApiValidator from "express-openapi-validator";

export default function init(): [Express, number] {
    const app: Express = express();
    const port = 4222;

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    //APP
    const whitelist = ["http://localhost:8080"];
    app.use(cors({
        origin: ["http://localhost:8080"],
        "methods": "GET,PUT,POST",
        "preflightContinue": false,
        "optionsSuccessStatus": 204,
        credentials: true
    }));
    
    const corsOptionsDelegate = function (req:any , callback:any) {
        let corsOptions;
        if (whitelist.indexOf(req.header("Origin")) !== -1) {
            corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
        } else {
            corsOptions = { origin: false }; // disable CORS for this request
        }
        callback(null, corsOptions); // callback expects two parameters: error and options
    };
    app.get("/api", cors(corsOptionsDelegate), function(req, res){ });

    app.use(morgan("dev"));

    app.use(
        OpenApiValidator.middleware({
            apiSpec: "../openapi.yml",
            validateRequests: true, // (default)
            validateResponses: true, // false by default
        }),
    );

    app.use((err: any, req: any, res: any, next: any) => {
        // format error
        res.status(err.status || 500).json({
            message: err.message,
            errors: err.errors,
        });
    });

    app.use(bodyParser.json());
    app.set("trust proxy", ["loopback", "linklocal", "uniquelocal"]);
    return [app, port];
}