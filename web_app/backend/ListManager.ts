/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import fs from "fs";

/**
 * Element Interface representing Media Elements
 */
export interface M_Element {
    identifier: string;
    path: string;
}

/**
 * List Interface representing Media Lists
 */
export interface M_List {
    identifier: string;
    elements: M_Element[];
}

/**
 * MediaJson interface representing the media.json file
 */
export interface MediaJson {
    lists: M_List[]
}

/**
 * Representing an error in the media.json file which can be fixed with a call to fix()
 */
export interface MediaJsonError {
    message: string;
    fix: () => void;
}

/**
 * This function generates an empty element according to the specification
 * @returns an empty element
 */
function gen_element(): M_Element {
    return { identifier: "", path: "" };
}

/**
 * This function generates a list of 256 empty elements according to the specification
 * @returns a list of 256 elements
 */
function gen_element_list(): M_Element[] {
    return Array(256).fill(gen_element());
}

/**
 * This function generates a media list element with 256 empty elements according to the specification
 * @returns a media list
 */
function gen_mlist(): M_List {
    return { identifier: "", elements: gen_element_list() };
}

/**
 * This function generates a list of 256 empty media lists according to the specification
 * @returns a list of 256 media list elements
 */
function gen_mlist_list(): M_List[] {
    return Array(256).fill(gen_mlist());
}

/**
 * This function generates an empty media.json object according to the specification
 * @returns a media json object
 */
function gen_media_json(): MediaJson {
    return { lists: gen_mlist_list() };
}

/**
 * This class is used for managing the media.json file internally. It provides a useful wrapper for interacting with the object
 * - setListIdentifier()
 * - setElementIdentifier()
 * - setElementPath()
 * 
 * - getListIdentifier()
 * - getElementIdentifier()
 * - getElementPath()
 * 
 * - removeElement()
 * - removeList()
 * 
 * - writeToFile()
 * - isValid()
 * - printErrors()
 * - fixErrors()
 */
export default class ListManager {
    list: MediaJson;
    errors: MediaJsonError[];
    filePath = "/usr/bin/mediaserver/medien.json";
    constructor(list: any) {
        this.list = list;
        this.errors = [];
    }

    /**
     * Returns an element at given index
     * 
     * @param list_idx List index
     * @param elem_idx Element index
     * @returns element at given index
     */
    private getElement(list_idx: number, elem_idx: number): M_Element {
        if (list_idx >= 0 && list_idx < 256 && elem_idx >= 0 && elem_idx < 256) {
            return this.list.lists[list_idx].elements[elem_idx];
        } else {
            console.error("Index out of bounds while getting M_Element[" + elem_idx + "] in M_List[" + list_idx + "]");
        }
        return gen_element();
    }

    /**
     * Updates an Element at given index
     * 
     * @param list_idx List Index
     * @param elem_idx Element Index
     * @param e an Element
     */
    private updateElement(list_idx: number, elem_idx: number, e: M_Element): void {
        if (list_idx >= 0 && list_idx < 256 && elem_idx >= 0 && elem_idx < 256) {
            this.list.lists[list_idx].elements[elem_idx] = e;
        } else {
            console.error("Index out of bounds while updating M_Element[" + elem_idx + "] in M_List[" + list_idx + "]");
        }

    }

    /**
     * Returns a list with given 
     * 
     * @param idx List index
     * @returns returns a list at given index 
     */
    private getList(idx: number): M_List {
        if (idx >= 0 && idx < 256) {
            return this.list.lists[idx];
        } else {
            console.error("Index out of bounds while getting M_List[" + idx + "]");
        }
        return gen_mlist();
    }

    /**
     * Updates a list with given index 
     * 
     * @param idx list index
     * @param l main list with list identifiers and list of 
     */
    private updateList(idx: number, l: M_List): void {
        if (idx >= 0 && idx < 256) {
            this.list.lists[idx] = l;
        } else {
            console.error("Index out of bounds while getting M_List[" + idx + "]");
        }
    }


    setListIdentifier(idx: number, identifier: string): void {
        this.getList(idx).identifier = identifier;
    }

    setElementIdentifier(list_idx: number, elem_idx: number, identifier: string): void {
        this.getElement(list_idx, elem_idx).identifier = identifier;
    }

    setElementPath(list_idx: number, elem_idx: number, path: string): void {
        this.getElement(list_idx, elem_idx).path = path;
    }

    getListIdentifier(idx: number): string {
        return this.getList(idx).identifier;
    }

    getListIdentifiers(): string[] {
        return this.list.lists.map((l: M_List) => l.identifier);
    }

    getElementIdentifiers(idx: number): string[] {
        return this.getList(idx).elements.map((e: M_Element) => e.identifier);
    }

    getElementPaths(idx: number): string[] {
        return this.getList(idx).elements.map((e: M_Element) => e.path);
    }

    getElements(idx: number): M_Element[] {
        return this.getList(idx).elements;
    }

    getElementIdentifier(list_idx: number, elem_idx: number): string {
        return this.getElement(list_idx, elem_idx).identifier;
    }

    getElementPath(list_idx: number, elem_idx: number): string {
        return this.getElement(list_idx, elem_idx).path;
    }

    removeElement(list_idx: number, elem_idx: number): void {
        this.updateElement(list_idx, elem_idx, gen_element());
    }

    removeList(idx: number): void {
        this.updateList(idx, gen_mlist());
    }

    getFreeListSpot(): number {
        return this.getListIdentifiers().findIndex((value, index, obj) => value === "");
    }

    getFreeElementSpot(idx: number): number {
        return this.getElementIdentifiers(idx).findIndex((value, index, obj) => value === "");
    }

    writeToFile(): void {
        fs.writeFileSync(this.filePath, JSON.stringify(this.list, null, 2));
    }

    isValid(): boolean {
        const errors: MediaJsonError[] = [];
        let list_length_checked = 256;
        // Check if lists attribute exists
        if (!("lists" in this.list)) {
            console.error("There is not attribute lists");
        }
        const lists: M_List[] = this.list.lists;
        // Check that there are 256 M_List
        if (lists.length !== 256) {
            const message = "There are not the right number of M_List's. Expected 256 got: " + lists.length;
            const fix_list_length = () => {
                const missing: number = 256 - lists.length;
                if (missing < 0) {
                    for (let m = 0; m < -missing; m++) { lists.pop(); }
                } else {
                    for (let m = 0; m < missing; m++) { lists.push(gen_mlist()); }
                }
            };
            errors.push({ message, fix: fix_list_length });
            list_length_checked = lists.length;
        }

        for (let i = 0; i < list_length_checked; i++) {

            // Check that every M_List has an identifier attribute
            if (!("identifier" in lists[i])) {
                const message = "The M_List[" + i + "] has no identifier attribute";
                const fix_identifier = () => {
                    lists[i] = { identifier: "", elements: lists[i].elements };
                };
                errors.push({ message, fix: fix_identifier });
            }

            // Check that every M_List has an elements attribute
            if (!("elements" in lists[i])) {
                const message = "The M_List[" + i + "] has no elements attribute";
                const fix_elements = () => {
                    lists[i] = { identifier: lists[i].identifier, elements: gen_element_list() };
                };
                errors.push({ message, fix: fix_elements });
            } else {

                // Check that every M_List has 256 M_Element
                const elem_list: M_Element[] = lists[i].elements;
                let elem_list_length_checked = 256;
                if (elem_list.length !== 256) {
                    const message = "There are not the right number of M_Element's in M_List[" + i + "] Expected 256 got: " + elem_list.length;
                    const fix_list_length = () => {
                        const missing: number = 256 - elem_list.length;
                        if (missing < 0) {
                            for (let m = 0; m < -missing; m++) { elem_list.pop(); }
                        } else {
                            for (let m = 0; m < missing; m++) { elem_list.push(gen_element()); }
                        }
                    };
                    errors.push({ message, fix: fix_list_length });
                    elem_list_length_checked = elem_list.length;
                }

                for (let j = 0; j < elem_list_length_checked; j++) {
                    // Check that every M_element has an identifier attribute
                    elem_list[j].identifier;
                    if (!("identifier" in elem_list[j])) {
                        const message = "The M_Element[" + j + "] has no identifier attribute in M_List[" + i + "]";
                        const fix_identifier = () => {
                            elem_list[j] = { identifier: "", path: elem_list[j].path };
                        };
                        errors.push({ message, fix: fix_identifier });
                    }

                    // Check that every M_element has an elemets attribute
                    if (!("path" in elem_list[j])) {
                        const message = "The M_Element[" + j + "] has no path attribute in M_List[" + i + "]";
                        const fix_path = () => {
                            elem_list[j] = { identifier: elem_list[j].identifier, path: "" };
                        };
                        errors.push({ message, fix: fix_path });
                    }
                }
            }
        }
        this.errors = errors;
        return errors.length === 0;
    }


    printErrors(): void {
        for (let i = 0; i < this.errors.length; i++) {
            console.log(this.errors[i]);
        }
    }

    /**
     * fixes errors included in errors-Array
     */
    fixErrors(): void {
        if (this.errors.length === 0) {
            this.isValid();
        }
        for (let i = 0; i < this.errors.length; i++) {
            console.log("Fixing: " + this.errors[i].message);
            this.errors[i].fix();
        }
    }
    /**
     * Selected Element-Item will be moved from previous position to current position. 
     * 
     * other elements will be shifted by one index relative to current position. Either up or down
     *  
     * @param idx Index of a list
     * @param current Index of a new position of the element
     * @param previous Index of a old position of the element
     */
    moveElement(idx: number, current: number, previous: number): void {

        let lst = this.getList(idx).elements;
        const tmp = lst[previous];

        // move from bottom to top
        if (current < previous) {
            lst = lst.slice(0, current).concat(tmp).concat(lst.slice(current, previous)).concat(lst.slice(previous + 1));
            this.getList(idx).elements = lst;
        }
        //vmove from top to bottom
        else {
            lst = lst.slice(0, previous).concat(lst.slice(previous + 1, current + 1)).concat(tmp).concat(lst.slice(current + 1));
            this.getList(idx).elements = lst;
        }
    }
    /**
     * Selected List-Item will be moved from previous position to current position. 
     * 
     * other elements will be shifted by one index relative to current position. Either up or down
     * 
     * @param current Index of a new position of the element
     * @param previous Index of a old position of the element
     */
    moveList(current: number, previous: number): void {
        let lists = this.list.lists;
        const tmp = lists[previous];
        //von unten nach oben schieben
        if (current < previous) {
            lists = lists.slice(0, current).concat(tmp).concat(lists.slice(current, previous)).concat(lists.slice(previous + 1));
            this.list.lists = lists;
        }
        //von oben nach unten
        else {
            //RICHTIG
            const tmpLst = lists.slice(0, previous).concat(lists.slice(previous + 1, current + 1)).concat(tmp).concat(lists.slice(current + 1));
            this.list.lists = tmpLst;
        }

    }
}