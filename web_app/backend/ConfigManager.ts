/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable linebreak-style */
import fs from "fs";
/**
 * Config-List Interface representing Config file
 */
export interface ConfigList {
    universe: number;
    start_address: number;
    footprint_size: number;
}

export default class ConfigManager{
    list: ConfigList;
    filePath = "/usr/bin/mediaserver/config.json";

    constructor(list: any) {
        this.list = list;
    }
    
    getConfig(){
        return [ this.getUniverse(),this.getStartAddress(),this.getFootprintSize()];
    }
    
    getUniverse(){
        return this.list.universe;
    }

    getStartAddress(){
        return this.list.start_address;
    }

    getFootprintSize(): number {
        return this.list.footprint_size;
    }

    setUniverse(universe: number){
        if(universe >= 1 && universe <= 63999){
            this.list.universe = universe;
        }
        else{
            console.error("Index out of bounds while setting universe to " + universe);
        }
    }

    setStartAddress(startAddress: number){
        if(startAddress >= 1  && startAddress <= 513 - this.getFootprintSize()){
            this.list.start_address = startAddress;
        }
        else{
            console.error("Index out of bounds while setting start address to " + startAddress);
        }
    }

    writeToFile(): void {
        fs.writeFileSync(this.filePath, JSON.stringify(this.list, null, 2));
    }
}