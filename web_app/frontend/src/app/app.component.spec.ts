/* eslint-disable no-undef */
import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { By } from '@angular/platform-browser'
describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents()
  })

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.componentInstance
    expect(app).toBeTruthy()
  })

  it('should have as title \'Mediaserver\'', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.componentInstance
    expect(app.title).toEqual('Mediaserver')
  })
  it('render a independent header', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const header = fixture.debugElement.query(By.css('app-header'))
    expect(header).toBeTruthy()
  })
  it('render a independent footer', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const footer = fixture.debugElement.query(By.css('app-footer'))
    expect(footer).toBeTruthy()
  })
  it('render a independent content-window', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const contentwindow = fixture.debugElement.query(By.css('app-content-window'))
    expect(contentwindow).toBeTruthy()
  })
})
