import { ListOfElements } from './ListOfElements'

export interface List {
    identifier: string;
    elements: ListOfElements[];
}
