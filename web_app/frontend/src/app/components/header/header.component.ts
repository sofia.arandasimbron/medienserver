/* eslint-disable camelcase */
/* eslint-disable no-useless-constructor */
import { Component, OnInit } from '@angular/core'
import { DefaultService } from 'src/app/services/api'
import { MatDialog } from '@angular/material/dialog'
import { ConfigWindowComponent } from '../config-window/config-window.component'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  universe: number | undefined;
  startAddress: number | undefined;
  constructor (private apiService: DefaultService, public dialog: MatDialog) { }

  ngOnInit (): void {
    this.apiService.getConfig().subscribe((arr : number[]) => {
      this.universe = arr[0]
      this.startAddress = arr[1]
    })
  }

  /**
   * triggers the config window to open
   */
  openSettings () {
    const dialogRef = this.dialog.open(ConfigWindowComponent)
    dialogRef.afterClosed().subscribe(() => { this.ngOnInit() })
  }
}
