/* eslint-disable no-undef */
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ButtonComponent } from './button.component'

describe('ButtonComponent', () => {
  let component: ButtonComponent
  let fixture: ComponentFixture<ButtonComponent>
  let button: DebugElement
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ButtonComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    button = fixture.debugElement.query(e => e.nativeNode.textContent === '')
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('should create', () => {
    expect(component).toBeDefined()
  })
  it('Button is working ', () => {
    button.nativeElement.click()
    expect(component).toBeTruthy()
  })
  it('raises the event ', () => {
    spyOn(component.btnClick, 'emit')
    button.nativeElement.click()
    expect(component.btnClick.emit).toHaveBeenCalled()
  })
})
