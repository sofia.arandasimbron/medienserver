/* eslint-disable no-undef */
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ComponentFixture, inject, TestBed } from '@angular/core/testing'
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog'
import { By } from '@angular/platform-browser'
import { RouterTestingModule } from '@angular/router/testing'
import { DefaultService } from 'src/app/services/api'

import { DialogWindowComponent } from './dialog-window.component'

describe('DialogWindowComponent', () => {
  let component: DialogWindowComponent
  let fixture: ComponentFixture<DialogWindowComponent>
  let apiSvc: DefaultService
  // let idx: IdentifierObject
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogWindowComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} }
      // { provide: MAT_DIALOG_DATA, useValue: [] },
      // ...
      ]
    })
      .compileComponents()
  })

  beforeEach(inject(
    [DefaultService],
    (apiService: DefaultService) => {
      apiSvc = apiService
    }
  ))

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWindowComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('render a independent button-item', () => {
    const button = fixture.debugElement.query(By.css('app-button'))
    expect(button).toBeTruthy()
  })
  it('call alert if empty string', () => {
    spyOn(window, 'alert')
    component.newList('')
    expect(window.alert).toHaveBeenCalledWith('Empty String!')
  })
  it('call apiService if valid identifier', () => {
    spyOn(apiSvc, 'addList').and.callThrough()
    component.newList('Test')
    expect(apiSvc.addList).toHaveBeenCalled()
  })
})
