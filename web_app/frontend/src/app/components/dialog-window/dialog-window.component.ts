/* eslint-disable no-useless-constructor */
import { Component } from '@angular/core'
import { MatDialogRef } from '@angular/material/dialog'
import { DefaultService, IndexObject } from 'src/app/services/api'

@Component({
  selector: 'app-dialog-window',
  templateUrl: './dialog-window.component.html',
  styleUrls: ['./dialog-window.component.css']
})
export class DialogWindowComponent {
  listname: any = '';
  title: string = 'Lists';

  constructor (private apiService: DefaultService, public dialog: MatDialogRef<DialogWindowComponent>) { }

  ngOnInit () {
  }

  /**
   * Adds a new list
   *
   * @param identifier is a identifier of a List
   * @returns
   */
  newList (identifier: string) {
    if (identifier === '') {
      alert('Empty String!')
      return
    }
    this.apiService.addList({ identifier }).subscribe((out: IndexObject) => { this.dialog.close(out.idx) })
  }
}
