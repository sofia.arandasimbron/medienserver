/* eslint-disable camelcase */
/* eslint-disable no-useless-constructor */
import { Component, OnInit } from '@angular/core'

import { MatDialogRef } from '@angular/material/dialog'
import { DefaultService } from 'src/app/services/api'
import { DialogWindowComponent } from '../dialog-window/dialog-window.component'

@Component({
  selector: 'app-config-window',
  templateUrl: './config-window.component.html',
  styleUrls: ['./config-window.component.css']
})
export class ConfigWindowComponent implements OnInit {
  startAddress: number = 0;
  universe!: number;
  footprintSize: number | undefined;
  maxAllowedStartAddress!: number;

  constructor (private apiService: DefaultService, public dialog: MatDialogRef<DialogWindowComponent>) { }

  ngOnInit (): void {
    this.apiService.getConfig().subscribe((arr : number[]) => {
      this.universe = arr[0]
      this.startAddress = arr[1]
      this.footprintSize = arr[2]
      this.maxAllowedStartAddress = 513 - this.footprintSize
    })
  }

  /**
   *
   * @param num1 an integer
   * @param num2 an integer
   * @returns true if num1 is graeter than num2
   */
  public greaterThan (num1: number, num2: number) {
    return num1 > num2
  }

  /**
   *
   * @param num1 an integer
   * @param num2 an integer
   * @returns true if num1 is smaller than num2
   */
  public smallerThan (num1: number, num2: number) {
    return num1 < num2
  }

  /**
   * After saving, the backend will be updated
   */
  save () {
    this.apiService.updateConfigFile({ universe: this.universe, startAddress: this.startAddress }).subscribe(() => {})
    this.dialog.afterClosed().subscribe(() => { this.ngOnInit() })
  }
}
