import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core'
// eslint-disable-next-line no-unused-vars
import { FileManager, Toolbar, NavigationPane, ContextMenuSettings, DetailsView, ToolbarClickEventArgs, ToolbarCreateEventArgs } from '@syncfusion/ej2-filemanager'
import { FileManagerComponent } from '@syncfusion/ej2-angular-filemanager'
import { ElementObject } from 'src/app/services/api'

FileManager.Inject(Toolbar, NavigationPane, DetailsView)
@Component({
  selector: 'app-content-manager',
  templateUrl: './content-manager.component.html',
  styleUrls: ['./content-manager.component.css']
})
export class ContentManagerComponent implements OnInit {
  @ViewChild('file')
  public title: string = 'File Manager!';

  public fileManagerInstance!: FileManagerComponent;
  public ajaxSettings: object | undefined;
  public toolbarSettings: object | undefined;
  public uploadSettings: object | undefined;
  public contextMenuSettings: object | undefined;
  public hostUrl: string = '/filemanager/';
  public details: Object[] = [];
  public filePath: String = '/home/pi/media';

  @Input() fileIdentifier: string = '';
  @Input() path: string = '';
  @Output() onClick: EventEmitter<ElementObject> = new EventEmitter();

  public ngOnInit (): void {
    // Initializing the File Manager with NodeJS service.
    this.ajaxSettings = {
      url: this.hostUrl,
      downloadUrl: this.hostUrl + 'Download',
      uploadUrl: this.hostUrl + 'Upload',
      getImageUrl: this.hostUrl + 'GetImage'
      // maxFileSize: 5e+8
    }
    this.toolbarSettings = { items: ['NewFolder', 'Add Element', 'Upload', 'Delete', 'Download', 'Rename', 'SortBy', 'Refresh', 'Selection', 'View', 'Details'] }
    this.uploadSettings = {
      maxFileSize: Number.MAX_VALUE
    }
  }

  /**
   * Sends the path and name of a selected file to element-window.component
   * @param args Arguments of the toolbar button
   */
  toolbarClick (args: ToolbarClickEventArgs) {
    if (args.item.text === 'Add Element') {
      let path: string = ''
      let name: string = ''
      this.details = args.fileDetails
      // extract the path of the selected file
      this.details.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
          if (key === 'filterPath') {
            path += value
          }
        })
      })
      // build the path and extract the name of a selected file
      this.details.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
          if (key === 'name') {
            path += value
            name = value
          }
        })
      })

      this.path = this.filePath + path
      this.fileIdentifier = name
      // emits the object to the element-item
      this.onClick.emit({ identifier: this.fileIdentifier, path: this.path })
    }
  }

  toolbarCreate (args: ToolbarCreateEventArgs) {
    for (let i = 0; i < args.items.length; i++) {
      console.log('ITEMS', args.items)
      if (args.items[i].id === 'filemanager_tb_add element') {
        args.items[i].prefixIcon = 'e-icons e-fe-large'
      }
    }
  }
}
