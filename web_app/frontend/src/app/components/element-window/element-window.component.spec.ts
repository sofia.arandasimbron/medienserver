/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { ComponentFixture, inject, TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ElementWindowComponent } from './element-window.component'
import { RouterTestingModule } from '@angular/router/testing'
import { MatDialogModule } from '@angular/material/dialog'
import { By } from '@angular/platform-browser'
import { ElementItemComponent } from '../element-item/element-item.component'
import { DefaultService, ElementObject } from 'src/app/services/api'
import { CdkDragDrop } from '@angular/cdk/drag-drop'
describe('ElementWindowComponent', () => {
  let component: ElementWindowComponent
  let fixture: ComponentFixture<ElementWindowComponent>
  let apiSvc: DefaultService
  const element: ElementObject = { identifier: 'Name', path: 'D:/Bilder/bild.jgp' }
  let event: CdkDragDrop<string[]>
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ElementWindowComponent, ElementItemComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule
      ]
    })
      .compileComponents()
  })
  beforeEach(inject(
    [DefaultService],
    (apiService: DefaultService) => {
      apiSvc = apiService
    }
  ))
  beforeEach(() => {
    fixture = TestBed.createComponent(ElementWindowComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('render a independent element-item', () => {
    const elementitem = fixture.debugElement.query(By.css('element-item'))
    expect(elementitem).toBeDefined()
  })
  it('deleteElement call apiService if valid identifier', () => {
    component.listID = 1
    spyOn(apiSvc, 'deleteElement').and.callThrough()
    component.deleteElement(1)
    expect(apiSvc.deleteElement).toHaveBeenCalled()
  })
  it('updateListId call apiService getElementIdentifiers if valid identifier', () => {
    spyOn(apiSvc, 'getElementIdentifiers').and.callThrough()
    component.updateListId(1)
    expect(apiSvc.getElementIdentifiers).toHaveBeenCalled()
  })
  it('addElement call apiService addElement if valid identifier', () => {
    spyOn(apiSvc, 'addElement').and.callThrough()
    component.addElement(element)
    expect(apiSvc.addElement).toHaveBeenCalled()
  })
})
