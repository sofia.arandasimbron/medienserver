/* eslint-disable no-useless-constructor */
/* eslint-disable no-return-assign */
import { Component, OnInit } from '@angular/core'
import { DefaultService, ElementObject, IdentifierObject, IndexObject, PathObject } from 'src/app/services/api'
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop'
@Component({
  selector: 'app-element-window',
  templateUrl: './element-window.component.html',
  styleUrls: ['./element-window.component.css']
})
export class ElementWindowComponent implements OnInit {
  listID: number = 0;
  title: string = 'Elements';
  elementIdentifiers!: string[];
  elementPaths!: string[];

  constructor (private apiService: DefaultService) { }

  ngOnInit (): void {
    this.apiService.getElementIdentifiers(this.listID).subscribe((identifiers: string[]) => this.elementIdentifiers = identifiers)
    this.apiService.getElementPaths(this.listID).subscribe((paths: string[]) => this.elementPaths = paths)
  }

  /******************************************************************
 *
 *                      Event Callbacks
 *
 ******************************************************************/

  /**
   * updates the elements (backend)
   * @param idx ID of the element
   */
  private updateElement (idx: number) {
    this.apiService.getElementIdentifier(this.listID, idx).subscribe((obj: IdentifierObject) => this.elementIdentifiers[idx] = obj.identifier || '')
    this.apiService.getElementPath(this.listID, idx).subscribe((obj: PathObject) => this.elementPaths[idx] = obj.path || '')
    this.apiService.updateElement(this.listID, idx).subscribe(() => {})
  }

  /**
   * Deletes an element and updates the medien.json file
   * @param idx ID of the element
   */
  deleteElement (idx: number) {
    this.apiService.deleteElement(this.listID, idx).subscribe(() => { })
    this.apiService.updateElement(this.listID, idx).subscribe(() => { })
    this.updateElement(idx)
  }

  /**
   * Called from list-window
   * @param idx
   */
  updateListId (idx: number) {
    this.listID = idx
    this.apiService.getElementIdentifiers(idx).subscribe((identifiers: string[]) => this.elementIdentifiers = identifiers)
  }

  /**
   * Changes the order of the element list after droping an element
   * @param event
   */
  drop (event: CdkDragDrop<string[]>) {
    moveItemInArray(this.elementIdentifiers, event.previousIndex, event.currentIndex)

    this.apiService.moveElement(this.listID, { current: event.currentIndex, previous: event.previousIndex }).subscribe(() => {
      this.updateElement(this.listID)
    })
  }

  /**
   * Adds the element into the medien.json file
   * @param element
   */
  addElement (element: ElementObject) {
    this.apiService.addElement(this.listID, element).subscribe((obj: IndexObject) => {
      if (obj.idx !== undefined) {
        this.updateElement(obj.idx)
      }
      this.apiService.updateList().subscribe(() => { })
    })
  }
}
