import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
@Component({
  selector: 'app-element-item',
  templateUrl: './element-item.component.html',
  styleUrls: ['./element-item.component.css']
})
export class ElementItemComponent implements OnInit {
  @Input() id!: number;
  @Input() identifier!: string;
  @Input() path!: string;
  @Output() onDeleteElement: EventEmitter<number> = new EventEmitter();

  ngOnInit (): void {
  }

  /**
   * Emits the id of an element for delete
   */
  deleteElement (): void {
    this.onDeleteElement.emit(this.id)
  }
}
