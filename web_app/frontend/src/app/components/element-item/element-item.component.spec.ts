/* eslint-disable no-undef */
import { DebugElement } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ElementItemComponent } from './element-item.component'

describe('ElementItemComponent', () => {
  let component: ElementItemComponent
  let fixture: ComponentFixture<ElementItemComponent>
  let span: DebugElement
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ElementItemComponent]
    })
      .compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementItemComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    span = fixture.debugElement.query(e => e.nativeNode.textContent === 'close')
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('raises the event ', () => {
    spyOn(component.onDeleteElement, 'emit')
    span.nativeElement.click()
    expect(component.onDeleteElement.emit).toHaveBeenCalled()
  })
})
