/* eslint-disable no-undef */
import { ComponentFixture, inject, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ListWindowComponent } from './list-window.component'
import { RouterTestingModule } from '@angular/router/testing'
import { MatDialogModule } from '@angular/material/dialog'
import { ListItemComponent } from '../list-item/list-item.component'
import { ButtonComponent } from '../button/button.component'
import { DefaultService } from 'src/app/services/api'
import { CdkDragDrop } from '@angular/cdk/drag-drop'

describe('ListWindowComponent', () => {
  let component: ListWindowComponent
  let fixture: ComponentFixture<ListWindowComponent>
  let apiSvc: DefaultService
  let event: CdkDragDrop <string[]>
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListWindowComponent, ListItemComponent, ButtonComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule
      ]
    })
      .compileComponents()
  })
  beforeEach(inject(
    [DefaultService],
    (apiService: DefaultService) => {
      apiSvc = apiService
    }
  ))
  beforeEach(() => {
    fixture = TestBed.createComponent(ListWindowComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('render a independent button', () => {
    const button = fixture.debugElement.query(By.css('app-button'))
    expect(button).toBeTruthy()
  })
  it('onUpdate list call apiService updateList if valid identifier', () => {
    spyOn(apiSvc, 'updateList').and.callThrough()
    component.onUpdateList()
    expect(apiSvc.updateList).toHaveBeenCalled()
  })
  it('deleteList call apiService deleteList and updateList if valid identifier', () => {
    const idx = 0
    console.log(idx)
    spyOn(apiSvc, 'deleteList').and.callThrough()
    component.deleteListEvent(idx)
    expect(apiSvc.deleteList).toHaveBeenCalled()
    spyOn(apiSvc, 'updateList').and.callThrough()
    component.onUpdateList()
    expect(apiSvc.updateList).toHaveBeenCalled()
  })
  it('selectList select a List and emit an event', () => {
    const idx = 0
    component.selected = idx
    spyOn(component.onSelect, 'emit')
    component.selectList(idx)
    expect(component.onSelect.emit).toHaveBeenCalled()
  })
})
