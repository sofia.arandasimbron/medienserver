/* eslint-disable no-unused-expressions */
/* eslint-disable no-return-assign */
/* eslint-disable no-useless-constructor */
import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { DialogWindowComponent } from '../dialog-window/dialog-window.component'
import { DefaultService, IdentifierObject } from 'src/app/services/api/'
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop'
import { List } from 'src/app/List'

@Component({
  selector: 'app-list-window',
  templateUrl: './list-window.component.html',
  styleUrls: ['./list-window.component.css']
})
export class ListWindowComponent implements OnInit {
  title: string = 'Lists';
  listIdentifiers!: string[];
  lists!: List[];
  selected: number = 0;
  @Output() onSelect: EventEmitter<number> = new EventEmitter();

  constructor (private apiService: DefaultService, public dialog: MatDialog) { }

  ngOnInit () {
    this.apiService.getListIdentifiers().subscribe((list: string[]) => this.listIdentifiers = list)
  }

  /******************************************************************
   *
   *                      Event Callbacks
   *
   ******************************************************************/

  /**
   * Updates the list
   * @param idx index of a list
   */
  private updateElement (idx: number) {
    this.apiService.getListIdentifier(idx).subscribe((obj: IdentifierObject) => this.listIdentifiers[idx] = obj.identifier || '')
  }

  /**
   * This is called from a list-item-component
   * @param idx the index to be deleted
   */
  deleteListEvent (idx: number) {
    console.log(idx)
    this.apiService.deleteList(idx).subscribe((e) => { })
    this.updateElement(idx)
    this.onUpdateList()
    // alert(this.list_identifiers[idx] + " has been deleted from Lists!");
  }

  /**
   * List selector
   * called from ListItem
   * @param idx
   */
  selectList (idx: number) {
    this.selected = idx
    this.onSelect.emit(idx)
  }

  /**
   * Creates a list with a new dialog window
   */
  createList (): void {
    const dialogRef = this.dialog.open(DialogWindowComponent)
    dialogRef.afterClosed().subscribe((result: number) => { (result !== undefined) ? this.updateElement(result) : 0 })
    dialogRef.afterClosed().subscribe(() => { this.onUpdateList() })
  }

  /**
   * Updates the list
   */
  onUpdateList (): void {
    this.apiService.updateList().subscribe(() => {})
  }

  /**
   * Changes the order of the element list after droping an element
   * @param event
   */
  drop (event: CdkDragDrop<string[]>) {
    moveItemInArray(this.listIdentifiers, event.previousIndex, event.currentIndex)
    this.apiService.moveList({ current: event.currentIndex, previous: event.previousIndex }).subscribe(() => {
      this.onUpdateList()
      this.selectList(event.currentIndex)
    })
  }
}
