/* eslint-disable no-undef */

import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ContentManagerComponent } from '../content-manager/content-manager.component'
import { ElementWindowComponent } from '../element-window/element-window.component'
import { ListWindowComponent } from '../list-window/list-window.component'

import { ContentWindowComponent } from './content-window.component'

describe('ContentWindowComponent', () => {
  let component: ContentWindowComponent
  let fixture: ComponentFixture<ContentWindowComponent>
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContentWindowComponent, ListWindowComponent, ElementWindowComponent, ContentManagerComponent]
    })
      .compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentWindowComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
