import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core'

@Component({
  selector: 'app-content-window',
  templateUrl: './content-window.component.html',
  styleUrls: ['./content-window.component.css']
})
export class ContentWindowComponent implements OnInit {
  @ViewChild('contentWindowTemplate') contentWindowTemplate!: TemplateRef<any>

  ngOnInit (): void {
  }
}
