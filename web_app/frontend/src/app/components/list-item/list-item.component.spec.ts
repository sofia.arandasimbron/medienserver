/* eslint-disable no-undef */
import { DebugElement } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ListItemComponent } from './list-item.component'

describe('ListItemComponent', () => {
  let component: ListItemComponent
  let fixture: ComponentFixture<ListItemComponent>
  let span: DebugElement
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListItemComponent]
    })
      .compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    span = fixture.debugElement.query(e => e.nativeNode.textContent === 'close')
  })
  it('should created', () => {
    expect(component).toBeDefined()
  })
  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('Inputs working ', () => {
    component.id = 1
    expect(component.id).toEqual(1)
    component.identifier = 'Gruppe 72'
    expect(component.identifier).toEqual('Gruppe 72')
    component.selected = true
    expect(component.selected).toEqual(true)
  })

  it('delete List with id = 1', () => {
    component.id = 1
    expect(component.onDeleteList).toBeTruthy()
  })

  it('delete () working', () => {
    component.id = 1
    expect(component.deleteList()).toBe()
  })
  it('select List with id = 1', () => {
    component.id = 1
    spyOn(component.onSelect, 'emit')
    span.nativeElement.click()
    expect(component.onSelect.emit).toHaveBeenCalled()
  })

  it('delete () working', () => {
    component.id = 1
    expect(component.clickListItem()).toBe()
  })
  it('raises the event ', () => {
    spyOn(component.onDeleteList, 'emit')
    span.nativeElement.click()
    expect(component.onDeleteList.emit).toHaveBeenCalled()
  })
  it('selected  ListItem with id = 0 if clicked run', () => {
    component.id = 0
    component.selected = true
    component.clickListItem()
    expect(component.id).toBe(0)
  })
})
