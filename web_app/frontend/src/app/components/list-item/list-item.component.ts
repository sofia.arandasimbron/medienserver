import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() id!: number;
  @Input() identifier!: string;
  @Input() selected: boolean = false;
  @Output() onDeleteList: EventEmitter<number> = new EventEmitter();
  @Output() onSelect: EventEmitter<number> = new EventEmitter();

  ngOnInit (): void {
  }

  /**
   * Emits the id of a list for delete
   */
  deleteList (): void {
    this.onDeleteList.emit(this.id)
  }

  /**
   * Emits the id of the list after being clicked
   */
  clickListItem (): void {
    this.onSelect.emit(this.id)
  }
}
