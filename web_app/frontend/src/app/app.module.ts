import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatInputModule } from '@angular/material/input'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatCardModule } from '@angular/material/card'
import { MatIconModule } from '@angular/material/icon'
import { MatDialogModule } from '@angular/material/dialog'
import { FormsModule } from '@angular/forms'
import { DragDropModule } from '@angular/cdk/drag-drop'

import { DetailsViewService, FileManagerModule, NavigationPaneService, ToolbarService } from '@syncfusion/ej2-angular-filemanager'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { ButtonComponent } from './components/button/button.component'
import { FooterComponent } from './components/footer/footer.component'
import { HeaderComponent } from './components/header/header.component'
import { ListWindowComponent } from './components/list-window/list-window.component'
import { ElementWindowComponent } from './components/element-window/element-window.component'
import { ElementItemComponent } from './components/element-item/element-item.component'
import { ListItemComponent } from './components/list-item/list-item.component'
import { DialogWindowComponent } from './components/dialog-window/dialog-window.component'
import { enableRipple } from '@syncfusion/ej2-base'
import { ApiModule } from './services/api/api.module'
import { ContentWindowComponent } from './components/content-window/content-window.component'
import { ContentManagerComponent } from './components/content-manager/content-manager.component'
import { ConfigWindowComponent } from './components/config-window/config-window.component'
enableRipple(true)
@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    FooterComponent,
    HeaderComponent,
    ListWindowComponent,
    ElementWindowComponent,
    ElementItemComponent,
    ListItemComponent,
    DialogWindowComponent,
    ContentWindowComponent,
    ContentManagerComponent,
    ConfigWindowComponent
  ],
  imports: [
    ApiModule,
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    ScrollingModule,
    FileManagerModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatToolbarModule,
    FormsModule,
    DragDropModule
  ],
  providers: [NavigationPaneService, ToolbarService, DetailsViewService],

  bootstrap: [AppComponent]
})
export class AppModule { }
