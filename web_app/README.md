# npm installieren
0. `sudo apt update && sudo apt upgrade`
1. `sudo apt-get install nodejs npm node-semver`
2. `export NODE_OPTIONS="--max-old-space-size=3072"`

# Angular installieren
1. `sudo npm install -g @angular/cli@13.0.3`

# Dependencies installieren
1. `cd /medienserver/web_app/backend`
2. `sudo npm install`
3. Zum frontend-Ordner navigieren ( `cd /medienserver/web_app/frontend`)
4. `npm install @syncfusion/ej2-filemanager-node-filesystem`
5. `cd /node_modules/@syncfusion/ej2-filemanager-node-filesystem`
6. `npm install`

# Apache Server installieren und konfigurieren
1. `sudo apt update`
2. `sudo apt upgrade`
3. `sudo apt install apache2`
4. `sudo nano /etc/apache2/sites-available/medienserver.conf`
```
<VirtualHost *:80>
    ServerAlias medienserver
	ServerName medienserver
	DocumentRoot /home/pi/medienserver/web_app/frontend/dist/frontend
		
	
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	
	RewriteEngine On
	ProxyRequests Off
	ProxyPreserveHost On
	 
	ProxyPass /filemanager/ http://localhost:8090/
	ProxyPassReverse /filemanager/ http://localhost:8090/
</VirtualHost>
```
5. `sudo nano /etc/apache2/apache2.conf`
Add following part: 
```
<Directory "/home/pi/medienserver/web_app/frontend/dist/frontend/">
    AllowOverride None
    Require all granted
</Directory>
```
6. `sudo a2ensite medienserver.conf`
7. `sudo a2dissite 000-default.conf`
8. `sudo systemctl reload apache2`
9. `sudo a2enmod proxy proxy_http`

## IP-Adresse des Webservers im Browser aufrufen
