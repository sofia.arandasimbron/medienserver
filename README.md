# Mediaserver

This project provides a full software stack, to create a dmx-controllable mediaserver on a Raspberry Pi 4.

![](doc/images/Softwareaufbau.png)

## Features

### Current Features
- Upload Media-Files via webserver
- Manage Media-Lists via webserver
- Playback of Mediafiles controlled via sACN
    - Videofiles only
    - Optimized for H264 Playback (Hardware acceleration)

### Planned Features
- Configuration of DMX-Address and Universe via webserver
- Support for image- and audiofiles
- Autostart of software-stack
- Control over video-alpha-levels
- Software Logs available via webserver
- Multiple Video Layers
- Control over audio-levels
- Status-overview (temperature, cpu, ...) on webserver
- Control over colors, speed, scale, position, rotation
- Usage of external storage
- WiFi
- Video Preview over webserver
- Video Preview over CITP
- Thumbnails over CITP
- Preloading of videos
- Live Video Inputs (USB Webcams / Capturecards?)
- RDM?
- Availability for other platforms
- Usage of both HDMI Outputs?
- Fill and Key Playback Mode?

### Known limitations
- Performance Limitations on the Raspberry Pi: Higher resolutions than FullHD with H264 can cause problems
- H264 Videofiles with colorimetry-information in the header won't play. (Work in Progress)

## Installation

### Install prebuilt image
Coming soon

### Build from source
- Installation of Operating System
    - Flash `Raspberry Pi OS Lite 64bit` on SD Card
    - Add empty `ssh` File in Root-Directory of SD Card
- Configuration of raspberry pi
    - Login to pi via ssh with username `pi` and password `raspberry`
    - `sudo raspi-config` --> Advanced Settings --> Expand filesystem
    - `sudo apt update`
    - `sudo apt upgrade`
    - `sudo reboot now`
- Install Requirements
    - `sudo apt install git nodejs npm node-semver apache2 cmake libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-pulseaudio doxygen` (Can take a while)
    - `sudo npm install -g @angular/cli@13.0.3`
    - `sudo npm install -g express`
    - `sudo npm install -g ts-node`
    - `sudo npm install -g typescript`
    - `export NODE_OPTIONS="--max-old-space-size=3072"`
    - TODO: gst-shark https://developer.ridgerun.com/wiki/index.php?title=GstShark_-_Getting_Started
- Configure apache2
    - `sudo a2ensite medienserver.conf && sudo a2dissite 000-default.conf && sudo systemctl reload apache2 && sudo a2enmod proxy proxy_http && sudo a2enmod rewrite`
    - `sudo systemctl reload apache2`
- Clone Repository
    - `cd /home/pi`
    - `git clone REPO-URL`
    - `cd medienserver`
    - `git submodule init`
    - `git submodule update`
- Build application
    - `sudo cmake -B build`
    - `sudo cmake --build build`
    - `cd build`
    - `sudo make install`
    - `sudo mediaserver-start` (Can take a while)
    - `sudo reboot now`
- Enable Autostart
    - `sudo systemctl enable mediaserver-init.service`

## Usage
- Software will start automatically
- A webserver is accessible at the IP Address if the Pi
- Media files can be uploaded via the webserver
- The DMX Address can be changed in the webserver
- The Mediaserver will occupy 2 DMX-Channels:
    - Channel 1: Select List 0 to 255
    - Channel 2: Select Element 0 to 255 in selected List

## Developement Information

### Software architecture

![](doc/images/Zielanwendung.png)

### Tools

#### Generate documentation of local_app
`cmake -B build -DBUILD_DOC=ON`

#### Linting of local_app
`cmake -B build -DDO_FORMAT=ON`

#### Performancetests of Pipeline
- GST-Shark Skript for generation of pipeline-visualisation and performance graphs
- `/local_app/general_tools/gst-performance-test`
